
import React from 'react';
const { render, fireEvent, screen } = require('@testing-library/react');
import "@testing-library/jest-dom/extend-expect";
import { MemoryRouter } from 'react-router-dom';
import Home from '../src/pages/home.js'
import Cities from '../src/pages/cities.js';
import Shelters from '../src/pages/shelters.js';
import FoodPantries from '../src/pages/food-pantries.js';
import About from '../src/pages/about.js'
import NavBar from "../src/components/Navbar.js";
import cities_test from "./cities_test.json";
import shelters_test from "./shelters_tests.json";
import pantries_test from "./pantries_test.json"

const URL = 'https://app.bridging-the-bayou.me/'

test("Testing Navbar's content", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <NavBar />
    </MemoryRouter>
  );
  expect(getByText("About")).toBeInTheDocument();
  expect(getByText("Home")).toBeInTheDocument();
  expect(getByText("Cities")).toBeInTheDocument();
  expect(getByText("Shelters")).toBeInTheDocument();
  expect(getByText("Food Pantries")).toBeInTheDocument();
});

test("Test the Cities page", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <Cities />
    </MemoryRouter>
  );
  expect(getByText("Cities in Delta region")).toBeInTheDocument();
});

test("Test Cities' instances", async () => {
  const cities = cities_test;
  const tree = render(
      <MemoryRouter>
        <Cities cities={cities} highlight={""} />
      </MemoryRouter>
    )
  expect(tree).toMatchSnapshot();
});

test("Test the Shelters page", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <Shelters />
    </MemoryRouter>
  );
  expect(getByText("Shelters in Delta region")).toBeInTheDocument();
});

test("Test Shelters' instances", async () => {
  const shelters = shelters_test;
  const tree = render(
      <MemoryRouter>
        <Shelters shelters={shelters} highlight={""} />
      </MemoryRouter>
    )
  expect(tree).toMatchSnapshot();
});

test("Test the Food Pantries page", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <FoodPantries />
    </MemoryRouter>
  );
  expect(getByText("Food Pantries in Delta region")).toBeInTheDocument();
});

test("Test Food Pantries' instances", async () => {
  const pantries = pantries_test;
  const tree = render(
      <MemoryRouter>
        <FoodPantries pantries={pantries} highlight={""} />
      </MemoryRouter>
    )
  expect(tree).toMatchSnapshot();
});

test("Test the About page", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <About />
    </MemoryRouter>
  );
  expect(getByText("About")).toBeInTheDocument();
});

test("Test Bridging the Bayou's purpose in About page", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <About />
    </MemoryRouter>
  );
  expect(getByText("Bridging the Bayou is a project designed to serve homeless people in the Mississippi Delta region. We provide resources to help address their housing and food insecurity. This website is a directory for those in the Mississippi Delta region who are homeless and looking for help.")).toBeInTheDocument();
});

test("Test the Home page", async () => {
  const { getByText } = render(
    <MemoryRouter>
      <Home />
    </MemoryRouter>
  );
  expect(getByText("Bridging the Bayou")).toBeInTheDocument();
});