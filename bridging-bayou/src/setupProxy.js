const { createProxyMiddleware } = require('http-proxy-middleware');

console.log('setup proxy ????')
module.exports = function(app) {
  app.use(
    '/api/*',
    createProxyMiddleware({
      target: 'http://54.224.191.12:5000',
      changeOrigin: true,
      pathRewrite: {
        '^/api': '',  // remove the /api prefix before forwarding the request
      },
    })
  );
};
