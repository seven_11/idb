import pytest
import sys
import json
sys.path.append('../../../api')
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_get_all_shelter_names(client):
    response = client.get('/allShelterNames')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)

def test_get_all_shelter_briefs(client):
    response = client.get('/allShelterBriefs')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)

def test_get_shelter(client):
    response = client.get('/allShelterNames')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    if data:
        shelter_name = data[0]
        response = client.get(f'/shelter/{shelter_name}')
        assert response.status_code == 200
        data = json.loads(response.data)
        assert isinstance(data, dict)

def test_get_nonexistent_shelter(client):
    shelter_name = 'NonExistentShelter'
    response = client.get(f'/shelter/{shelter_name}')
    assert response.status_code == 404
    data = json.loads(response.data)
    assert 'error' in data
    assert data['error'] == 'Shelter not found'

def test_get_pantries(client):
    response = client.get('/allPantryNames')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)

def test_get_pantry(client):
    response = client.get('/allPantryNames')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    if data:
        pantry_name = data[0]
        response = client.get(f'/pantry/{pantry_name}')
        assert response.status_code == 200
        data = json.loads(response.data)
        assert isinstance(data, dict)

def test_get_nonexistent_pantry(client):
    pantry_name = 'NonExistentPantry'
    response = client.get(f'/pantry/{pantry_name}')
    assert response.status_code == 404
    data = json.loads(response.data)
    assert 'error' in data
    assert data['error'] == 'Pantry not found'

def test_get_all_pantry_briefs(client):
    response = client.get('/allPantryBriefs')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)

def test_get_all_city_names(client):
    response = client.get('/allCityNames')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)

def test_get_all_city_briefs(client):
    response = client.get('/allCityBriefs')
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
