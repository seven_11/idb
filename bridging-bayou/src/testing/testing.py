import unittest
import selenium
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.bridging-the-bayou.me/"

class Test(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches", ["enable-logging"])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1920,1080")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}
        self.driver = webdriver.Chrome(
            options=options, service=Service(ChromeDriverManager().install())
        )
        self.driver.get(URL)

    def tearDown(self):
        self.driver.quit()
    
    def test_home(self):
        self.driver.get(URL + "home")
        title_element = self.driver.find_element(By.CSS_SELECTOR, ".display-2")
        self.assertIn("Bridging the Bayou", title_element.text)

    def test_cities(self):
        self.driver.get(URL + "cities")
        title_element = self.driver.find_element(By.CSS_SELECTOR, ".display-3")
        self.assertIn("Cities in Delta region", title_element.text)
        
    def test_shelters(self):
        self.driver.get(URL + "shelters")
        title_element = self.driver.find_element(By.CSS_SELECTOR, ".display-3")
        self.assertIn("Shelters in Delta region", title_element.text)

    def test_food_pantries(self):
        self.driver.get(URL + "food-pantries")
        title_element = self.driver.find_element(By.CSS_SELECTOR, ".display-3")
        self.assertIn("Food Pantries in Delta region", title_element.text)

    def test_about(self):
        self.driver.get(URL + "about")
        header_element = self.driver.find_element(By.CSS_SELECTOR, "h1")
        self.assertEqual("About", header_element.text)

    def test_about_laura_young(self):
        self.driver.get(URL + "about")
        element = WebDriverWait(self.driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//*[contains(text(), 'Laura Young')]"))
        )
    
    def test_about_caleb_wolf(self):
        self.driver.get(URL + "about")
        element = WebDriverWait(self.driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//*[contains(text(), 'Caleb Wolf')]"))
        )
    
    def test_about_kevin_pham(self):
        self.driver.get(URL + "about")
        element = WebDriverWait(self.driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//*[contains(text(), 'Kevin Pham')]"))
        )
    
    def test_about_julia_elias(self):
        self.driver.get(URL + "about")
        element = WebDriverWait(self.driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//*[contains(text(), 'Julia Elias')]"))
        )
    
    def test_about_sam_hooper(self):
        self.driver.get(URL + "about")
        element = WebDriverWait(self.driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//*[contains(text(), 'Sam Hooper')]"))
        )



if __name__ == "__main__":
    unittest.main()
