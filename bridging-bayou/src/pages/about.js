import React from "react";
import PulseLoader from "react-spinners/PulseLoader";
import { Link } from "react-router-dom";

import "../css/dropdown.css"
import "../css/card-instance.css"
import "../css/homepage.css"
import "../App.css"


const lauraContributorInfo = {
  headshotFilename: "Laura Young.png",
  name: "Laura Young",
  bio: "Hi, my name is Laura Young and I am a junior majoring Computer Science. My hobbies include intramural sports, rock climbing, and watching anime.", //TODO
  majorResponsibilities: "Frontend, instance pages.", //TODO
  numUnitTests: 0,
  email: "lauracyoung@utexas.edu"
};

const contributorInfo = {
  "Kevin Pham": {
    headshotFilename: "Kevin Pham.png",
    name: "Kevin Pham",
    bio: "I am a third-year majoring in computer science. What I love to do outside of school is bouldering, traveling, and spending time with loved ones.", //TODO
    majorResponsibilities: "Frontend, instance pages.", //TODO
    numUnitTests: 38
  },
  "Sam Hooper": {
    headshotFilename: "Sam Hooper.png",
    name: "Sam Hooper",
    // bio: "I'm Sam Hooper! This is my bio.", //TODO
    bio: "I'm a sophmore at UT Austin majoring in computer science. Outside of school, I spend most of my time reading, running, talking to friends, and working on other jobs I have.", //TODO
    majorResponsibilities: "About page, site setup/hosting, data collection", //TODO
    numUnitTests: 0
  },
  "Julia Elias": {
    headshotFilename: "Julia Elias.png",
    name: "Julia Elias",
    bio: "Hi! My name is Julia, and I am a third-year computer science major at UT. Outside of web development, I love spending my time outdoors, whether that be through running, hiking, camping, or stargazing.",
    majorResponsibilities: "Home page and navigation bar, design of pages and components.",
    numUnitTests: 0
  },
  "Caleb Wolf": {
    headshotFilename: "Caleb Wolf.png", //TODO
    name: "Caleb Wolf",
    bio: "Hi, I'm Caleb, a junior Computer Science major! Outside of school, I enjoy hanging out with friends, working out, and playing Minecraft.", //TODO
    majorResponsibilities: "Frontend, model pages.", //TODO
    numUnitTests: 0
  },
  "Laura Young": lauraContributorInfo
};

function makeAliases() {
  const obj = {};
  for(const set of ALIAS_SETS)
    for(const elem of set)
      obj[elem] = set;
  return obj;
}

const ALIAS_SETS = [
  ["Julia Elias", "julia-elias", "jjelias03@gmail.com"],
  ["Kevin Pham", "Rhirekith", "zeddyas1@gmail.com"],
  ["Sam Hooper", "samhooper256", "samhooper256@gmail.com"],
  ["Caleb Wolf", "calebwolf42@gmail.com"],
  ["Laura Young", "laurayoung@MacBook-Air.local"],
];

const ALIASES = makeAliases(ALIAS_SETS);

function isSamePerson(a, b) {
  return ALIASES[a] === ALIASES[b];
}

function getCanonAlias(alias) {
  return ALIASES[alias][0];
}

function getInfo(alias) {
  return contributorInfo[getCanonAlias(alias)];
}

const totalUnitTests = Object.values(contributorInfo).reduce((total, contributor) => total + contributor.numUnitTests, 0);

const gitLabProjectId = "50437134";


const AboutCard = ({ alias, commits, issuesClosed }) => {
  console.log(`alias: ${alias}`);
  const info = getInfo(alias);
  console.log(`info for ${alias}`);
  console.log(info);
  return (
    <div className="card h-100" style={{width: 18 + "rem"}}>
      <img src={info.headshotFilename} className="img-thumbnail" alt="team member photo"/>
      <div className="card-body">
        <h5 className="card-title">{info.name}</h5>
      </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            {info.bio}
          </li>
          <li className="list-group-item">

            Email:{info.email}
          </li>
          <li className="list-group-item">

            Major Responsibilites: {info.majorResponsibilities}
          </li>
          <li className="list-group-item">
            Commits:{commits}
          </li>
          <li className="list-group-item">
            Issues Closed: {issuesClosed}
          </li>
          <li className="list-group-item">
            Unit Tests: {info.numUnitTests}
          </li>
        </ul>
    </div>
  );
};

const OurTeam = ({ contributors, issues }) => {
  const issuesClosedByName = {};
  let totalIssuesClosed = 0;
  for(const issue of issues)
    if(issue.is_closed)
      totalIssuesClosed++;
  for(const contributor of contributors) {
    const name = contributor.name;
    let numClosed = 0;
    for(const issue of issues) {
      if(issue.is_closed) {
        for(const assignee of issue.assignees) {
          if(isSamePerson(assignee.name, name))
            numClosed++;
        }
      }
    }
    issuesClosedByName[contributor.name] = numClosed;
  }
  const totalCommits = contributors.reduce((total, contributor) => total + contributor.commits, 0);
  const contributorNames = new Set();
  for(const contributor of contributors)
    contributorNames.add(contributor.name);
  const commitMap = {};
  for(const contributor of contributors) {
    commitMap[getCanonAlias(contributor.name)] = 0;
  }

  for(const contributor of contributors) {
    commitMap[getCanonAlias(contributor.name)] += contributor.commits;
  }

  return (
    <div className="container p-4">

      <div className="row justify-content-evenly" >

        {/* Omitting the "key" below causes a warning from react. The key helps uniquely identify the elements. */}
        {[...contributorNames].map(name => 
              <div className="col col-md-6 col-lg-4 p-3 d-flex justify-content-center"> 
              <AboutCard key={name} alias={name} commits={commitMap[getCanonAlias(name)]} issuesClosed={issuesClosedByName[name]}/>
              </div>)}
      </div>
      <h1 className="display-6 py-3 about-header">Project Resources</h1>
      <ProjectResources totalCommits={totalCommits} totalIssuesClosed={totalIssuesClosed}/>
    </div>
  );
};

const ProjectResources = ({totalCommits, totalIssuesClosed}) => {
  return (
      <div>
      <div className="d-flex justify-content-center gap-1">
        <div className="about-button">Total Commits: {totalCommits} </div>
        <div className="about-button">
          Total Issues Closed: {totalIssuesClosed}
        </div>
        <div className="about-button">
          Total Unit Tests: {totalUnitTests}
        </div>
      </div>
      <div className="d-flex justify-content-center gap-2 py-3">
        <Link to={"https://documenter.getpostman.com/view/29331498/2s9YJXb65D"}>
          <div className="btn btn-card">API Documentation</div></Link>
        <Link to={"https://gitlab.com/seven_11/idb"}><div className="btn btn-card">Github Repository</div></Link>
      </div>
    </div>
  )
}
const DataSourceLink = ({ link, text }) => {
  return (
    <Link to={link}>
    <div className="btn-source">{text}</div></Link>
    
  );
}

const DataSources = () => {
  return (
    <div className="row home-about">
      <div className="col-sm align-items-center">
        <h4 className="display-5 text-white">Data Sources</h4>
            {/* Cite for quote: https://www.bbc.com/news/magazine-16385337*/}
            <p className="text-white w-75 m-auto">A big challenge for our project was integrating multiple disparate and heterogenous data sources. The data provided for each of our three "data models" on our website (cities, homeless shelters, and food pantries) comes from multiple different sources. The first step in combining data from these different sources is to find a common axis point - that is, a field which all of these sources share. For all three of our models, the common field we use is simply the name of the place (or for cities, a combination of the city name and state abbreviation). With a common field established, we combine data from different sources on this common field. For data fields present in multiple sources, we aggregate them differently depending on the kind. For data that can be repeated (for example, photos, which a single model instance can have multiple of), we compile all of them into a list; for data that cannot be repeated (for example, a citiy's population count, which a city only has one of), we preferred the data source with the more reliable data. In this way, we aggregated data from many different sources and stored them in our PostgreSQL database hosted on AWS.
 </p>
        <div className="row">
          <div className="d-flex flex-column flex-lg-row justify-content-center gap-3 py-3">
            <DataSourceLink link="https://myresources.mdhs.ms.gov/MRProviderServices/Index?service=Food" text="Mississippi Department of Human Services"/>
            <DataSourceLink link="https://www.feedingamerica.org/find-your-local-foodbank/all-food-banks" text="Feeding America"/>
            <DataSourceLink link="https://foodpantries.org/" text="FoodPantries.org"/>
            <DataSourceLink link="https://arkansasfoodbank.org/locations/#agency-finder" text="Arkansas Food Bank"/>
            <DataSourceLink link="https://www.homelessshelterdirectory.org/" text="Homeless Shelters Directory"/>
            <DataSourceLink link="https://www.census.gov/data/developers/data-sets.html" text="U.S. Census APIs"/>
            <DataSourceLink link="https://ampleharvest.org/" text="AmpleHarvest"/>
          </div>
        </div>
      </div>
    </div>
    
  );  
};

const ToolsUsed = () => {
  const list = [
    {"name":"Node", "img":"node.png"},
    {"name":"React", "img":"react.png"},
    {"name":"Bootstrap", "img":"bootstrap.png"},
    {"name":"PostgreSQL", "img":"postgre.png"},
    {"name":"Nginx", "img":"nginx.png"},
    {"name":"Flask", "img":"flask.png"},
    {"name":"Postman", "img":"postman.png"},
    {"name":"GitLab Contributors/Issues API", "img":"gitlab.png"},
    {"name":"Amazon Web Services (Amplify, EC2)", "img":"aws.png"}

  ]
  return (
    <div>
      <div>We used the tools, frameworks, APIs, and technologies listed below to make Bridging the Bayou.</div>
      <div className="container p-4">
        <div className="row justify-content-evenly" >
        {list.map(info => 
        
        <div className="col p-3 d-flex justify-content-center"> 
          <div className="card h-100" style={{width: 18 + "rem"}}>
            <img src={info.img} className="img-thumbnail" alt="team member photo"/>
            <div className="card-body">
              <h5 className="card-title">{info.name}</h5>
            </div>
          </div>
        </div>
        )}
      </div>
      </div>
    </div>
  );
}


const AboutBody = () => {
  const [contributors, setContributors] = React.useState([]);
  const [isContributorsLoading, setIsContributorsLoading] = React.useState(true);
  const [issues, setIssues] = React.useState([]);
  const [isIssuesLoading, setIsIssuesLoading] = React.useState(true);
  const isLoading = isContributorsLoading || isIssuesLoading;

  React.useEffect(() => {
    function cleanContributors(contributors) {
      return contributors;
    }

    async function fetchContributors() {
      try {
        const response = await fetch(`https://gitlab.com/api/v4/projects/${gitLabProjectId}/repository/contributors`);
        const data = await response.json();
        if (response.ok) {
          const contributorsCleaned = cleanContributors(data);
          setContributors(contributorsCleaned);
          console.log(' ');
          console.log(contributorsCleaned);
        } else {
          console.error("Failed to fetch contributors", data);
        }
      } catch (error) {
        console.error("An error occurred while fetching contributors:", error);
      } finally {
        setIsContributorsLoading(false);
      }
    }
    if(isContributorsLoading)
      fetchContributors();
  }, [isContributorsLoading]);

  React.useEffect(() => {
    function cleanIssues(issues) {
      for(let i = 0; i < issues.length; i++) {
        const issue = issues[i];
        issues[i] = {
          id: issue.id,
          is_closed: issue.closed_by !== null,
          closer_username: (issue.closed_by === null) ? null : issue.closed_by.username,
          closer_name: (issue.closed_by === null) ? null : issue.closed_by.name,
          assignees: issue.assignees
        };
      }
      return issues;
    }

    async function fetchIssues() {
      try {
        const response = await fetch(`https://gitlab.com/api/v4/projects/${gitLabProjectId}/issues?per_page=100`);
        const data = await response.json();
        if (response.ok) {
          setIssues(cleanIssues(data));
        } else {
          console.error("Failed to fetch contributors", data);
        }
      } catch (error) {
        console.error("An error occurred while fetching contributors:", error);
      } finally {
        setIsIssuesLoading(false);
      }
    }
    if(isIssuesLoading)
      fetchIssues();
  }, [isIssuesLoading]);
  
  return (
    <div className="background-page">
      <div className="w-100" id="shelter-image">
        <h1 className="display-3 model-header py-5">About</h1>
        <p className="text-white w-75 m-auto">
          Bridging the Bayou is a project designed to serve homeless people in the Mississippi Delta region. We provide resources to help address their housing and food insecurity. This website is a directory for those in the Mississippi Delta region who are homeless and looking for help.
        </p>
      </div>

      <h1 className="display-4 py-3 about-header">Our Team</h1>
      { isLoading ?
        <PulseLoader 
          color={"gray"}
          loading={isLoading}
          // cssOverride={override}
          size={20}
          speedMultiplier={0.5}
          aria-label="Loading Spinner"
          data-testid="loader"
        /> :
        <OurTeam contributors={contributors} issues={issues}/>
      }

      <DataSources/>
      <h1 className="display-4 py-4 about-header">Tools Used</h1>
      <ToolsUsed/>
    </div>
  );
}

const About = () => {
  return (
    <div>
      <AboutBody/>
    </div>
  );
};
  
export default About;