// Template for displaying shelter information on the shelter's webpage
import "../../css/shelter-page.css"
import "../../App.css"
import {useState, useEffect} from 'react'
import { getShelterInstance, getPantryInstance} from "../../requests/requests";
import Map from '../../components/Map'
import { useParams } from "react-router-dom";
import {Link} from 'react-router-dom'

const ShelterPage = () => {
  const [loadingDataStarted, setLoadingDataStarted] = useState(false);
  const [data, setData] = useState(null);
  const [name, setName] = useState(useParams()['name']);
  const [pantry, setPantry] = useState(null)

  useEffect(() => {
      async function fetchShelterInfo() {
        const info = await getShelterInstance(name);
        console.log(info)
        setData(info)
        return info;
        // Fetch each shelter's images
      }

      if(!loadingDataStarted) {
        setLoadingDataStarted(true);
        fetchShelterInfo()
      }
    }, [loadingDataStarted]);

  // Method for getting closest food pantry

  // Method for getting instance card of city
    return (
      <div className="page">

        {/* Header banner with background image */}
        {data && <div>
        <div className="w-100" id="shelter-image">
          <div className="d-flex justify-content-md-center align-items-center p-6">
            <div>
              <h1 className="display-3 text-white py-5">{data.name}</h1>
            </div>
          </div>
        </div>

        <div className="row mt-6 shelter-about">
          <div className="col-sm my-auto">
            {/* TODO: add default image */}
            <img src={data.photo_url_1} className="shelter-image" alt="shelter image"/>
          </div>
          <div className="col-sm align-items-start pb-3">
            <h4 className="display-5 mt-5">Our mission</h4>

            {data.description ? data.description : "Information not available."}
            <h4 className="display-5 mt-4">Our resources</h4>
            <div className="row text-start">
              <div className="col align-items-start">
              Genders served
              </div>
              <div className="col text-left">
              {data.genders_served ? data.genders_served : "Information not available."}              
              </div>
            </div>
            <div className="row text-start">
              <div className="col align-items-start">
              Child Support
              </div>
              <div className="col text-left">
              {data.serves_children ? "Yes" : "Not available."}              
              </div>
            </div>
            <div className="row text-start">
              <div className="col align-items-start">
              Laundry Services
              </div>
              <div className="col text-left">
              {data.has_laundry_services ? "Yes" : "Not available"}
              </div>
            </div>
           
  

            <div className="row text-start">
              <div className="col align-items-start">
              Nearby City
              </div>
              <div className="col text-left">
              <Link to={`/city/${data.city}/${data.state}`}><div className="btn btn-card my-2">{data.city}, {data.state}</div></Link>
              </div>
            </div>

            <div className="row text-start">
              <div className="col align-items-start">
              Closest Pantry
              </div>
              <div className="col text-left">
              <Link to={`/pantry/${data.nearest_pantry}`}><div className="btn btn-card">{data.nearest_pantry}</div></Link>
              </div>
            </div>
        </div>
      </div>


        {/* Contains info about the shelter and services they provide */}
        <span className="g-4">
           </span>
        <div className="row p-3 m-6">
          <div className="col align-items-center p-3">
              <Map data={data}locationName={data.name} address={data.address} city={data.city} state={data.state}/>
          </div>
          <div className="col align-items-center p-3 shelter-contact">
            <h4 className="display-5 text-black">Contact us</h4>
            
            {data.address &&
              <div className="row text-start">
                <div className="col align-items-start">
                Location
                </div>
                <div className="col">
                {data.address}
                </div>
              </div>
            }

            {data.city &&
              <div className="row text-start">
                <div className="col align-items-start">
                City
                </div>
                <div className="col">
                {data.city}
                </div>
              </div>
            }
            <div className="row text-start">
              <div className="col align-items-start">
                Phone number
              </div>
              <div className="col text-left">
              <a href={data.phone_number}>{data.phone_number}</a>
              </div>
            </div>

            {data.email_address &&
              <div className="row text-start">
                <div className="col align-items-start">
                  Email
                </div>
                <div className="col text-left">
                <a href={data.email_address}>{data.email_address}</a>
                </div>
              </div>
            }
            
            {data.official_website && 
              <div className="row text-start">
                <div className="col align-items-start">
                Website
                </div>
                <div className="col text-left">
                  <a href={data.official_website}>{data.official_website}</a>
                </div>
              </div>
            }

            <div className="row text-start">
              <div className="col align-items-start">
              Socials
              </div>
              <div className="col text-left">
              {data.facebook && <a href={data.facebook}>Facebook</a>}
              <span>  </span>
              {data.twitter && <a href={data.twitter}>Twitter</a>}
              <span>  </span>
              {data.instagram && <a href={data.instagram}>Instagram</a>}
              </div>
            </div>
          </div>
      </div>
      </div>
      }
    </div>
    );
  };

export default ShelterPage;