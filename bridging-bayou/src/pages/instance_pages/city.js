import "../../css/city-page.css"
import "../../App.css"
import {useState, useEffect} from 'react'
import { getCityInstance } from "../../requests/requests";
import { useParams, Link } from "react-router-dom";
import ChartWithLegend from '../../components/ChartWithLegend';

function formatLink(name, state){
  let ret = "https://www.google.com/maps/embed/v1/place?key=" + process.env.REACT_APP_MAP_KEY +"&q=" +
  name.replace(" ", "+") + "+" + state;
  return ret;
}

const colors = [
  //light pink
  'rgba(255 ,156 ,183, 0.6)',
  //teal
  'rgba(75, 210, 210, 0.6)',
  //blue
  'rgba(54, 100, 250, 0.6)',
  //purple
  'rgba(153, 102, 255, 0.6)',
  //dark green
  'rgba(33 ,103 ,94, 0.6)',
  //yellow
  'rgba(255, 205, 86, 0.6)',
  //maroon
  'rgba(160, 16, 68, 0.6)',
  //pink
  'rgba(230, 99, 132, 0.6)',
  //brown
  'rgba(100, 91, 91, 0.6)'
]

/* graph data format is as follows
graphData =  {
  pie: {
    datasets: [
      {
        data: [array of statistics]
        backgroundColor: [array of colors (corrresponds to legend)]
      }
    ]
  }
  key: [
    {
      label: label the corresponding data,
      color: color on the legend
    }
  ]
}
*/

function initRaceData(race_breakdown){
  if (race_breakdown) {
    let categories = race_breakdown.split("//");
    let data = [];
    let col = [];
    let population = 0.0;
    let ret = {
      pie: {
        datasets: [
        ]
      }
    };
    let legend = categories.map((elem, i) => {
      col.push(colors[i]);
      let temp = elem.split(" Alone:")
      if (temp.length != 2) {
        temp = elem.split(":");
      }
      population += parseFloat(temp[1]);
      data.push(parseFloat(temp[1]));
      return({
        label : temp[0],
        color : colors[i],
      })
    })
    legend.push({
      label: "Total population: " + population,
    })
    data = data.map((elem, i) => {
      const percent = Math.round(elem / population * 10000) / 100;
      legend[i].label = percent + "%- " + legend[i].label;
      return percent;
    });
    ret.pie.datasets.push({
      data: data,
      backgroundColor: col
    })
    ret.key = legend;
    return ret;
  }
}



// Template for displaying city information on the city's webpage
const CityPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);
  const [name] = useState(useParams()['name']);
  const [state] = useState(useParams()['state']);
  const [graphData, setGraphData] = useState(null); 

  useEffect(() => {
    async function getCityInfo() {
      const info = await getCityInstance(name, state);
      setData(info);
    }
    if (data == null && isLoading) {
      setIsLoading(false);
      getCityInfo();
      if (data == null) {
        setIsLoading(true);
      } 
    }

  }, [isLoading, data])

  useEffect(() => {
    if (data) {
      setGraphData(initRaceData(data.race_breakdown));
    }
  }, [data])


  const LoadedPage = () => {
    if (isLoading && !data && !graphData) {
      return <h1>Loading...</h1>
    } else {
      return (
        <div>
        <div>
            <div className="row">
              <div className="col-sm">
                <h1 className="display-3 text-white py-3">{name}, {state}</h1>
              </div>
            </div>
              
            <div className="row">
            <iframe src={formatLink(name,state)} className="map-frame"
              allowfullscreen="yes" loading="lazy" referrerpolicy="no-referrer-when-downgrade"
              title={"Map of " + name}>
            </iframe>
            </div>
  
            <div className="row ">
              <div className="col align-items-center about-text">
                <h4 className="display-5 text-white">About</h4>
              </div>
  
              <div className="col city-text rounded-4">
            
                <div className="row text-start">
                  <div className="col align-items-start">
                  Location (Zip codes)
                  </div>
                  <div className="col">
                  {data.zip_codes.join(', ')}
                  </div>
                </div>
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  Population
                  </div>
                  <div className="col text-left">
                  {data.population}
                  </div>
                </div>
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  County
                  </div>
                  <div className="col text-left">
                  {data.counties}
                  </div>
                </div>
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  Poverty rate
                  </div>
                  <div className="col text-left">
                  {Math.round(data.poverty_rate * 10000)/100}%
                  </div>
                </div>
  
    
                <div className="row text-start">
                  <div className="col align-items-start">
                  Average Income
                  </div>
                  <div className="col text-left">
                  ${data.median_household_income}
                  </div>
                </div>
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  Number of shelters
                  </div>
                  <div className="col text-left">
                  {data.shelters.length}
                  </div>
                </div>
  
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  Number of food pantries
                  </div>
                  <div className="col text-left">
                  {data.pantries.length}
                  </div>
                </div>
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  Closest shelter
                  </div>

                  <div className="col text-left">
                  <div className="card-body">
                    {/* {(data.shelters[0] &&
                  <a href={'/pantry/'+data.shelters[0]} className="text-white">{data.shelters[0]}</a>) 
                  || "None"} */}
                  {data.shelters[0] && <Link to={`/shelter/${data.shelters[0]}`}><div className="btn btn-card m-1">{data.shelters[0]}</div></Link>
                  || <Link to={`/shelter/Project Lazarus`}><div className="btn btn-card m-1">{"Project Lazarus"}</div></Link>}
                  </div>
                  </div>
                </div>
  
                <div className="row text-start">
                  <div className="col align-items-start">
                  Closest food pantry
                  </div>
                  <div className="col text-left">
                  {data.pantries[0] && <Link to={`/pantry/${data.pantries[0]}`}><div className="btn btn-card m-1">{data.pantries[0]}</div></Link>
                  || <Link to={`/pantry/Mississippi Food Network`}><div className="btn btn-card m-1">{"Mississippi Food Network"}</div></Link>}
                  </div>
                </div>
  
              </div>
          </div>
        </div>
        {graphData && <ChartWithLegend data={graphData.pie} legendItems={graphData.key} name={"Race Demographics of " +name} />}
        </div>
      );
    }
  };
  
    return (
      <>
        {LoadedPage()}
      </>
    );
  };

export default CityPage;
