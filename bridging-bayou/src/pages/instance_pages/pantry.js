// Template for displaying shelter information on the shelter's webpage
import "../../css/pantry-page.css"
import "../../App.css"
import {useState, useEffect} from 'react'
import { useParams } from "react-router-dom";
import { getPantryInstance, getCityInstance, getShelterInstance } from "../../requests/requests";
import Map from '../../components/Map'
import {Link} from 'react-router-dom'


const PantryPage = () => {
  const [loadingDataStarted, setLoadingDataStarted] = useState(false);
  const [data, setData] = useState(null);
  const [name, setName] = useState(useParams()['name']);
  const [cityInfo, setCityInfo] = useState(null)
  const [shelterInfo, setShelterInfo] = useState("Bishop Ott Day Center - Night Shelter")
  const [shelterImages, setShelterImages] = useState(null)
  useEffect(() => {
      async function fetchPantryInfo() {
        let info = await getPantryInstance(name);
        console.log(info);
        // const new_photo = info.photo_url_1.split("&key=")[0]// + "key=" + process.env.REACT_APP_MAP_KEY
        
        // info.photo_url_1 = new_photo
        // console.log("changed link")
        // console.log(info.photo_url_1)

        setData(info)
        return info;
        // Fetch each shelter's images
      }

      async function fetchNearbyCity(pantry) {
        const item = await getCityInstance(pantry.city, pantry.state)
        setCityInfo(item)
          console.log("in fetch")
          console.log(item)
        return item;
        }
        
      

      async function fetchNearbyShelter(city) {
        let list = []
        console.log("in here")
        console.log(city)
        if (city.shelters.length == 0) {
          const shelter = await getShelterInstance("Shelter of Hope Salvation Army Meridian")
          console.log(shelter)
          list = [...list, shelter]
          

        } else {
          
          const shelter = await getShelterInstance(city.shelters[0])
          list = [...list, shelter]
        }
        console.log("shelters")
        console.log(list)
        setShelterInfo(list)

        return list
      }
    
  
      if (!loadingDataStarted) {
        setLoadingDataStarted(true);
        fetchPantryInfo().then((pantry)=> {
          fetchNearbyCity(pantry).then((city)=> {
            console.log(city)
            fetchNearbyShelter(city).then((shelter)=> {
              console.log(shelter)
            })
          })
        }).catch(err=>console.log(err))
      }
    }, [loadingDataStarted]);

    return (
      <div className="page">
        {/* Header banner with background image */}
        {!data ? 
        <div class="d-flex justify-content-center align-items-center ">
          <div class="spinner-border text-success" role="status">
          </div>
          </div> 
        : 
        <div>

          <div className="w-100" id="pantry-image">
            <div className="d-flex justify-content-md-center align-items-center p-6">
              <div>
                <h1 className="display-3 text-white py-5">{data.name}</h1>
              </div>
    
            </div>
          </div>

          <div className="row mt-6 shelter-about">
            <div className="col-sm p-3">
              {/* TODO: change to unique photo */}
              <img src={data.local_photo_url} className="pantry-image" alt="Scotland Healthy Food Pantry Street View Image" />

            </div>
            <div className="col-sm align-items-start pb-3">
              <h4 className="display-5 mt-5">Our mission</h4>
              {data.description || "This organization does not currently have a mission listed."}
              <h4 className="display-5 mt-4">Our resources</h4>
              

              <div className="row text-start">
                <div className="col align-items-start" white-space="pre-line">
                Hours of Operation
                </div>
                <div className="col text-left">
                  <div className="row">
                    <div className="col">
                    Monday
                    </div>
                    <div className="col">
                    {data.monday_hours || "Not available"}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      Tuesday
                    </div>
                    <div className="col">
                    {data.tuesday_hours || "Not available"}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                    Wednesday
                    </div>
                    <div className="col">
                    {data.wednesday_hours  || "Not available"}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                    Thursday
                    </div>
                    <div className="col">
                    {data.thursday_hours  || "Not available"}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                    Friday
                    </div>
                    <div className="col">
                    {data.friday_hours  || "Not available"}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                    Saturday
                    </div>
                    <div className="col">
                    {data.saturday_hours  || "Not available"}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                    Sunday
                    </div>
                    <div className="col">
                    {data.sunday_hours  || "Not available"}
                    </div>
                  </div>
                </div>
              </div>
            
              <div className="row text-start">
                <div className="col align-items-start">
                Nearby Cities
                </div>
                <div className="col text-left">
                  <Link to={`/city/${data.city}/${data.state}`}><div className="btn btn-card">{data.city}</div></Link>

                </div>
              </div>

              <div className="row text-start">
                <div className="col align-items-start">
                Nearby Shelters
                </div>
                <div 
                className="col text-left">
                  <Link to={`/shelter/${shelterInfo[0].name}`}><div className="btn btn-card my-2">{shelterInfo[0].name}</div></Link>

                </div>
              </div>
              <br></br>
              <div 
                className="center">
                  Before your visit, please contact the pantry directly to ensure you have the most up-to-date information.
              </div>
          </div>
        </div>


        {/* Contains info about the shelter and services they provide */}
        <span className="g-4">
           </span>
        <div className="row p-3 m-6">
          
          <div className="col align-items-center p-3 shelter-contact">
          <br></br>
            <div>
            <h4 className="display-5 text-black">Contact Us</h4>
              

              <div className="row text-start">
                <div className="col align-items-start">
                Location
                </div>
                <div className="col text-left">
                {data.address ? data.address : "Not available"}
                </div>
              </div>

              <div className="row text-start">
                <div className="col align-items-start">
                  Phone number
                </div>
                <div className="col text-left">
                {data.phone_number ? <a href={data.phone_number}>{data.phone_number}</a> : "Not available"}
                </div>
              </div>
              
              <div className="row text-start">
                <div className="col align-items-start">
                  Contact person
                </div>
                <div className="col text-left">
                {data.contact_person ? data.contact_person : "Not available"}
                </div>
              </div>
              <div className="row text-start">
                <div className="col align-items-start">
                  Email
                </div>
                <div className="col text-left">
                {data.email_address ? <a href={data.email_address}>{data.email_address}</a> : "Not available"}
                </div>
              </div>
            </div>
            
            </div>

              

          <div className="col align-items-center p-3">
            <Map data={data}locationName={data.name} address={data.address} city={data.city} state={data.state}/>
          </div>
        </div> 
        </div>
        }
      </div>
    );
  };

export default PantryPage;