import React from 'react';
import { useState, useEffect } from 'react';
import CityCard from '../../components/card_templates/CityCard'
import MapDropdown from "../../components/dropdown_templates/MapDropdown"
import SortOrderDropdown from "../../components/dropdown_templates/SortOrderDropdown"

import Filter from '../../components/dropdown_templates/Filter';

import "../../css/card-instance.css"
import "../../css/model-landing-pages.css"
import "../../css/dropdown.css"
import "../../App.css"

import { getCityBriefs, getCityInstance, getQueryString } from '../../requests/requests';
import DisplayCards from '../../components/DisplayCards';

const Cities = () => {
  const [loadingDataStarted, setLoadingDataStarted] = useState(false);
  const [cityBriefs, setCityBriefs] = useState([]);
  const [cityImages, setCityImages] = useState({});
  const [sort, setSort] = useState(""); // "" or "category_name"
  const [desc, setDesc] = useState(false); // bool
  const [pantryFilter, setPantryFilter] = useState(""); // "true", "false", or ""
  const [shelterFilter, setShelterFilter] = useState(""); // same
  const [filter, setFilter] = useState({items:[{"Pantries":"", "Shelters":""}]})
  const items = [
        {"key": "Pantries", "options" : [{"key":"Available","value":"true"}, {"key":"Unavailable", "value":"false"}], "text": "Pantries"}, 
        {"key" : "Shelters", "options" : [{"key":"Available","value":"true"}, {"key":"Unavailable", "value":"false"}], "text" : "Shelters"}]

  useEffect(() => {
    async function fetchCityBriefs() {
      
 
      let query = getQueryString(sort, desc, {"has_pantry":filter.items[0].Pantries, "has_shelter":filter.items[0].Shelters})
      const briefs = await getCityBriefs(query);
      setCityBriefs(briefs);
      return briefs;
      // Fetch each shelter's images
    }
    async function fetchCityImages(cities) {
      const images =  {}
      await cities.map((city)=> (
  
        getCityInstance(city.city, city.state).then((item)=> {
          images[city.city] = item["photo_url_1"]
        })
      ))
      console.log(images)
      setCityImages(images)
      return images
    }

    if(!loadingDataStarted) {
      setLoadingDataStarted(true);
      fetchCityBriefs().then((briefs)=> {
        fetchCityImages(briefs)
      }).catch((error)=> console.log(error))
    }
  }, [loadingDataStarted, cityBriefs]);
  
  
  return (
  <div className="background-page">
    {/* Background image for page, prevents it from scrolling*/}
    <div className="w-100" id="city-image">
      <h1 className="display-3 model-header py-5">Cities in Delta region</h1>
    </div>


    {/* Grid style for instance cards. */}

    <div className="container p-4">
      {/* TODO: displayCards component, pass cities as props */}
      <div className="d-flex justify-content-end px-10 gap-1">
          <MapDropdown 
            title="Sort by"
            items={{"City (alphabetically)":"city", 
                    "State (alphabetically)":"state", 
                    "Population":"population", 
                    "Poverty Rate":"poverty_rate", 
                    "Median Income":"median_household_income"}}
            setLoading={setLoadingDataStarted}
            setSortOrder={setDesc}
            setFilter={setSort} />

          <SortOrderDropdown 
            setSortOrder={setDesc}
            setLoading={setLoadingDataStarted} />
          <Filter 
          selected={filter}
          setSelected={setFilter}
          setLoading={setLoadingDataStarted}
          items={items}
           />
      </div>
      {loadingDataStarted && cityBriefs.length != 0 && cityImages.length != 0 && <DisplayCards cards={cityBriefs} CardComponent={CityCard} cardsPerPage={12} cardImages={cityImages} setCardImages={setCityImages}/>}
    </div>

  </div>

  );
};
  

export default Cities;
