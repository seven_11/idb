
import React from 'react';
import {useState, useEffect} from 'react';
import ShelterCard from '../../components/card_templates/ShelterCard'

import "../../css/card-instance.css"
import "../../css/model-landing-pages.css"
import "../../App.css"
import MapDropdown from '../../components/dropdown_templates/MapDropdown';
import SortOrderDropdown from '../../components/dropdown_templates/SortOrderDropdown';
import { getShelterBriefs, getShelterInstance, getQueryString } from '../../requests/requests';
import DisplayCards from '../../components/DisplayCards';
import Filter from '../../components/dropdown_templates/Filter';
import "../../css/dropdown.css"
const Shelters = () => {
  const [loadingDataStarted, setLoadingDataStarted] = useState(false);
  const [shelterBriefs, setShelterBriefs] = useState([]);
  const [sort, setSort] = useState("");
  const [desc, setDesc] = useState(false);

  const [filter, setFilter] = useState({items:[{"Laundry":"", "Men":"", "Women":""}]})
  const items = [
        {"key": "Laundry", "options" : [{"key":"Available","value":"true"}, {"key":"Unavailable", "value":"false"}], "text": "Laundry Service"}, 
        {"key" : "Men", "options" : [{"key":"Available","value":"true"}, {"key":"Unavailable", "value":"false"}], "text" : "Serves Men"},
        {"key" : "Women", "options" : [{"key":"Available","value":"true"}, {"key":"Unavailable", "value":"false"}], "text" : "Serves Women"}]
      

  useEffect(() => {
    async function fetchShelterBriefs() {
      console.log(filter)
      let query = getQueryString(sort, desc, 
        {
          "has_laundry_services":filter.items[0].Laundry,
          "serves_men": filter.items[0].Men,
          "serves_women": filter.items[0].Women
        })
      const briefs = await getShelterBriefs(query);
      setShelterBriefs(briefs);
      return briefs;
    }
    if(!loadingDataStarted) {
      setLoadingDataStarted(true);
      fetchShelterBriefs().catch((error)=> console.log(error));
    }
  }, [loadingDataStarted, shelterBriefs]);

  return (
  <div className="background-page">
    {/* Background image for page, prevents it from scrolling*/}
    <div className="w-100" id="shelter-image">
      <h1 className="display-3 model-header py-5">Shelters in Delta region</h1>
    </div>

    {/* Grid style for instance cards. */}
    <div className="container p-4">
      {/* TODO: displayCards component, pass shelters as props */}
      <div className="d-flex justify-content-end px-10 gap-1">

          <MapDropdown 
            title="Sort"
            items={{"Name":"name", "City":"city", "State":"state"}}
            setLoading={setLoadingDataStarted}
            setFilter={setSort} />
  
          <SortOrderDropdown 
            setSortOrder={setDesc}
            setLoading={setLoadingDataStarted} />

          <Filter 
            selected={filter}
            setSelected={setFilter}
            setLoading={setLoadingDataStarted}
            items={items} />
        </div>
      {loadingDataStarted && shelterBriefs.length !== 0 && <DisplayCards cards={shelterBriefs} CardComponent={ShelterCard} cardsPerPage={12}/>}
    </div>
  </div>
  );
};
export default Shelters;
