
import React from 'react';
import {useState, useEffect} from 'react';
import PantryCard from '../../components/card_templates/PantryCard'
import "../../css/card-instance.css"
import "../../css/model-landing-pages.css"
import "../../App.css"
import { getPantryBriefs, getPantryInstance, getQueryString } from '../../requests/requests';
import DisplayCards from '../../components/DisplayCards';
import MapDropdown from '../../components/dropdown_templates/MapDropdown';
import SortOrderDropdown from '../../components/dropdown_templates/SortOrderDropdown';
import Filter from '../../components/dropdown_templates/Filter';
//<h1>Food Pantries</h1>
const FoodPantries = () => {
  const [loadingDataStarted, setLoadingDataStarted] = useState(false);
  const [pantryBriefs, setPantryBriefs] = useState([]);
  const [pantryImages, setPantryImages] = useState({})
  const [sort, setSort] = useState("");
  const [desc, setDesc] = useState(false);
  const [mondayFilter, setMondayFilter] = useState("");
  const [tuesdayFilter, setTuesdayFilter] = useState("");
  const [wednesdayFilter, setWednesdayFilter] = useState("");
  const [thursdayFilter, setThursdayFilter] = useState("");
  const [fridayFilter, setFridayFilter] = useState("");
  const [saturdayFilter, setSaturdayFilter] = useState("");
  const [sundayFilter, setSundayFilter] = useState("");

  const [filter, setFilter] = useState({items:[{"Mondays": "", "Tuesdays":"","Wednesdays":"", "Thursdays":"","Fridays":"",
      "Saturdays":"","Sundays":""}]})
  const items = [
        {"key": "Mondays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text": "Mondays"}, 
        {"key" : "Tuesdays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text" : "Tuesdays"},
        {"key" : "Wednesdays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text" : "Wednesdays"},
        {"key" : "Thursdays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text" : "Thursdays"},
        {"key" : "Fridays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text" : "Fridays"},
        {"key" : "Saturdays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text" : "Saturdays"},
        {"key" : "Sundays", "options" : [{"key":"Open","value":"true"}, {"key":"Closed", "value":"false"}], "text" : "Sundays"}
        ]

      
  
      
      
      

  useEffect(() => {
    async function fetchPantryBriefs() {
      console.log(filter)
      let query = getQueryString(sort, desc, 
        {"open_mondays": filter.items[0].Mondays, 
         "open_tuesdays": filter.items[0].Tuesdays,
         "open_wednesdays": filter.items[0].Wednesdays,
         "open_thursdays":  filter.items[0].Thursdays,
         "open_fridays": filter.items[0].Fridays,
         "open_saturdays": filter.items[0].Saturdays,
         "open_sundays": filter.items[0].Sundays});
      const briefs = await getPantryBriefs(query);
      setPantryBriefs(briefs);
      console.log(briefs)
      return briefs;
      // Fetch each shelter's images
    }
    async function fetchPantryImages(pantries) {
      const images =  {}
      // await pantries.map((pantry)=> (
        /* TODO: save image URL of current pantry, debugging */
      //  getPantryInstance(pantry.name).then((item)=> {
      //     images[pantry.name] = item["photo_url_1"]
      //    })
      // ))
      console.log(images)
      setPantryImages(images)
      return images
    }


    if(!loadingDataStarted) {
        setLoadingDataStarted(true);
        fetchPantryBriefs()
    }

    // if (shelterBriefs) {
    //   fetchShelterImages();
    //   console.log(shelterImages)
    // }
  }, [loadingDataStarted, pantryBriefs]);

  return (
    <div className="background-page">
    {/* Background image for page, prevents it from scrolling*/}
    <div className="w-100" id="pantry-image">
      <h1 className="display-3 model-header py-5">Food Pantries in Delta region</h1>
    </div>
    {/* Grid style for instance cards. */}
    {/* TODO: make images same size, and add overlay to background image*/}
    <div className="container p-4">
        <div className="d-flex justify-content-end px-10 gap-1">
          <MapDropdown 
            title="Sort"
            items={{"Name":"name", "City":"city", "State":"state"}}
            setLoading={setLoadingDataStarted}
            setFilter={setSort} />
          <SortOrderDropdown 
            setSortOrder={setDesc}
            setLoading={setLoadingDataStarted} />
          <Filter 
            selected={filter}
            setSelected={setFilter}
            setLoading={setLoadingDataStarted}
            items={items}
            />
        </div>

      {loadingDataStarted && pantryBriefs.length != 0 && <DisplayCards cards={pantryBriefs} CardComponent={PantryCard} cardsPerPage={12}/>}
    </div>
  </div>
  );
};
  
export default FoodPantries;