import * as Plot from "@observablehq/plot";
import { useEffect, useRef, useState } from "react";
import PulseLoader from "react-spinners/PulseLoader";

//we will cache provider data here:
let data1Obj = undefined;
let data2Obj = undefined;
let data3Obj = undefined;

async function getData1() {
  if(data1Obj !== undefined) return getCached(data1Obj);
  const response = await fetch(`https://api.nativeaidnetwork.me/agencies?limit=100`);
  const responseData = await response.json();
  for (const point of responseData.results) {
    point.numTribes = point.tribes.length;
    point['State'] = point.state.name;
  }
  return responseData;
}

async function getData2() {
  if(data2Obj !== undefined) return getCached(data2Obj);
  const response = await fetch(`https://api.nativeaidnetwork.me/tribes?limit=600`);
  const responseData = await response.json();
  //since responseData is really big (there are ~600 tribes),
  //put the data into another object and let responseData be garbage collected.
  const realData = [];
  const stateToNumTribes = {};

  if(responseData) {
    for (const point of responseData.results) {
      if(point.state) {
        const stateAbbreviation = point.state.abbreviation;
        if(stateToNumTribes[stateAbbreviation] === undefined)
          stateToNumTribes[stateAbbreviation] = 0;
        stateToNumTribes[stateAbbreviation] += 1;
      }
    }
    for(const stateAbbreviation in stateToNumTribes) {
      realData.push({
        'State': stateAbbreviation,
        'numTribes': stateToNumTribes[stateAbbreviation]
      });
    }
  }
  return realData;
}

async function getData3() {
  if(data3Obj !== undefined) return getCached(data3Obj);
  const response = await fetch(`https://api.nativeaidnetwork.me/reservations?limit=400`);
  const responseData = await response.json();
  //since responseData is really big (there are ~300 reservations),
  //put the data into another object and let responseData be garbage collected.
  const realData = []

  if(responseData) {
    for (const point of responseData.results) {
      if(point.population && point.total_area) {
        realData.push({
          squareMiles: point.total_area,
          population: point.population,
          'Name': point.name
        });
      }
    }
  }
}

async function getCached(dataObj) {
  await new Promise(resolve => setTimeout(resolve, Math.random() * 1500 + 600)); //wait
  return dataObj;
}

const Loader = () => {
  return (
    <PulseLoader 
      color={"gray"}
      loading={true}
      // cssOverride={override}
      size={20}
      speedMultiplier={0.5}
      aria-label="Loading Spinner"
      data-testid="loader"
    />
  );
};

data1Obj={"results": [{"name": "Minnesota Agency","numTribes": 1,"State": "Minnesota"},{"name": "Northern Idaho Agency","numTribes": 2,"State": "Idaho"},{"name": "Central California Agency","numTribes": 55,"State": "California"},{"name": "Seminole Agency","numTribes": 1,"State": "Florida"},{"name": "Northern Cheyenne Agency","numTribes": 1,"State": "Montana"},{"name": "Spokane Agency","numTribes": 2,"State": "Washington"},{"name": "Colville Agency","numTribes": 1,"State": "Washington"},{"name": "Alaska Regional Office","numTribes": 175,"State": "Alaska"},{"name": "Flathead Agency","numTribes": 1,"State": "Montana"},{"name": "Pawnee Agency","numTribes": 3,"State": "Oklahoma"},{"name": "Olympic Peninsula Agency","numTribes": 7,"State": "Washington"},{"name": "Rocky Mountain Regional Office","numTribes": 1,"State": "Montana"},{"name": "Yakama Agency","numTribes": 1,"State": "Washington"},{"name": "Crow Agency","numTribes": 1,"State": "Montana"},{"name": "Cherokee Agency","numTribes": 1,"State": "North Carolina"},{"name": "Fort Yuma Agency","numTribes": 2,"State": "Arizona"},{"name": "Horton Agency","numTribes": 3,"State": "Kansas"},{"name": "Hopi Agency","numTribes": 1,"State": "Arizona"},{"name": "Rocky Boy's Field Office","numTribes": 1,"State": "Montana"},{"name": "Tohono O'odham Agency","numTribes": 1,"State": "Arizona"},{"name": "Colorado River Agency","numTribes": 3,"State": "Arizona"},{"name": "Southern Paiute Agency","numTribes": 5,"State": "Utah"},{"name": "Eastern Nevada Agency","numTribes": 5,"State": "Nevada"},{"name": "Truxton Canon Agency","numTribes": 5,"State": "Arizona"},{"name": "Western Nevada Agency","numTribes": 11,"State": "Nevada"},{"name": "Western Regional Office","numTribes": 0,"State": "Arizona"},{"name": "Pima Agency","numTribes": 2,"State": "Arizona"},{"name": "San Carlos Agency","numTribes": 1,"State": "Arizona"},{"name": "Fort Apache Agency","numTribes": 1,"State": "Arizona"},{"name": "Salt River Agency","numTribes": 3,"State": "Arizona"},{"name": "Taholah Agency","numTribes": 1,"State": "Washington"},{"name": "Choctaw Agency","numTribes": 1,"State": "Mississippi"},{"name": "Navajo Regional Office","numTribes": 1,"State": "New Mexico"},{"name": "Osage Agency","numTribes": 1,"State": "Oklahoma"},{"name": "Southern California Agency","numTribes": 23,"State": "California"},{"name": "Palm Springs Agency","numTribes": 1,"State": "California"},{"name": "Pacific Regional Office","numTribes": 12,"State": "California"},{"name": "Blackfeet Agency","numTribes": 1,"State": "Montana"},{"name": "Northern California Agency","numTribes": 12,"State": "California"},{"name": "Ramah Navajo Agency","numTribes": 1,"State": "New Mexico"},{"name": "Southern Pueblos Agency","numTribes": 11,"State": "New Mexico"},{"name": "Northern Pueblos Agency","numTribes": 8,"State": "New Mexico"},{"name": "Southern Ute Agency","numTribes": 1,"State": "Colorado"},{"name": "Southwest Regional Office","numTribes": 0,"State": "New Mexico"},{"name": "Fort Peck Agency","numTribes": 1,"State": "Montana"},{"name": "Ute Mountain Ute Agency","numTribes": 1,"State": "Colorado"},{"name": "Zuni Agency","numTribes": 1,"State": "New Mexico"},{"name": "Fort Belknap Agency","numTribes": 1,"State": "Montana"},{"name": "Mescalero Agency","numTribes": 1,"State": "New Mexico"},{"name": "Jicarilla Agency","numTribes": 1,"State": "New Mexico"},{"name": "Southern Plains Regional Office","numTribes": 12,"State": "Oklahoma"},{"name": "Wind River Agency","numTribes": 2,"State": "Wyoming"},{"name": "Anadarko Agency","numTribes": 5,"State": "Oklahoma"},{"name": "Uintah &amp; Ouray Agency","numTribes": 2,"State": "Utah"},{"name": "Concho Agency","numTribes": 1,"State": "Oklahoma"},{"name": "Crow Creek Agency","numTribes": 1,"State": "South Dakota"},{"name": "Lower Brule Agency","numTribes": 1,"State": "South Dakota"},{"name": "Cheyenne River Agency","numTribes": 1,"State": "South Dakota"},{"name": "Wewoka Agency","numTribes": 1,"State": "Oklahoma"},{"name": "Okmulgee Agency","numTribes": 4,"State": "Oklahoma"},{"name": "Miami Agency","numTribes": 10,"State": "Oklahoma"},{"name": "Eastern Oklahoma Regional Office","numTribes": 3,"State": "Oklahoma"},{"name": "Chickasaw Agency","numTribes": 1,"State": "Oklahoma"},{"name": "Eastern Regional Office","numTribes": 33,"State": "Tennessee"},{"name": "Fairbanks Agency","numTribes": 50,"State": "Alaska"},{"name": "Juneau Office","numTribes": 0,"State": "Alaska"},{"name": "Northwest Regional Office","numTribes": 8,"State": "Oregon"},{"name": "Michigan Agency","numTribes": 12,"State": "Michigan"},{"name": "Chinle Navajo Agency","numTribes": 0,"State": "Arizona"},{"name": "Eastern Navajo Agency","numTribes": 0,"State": "New Mexico"},{"name": "Fort Defiance Agency","numTribes": 0,"State": "Arizona"},{"name": "Shiprock Agency","numTribes": 0,"State": "New Mexico"},{"name": "Western Navajo Agency","numTribes": 0,"State": "Arizona"},{"name": "Coeur d'Alene Agency","numTribes": 1,"State": "Idaho"},{"name": "Great Lakes Agency","numTribes": 10,"State": "Wisconsin"},{"name": "Yankton Agency","numTribes": 2,"State": "South Dakota"},{"name": "Fort Hall Agency","numTribes": 2,"State": "Idaho"},{"name": "Metlakatla Agency","numTribes": 1,"State": "Alaska"},{"name": "Winnebago Agency","numTribes": 3,"State": "Nebraska"},{"name": "Fort Berthold Agency","numTribes": 1,"State": "North Dakota"},{"name": "Turtle Mountain Agency","numTribes": 1,"State": "North Dakota"},{"name": "Puget Sound Agency","numTribes": 15,"State": "Washington"},{"name": "Standing Rock Agency","numTribes": 1,"State": "North Dakota"},{"name": "Umatilla Agency","numTribes": 1,"State": "Oregon"},{"name": "Warm Springs Agency","numTribes": 2,"State": "Oregon"},{"name": "Sisseton Agency","numTribes": 1,"State": "South Dakota"},{"name": "Rosebud Agency","numTribes": 1,"State": "South Dakota"},{"name": "Pine Ridge Agency","numTribes": 1,"State": "South Dakota"},{"name": "Midwest Regional Office","numTribes": 7,"State": "Minnesota"},{"name": "Great Plains Regional Office","numTribes": 1,"State": "South Dakota"},{"name": "Fort Totten Agency","numTribes": 1,"State": "North Dakota"}],"count": 91};
data2Obj=[{"State": "OR","numTribes": 9},{"State": "WA","numTribes": 29},{"State": "AK","numTribes": 178},{"State": "CA","numTribes": 101},{"State": "ME","numTribes": 5},{"State": "TX","numTribes": 3},{"State": "OK","numTribes": 37},{"State": "VA","numTribes": 6},{"State": "MI","numTribes": 12},{"State": "NE","numTribes": 4},{"State": "ND","numTribes": 4},{"State": "KS","numTribes": 4},{"State": "WI","numTribes": 11},{"State": "ID","numTribes": 4},{"State": "NM","numTribes": 22},{"State": "MT","numTribes": 8},{"State": "SD","numTribes": 8},{"State": "MN","numTribes": 6},{"State": "LA","numTribes": 4},{"State": "NY","numTribes": 8},{"State": "AZ","numTribes": 19},{"State": "WY","numTribes": 2},{"State": "UT","numTribes": 4},{"State": "SC","numTribes": 1},{"State": "MO","numTribes": 1},{"State": "NV","numTribes": 11},{"State": "CT","numTribes": 2},{"State": "CO","numTribes": 2},{"State": "MA","numTribes": 2},{"State": "IA","numTribes": 1},{"State": "FL","numTribes": 2},{"State": "AL","numTribes": 1},{"State": "MS","numTribes": 1},{"State": "RI","numTribes": 1},{"State": "NC","numTribes": 1}];
data3Obj=[{"squareMiles": 595.66,"population": 3011,"Name": "Acoma Pueblo Reservation"},{"squareMiles": 53.68,"population": 24781,"Name": "Agua Caliente Indian Reservation"},{"squareMiles": 12.54,"population": 608,"Name": "Alabama-Coushatta Reservation"},{"squareMiles": 48.51,"population": 6490,"Name": "Allegany Reservation"},{"squareMiles": 0.039,"population": 15,"Name": "Alturas Indian Rancheria"},{"squareMiles": 215.28,"population": 1460,"Name": "Annette Island Reserve"},{"squareMiles": 1.77,"population": 52,"Name": "Auburn Rancheria"},{"squareMiles": 0.88,"population": 11,"Name": "Augustine Reservation"},{"squareMiles": 196.64,"population": 1479,"Name": "Bad River Reservation"},{"squareMiles": 9.31,"population": 640,"Name": "Barona Reservation"},{"squareMiles": 1.05,"population": 148,"Name": "Battle Mountain Reservation"},{"squareMiles": 5.53,"population": 1014,"Name": "Bay Mills Reservation"},{"squareMiles": 0.57,"population": 76,"Name": "Benton Paiute Reservation"},{"squareMiles": 0.17,"population": 152,"Name": "Berry Creek Rancheria"},{"squareMiles": 0.069,"population": 9,"Name": "Big Bend Rancheria"},{"squareMiles": 82.38,"population": 591,"Name": "Big Cypress Reservation"},{"squareMiles": 0.0081,"population": 17,"Name": "Big Lagoon Rancheria"},{"squareMiles": 0.43,"population": 499,"Name": "Big Pine Reservation"},{"squareMiles": 0.39,"population": 118,"Name": "Big Sandy Rancheria"},{"squareMiles": 0.19,"population": 139,"Name": "Big Valley Rancheria"},{"squareMiles": 1.37,"population": 1588,"Name": "Bishop Reservation"},{"squareMiles": 2400.13,"population": 10405,"Name": "Blackfeet Indian Reservation"},{"squareMiles": 0.089,"population": 58,"Name": "Blue Lake Rancheria"},{"squareMiles": 211.89,"population": 874,"Name": "Bois Forte Reservation"},{"squareMiles": 0.054,"population": 35,"Name": "Bridgeport Reservation"},{"squareMiles": 57.15,"population": 694,"Name": "Brighton Reservation"},{"squareMiles": 18.97,"population": 128,"Name": "Burns Paiute Indian Colony"},{"squareMiles": 3,"population": 835,"Name": "Cabazon Reservation"},{"squareMiles": 28.93,"population": 187,"Name": "Cahuilla Reservation"},{"squareMiles": 2.59,"population": 443,"Name": "Campbell Ranch"},{"squareMiles": 25.76,"population": 362,"Name": "Campo Indian Reservation"},{"squareMiles": 0.28,"population": 242,"Name": "Carson Colony"},{"squareMiles": 1.58,"population": 841,"Name": "Catawba Reservation"},{"squareMiles": 34.41,"population": 2185,"Name": "Cattaraugus Reservation"},{"squareMiles": 0.054,"population": 15,"Name": "Cedarville Rancheria"},{"squareMiles": 7.38,"population": 649,"Name": "Chehalis Reservation"},{"squareMiles": 48.15,"population": 308,"Name": "Chemehuevi Reservation"},{"squareMiles": 4419.09,"population": 8090,"Name": "Cheyenne River Reservation"},{"squareMiles": 0.042,"population": 4,"Name": "Chicken Ranch Rancheria"},{"squareMiles": 0.7,"population": 555,"Name": "Chitimacha Reservation"},{"squareMiles": 10.07,"population": 817,"Name": "Cocopah Reservation"},{"squareMiles": 536.77,"population": 6760,"Name": "Coeur d'Alene Reservation"},{"squareMiles": 0.16,"population": 184,"Name": "Cold Springs Rancheria"},{"squareMiles": 464.14,"population": 8764,"Name": "Colorado River Indian Reservation"},{"squareMiles": 0.4,"population": 76,"Name": "Colusa Rancheria"},{"squareMiles": 2185.19,"population": 7687,"Name": "Colville Reservation"},{"squareMiles": 0.23,"population": 47,"Name": "Coos, Lower Umpqua, and Siuslaw Reservation"},{"squareMiles": 10.12,"population": 323,"Name": "Coquille Reservation"},{"squareMiles": 1.19,"population": 21,"Name": "Cortina Indian Rancheria"},{"squareMiles": 1.82,"population": 88,"Name": "Coushatta Reservation"},{"squareMiles": 5.43,"population": 104,"Name": "Cow Creek Reservation"},{"squareMiles": 0.14,"population": 144,"Name": "Coyote Valley Reservation"},{"squareMiles": 461.37,"population": 2010,"Name": "Crow Creek Reservation"},{"squareMiles": 3606.63,"population": 6863,"Name": "Crow Reservation"},{"squareMiles": 1.23,"population": 314,"Name": "Dresslerville Colony"},{"squareMiles": 452.6,"population": 1309,"Name": "Duck Valley Reservation"},{"squareMiles": 6.25,"population": 156,"Name": "Duckwater Reservation"},{"squareMiles": 0.42,"population": 99,"Name": "Elk Valley Rancheria"},{"squareMiles": 0.3,"population": 736,"Name": "Elko Colony"},{"squareMiles": 5.65,"population": 202,"Name": "Ely Reservation"},{"squareMiles": 0.066,"population": 1,"Name": "Enterprise Rancheria"},{"squareMiles": 13.04,"population": 620,"Name": "Fallon Paiute-Shoshone Reservation"},{"squareMiles": 3.5,"population": 418,"Name": "Flandreau Reservation"},{"squareMiles": 2057.93,"population": 28359,"Name": "Flathead Reservation"},{"squareMiles": 159.33,"population": 4250,"Name": "Fond du Lac Reservation"},{"squareMiles": 19.54,"population": 588,"Name": "Forest County Potawatomi Community"},{"squareMiles": 2631.21,"population": 13409,"Name": "Fort Apache Reservation"},{"squareMiles": 1018.36,"population": 2851,"Name": "Fort Belknap Reservation"},{"squareMiles": 1582.62,"population": 6341,"Name": "Fort Berthold Reservation"},{"squareMiles": 5.49,"population": 94,"Name": "Fort Bidwell Reservation"},{"squareMiles": 855.59,"population": 5767,"Name": "Fort Hall Reservation"},{"squareMiles": 0.87,"population": 93,"Name": "Fort Independence Reservation"},{"squareMiles": 54.39,"population": 334,"Name": "Fort McDermitt Indian Reservation"},{"squareMiles": 38.96,"population": 971,"Name": "Fort McDowell Yavapai Nation Reservation"},{"squareMiles": 52.73,"population": 1477,"Name": "Fort Mojave Reservation"},{"squareMiles": 3302,"population": 10008,"Name": "Fort Peck Indian Reservation"},{"squareMiles": 0.093,"population": 60,"Name": "Fort Pierce Reservation"},{"squareMiles": 70.32,"population": 2197,"Name": "Fort Yuma Indian Reservation"},{"squareMiles": 584.71,"population": 11712,"Name": "Gila River Indian Reservation"},{"squareMiles": 188.1,"population": 143,"Name": "Goshute Reservation"},{"squareMiles": 75.65,"population": 565,"Name": "Grand Portage Reservation"},{"squareMiles": 16.44,"population": 434,"Name": "Grand Ronde Community"},{"squareMiles": 1.25,"population": 608,"Name": "Grand Traverse Reservation"},{"squareMiles": 0.11,"population": 33,"Name": "Greenville Rancheria"},{"squareMiles": 0.14,"population": 164,"Name": "Grindstone Indian Rancheria"},{"squareMiles": 0.069,"population": 52,"Name": "Guidiville Rancheria"},{"squareMiles": 9.44,"population": 523,"Name": "Hannahville Indian Community"},{"squareMiles": 275.83,"population": 465,"Name": "Havasupai Reservation"},{"squareMiles": 11.05,"population": 1375,"Name": "Ho-Chunk Nation Reservation"},{"squareMiles": 0.7,"population": 116,"Name": "Hoh Indian Reservation"},{"squareMiles": 0.79,"population": 1742,"Name": "Hollywood Reservation"},{"squareMiles": 141.68,"population": 3041,"Name": "Hoopa Valley Reservation"},{"squareMiles": 2533.12,"population": 7185,"Name": "Hopi Reservation"},{"squareMiles": 0.25,"population": 38,"Name": "Hopland Rancheria"},{"squareMiles": 1.4,"population": 213,"Name": "Houlton Maliseet Reservation"},{"squareMiles": 1604.6,"population": 1335,"Name": "Hualapai Indian Reservation"},{"squareMiles": 0.33,"population": 52,"Name": "Huron Potawatomi Reservation"},{"squareMiles": 0.98,"population": 127,"Name": "Immokalee Reservation"},{"squareMiles": 44.48,"population": 718,"Name": "Indian Township Reservation"},{"squareMiles": 19.9,"population": 166,"Name": "Iowa Reservation"},{"squareMiles": 218.33,"population": 26274,"Name": "Isabella Reservation"},{"squareMiles": 330.98,"population": 3400,"Name": "Isleta Pueblo"},{"squareMiles": 0.21,"population": 11,"Name": "Jamestown S'Klallam Reservation"},{"squareMiles": 139.66,"population": 1815,"Name": "Jemez Pueblo"},{"squareMiles": 1374.06,"population": 3254,"Name": "Jicarilla Apache Nation Reservation"},{"squareMiles": 189.75,"population": 240,"Name": "Kaibab Indian Reservation"},{"squareMiles": 10.56,"population": 231,"Name": "Kalispel Reservation"},{"squareMiles": 1.52,"population": 506,"Name": "Karuk Reservation"},{"squareMiles": 236.83,"population": 4134,"Name": "Kickapoo Reservation"},{"squareMiles": 0.19,"population": 366,"Name": "Kickapoo Reservation"},{"squareMiles": 0.5,"population": 26,"Name": "Klamath Reservation"},{"squareMiles": 3.18,"population": 82,"Name": "Kootenai Reservation"},{"squareMiles": 13.5,"population": 476,"Name": "La Jolla Reservation"},{"squareMiles": 6.39,"population": 55,"Name": "La Posta Indian Reservation"},{"squareMiles": 124.26,"population": 2803,"Name": "Lac Courte Oreilles Reservation"},{"squareMiles": 135.21,"population": 3442,"Name": "Lac du Flambeau Reservation"},{"squareMiles": 0.39,"population": 137,"Name": "Lac Vieux Desert Reservation"},{"squareMiles": 789.16,"population": 4043,"Name": "Laguna Pueblo"},{"squareMiles": 1508.73,"population": 10922,"Name": "Lake Traverse Reservation"},{"squareMiles": 110.06,"population": 3703,"Name": "L'Anse Reservation"},{"squareMiles": 6.24,"population": 154,"Name": "Las Vegas Indian Colony"},{"squareMiles": 0.31,"population": 212,"Name": "Laytonville Rancheria"},{"squareMiles": 1310.57,"population": 10660,"Name": "Leech Lake Reservation"},{"squareMiles": 1.79,"population": 57,"Name": "Little River Reservation"},{"squareMiles": 1.13,"population": 51,"Name": "Little Traverse Bay Reservation"},{"squareMiles": 0.37,"population": 212,"Name": "Lone Pine Reservation"},{"squareMiles": 0.062,"population": 11,"Name": "Lookout Rancheria"},{"squareMiles": 39.21,"population": 98,"Name": "Los Coyotes Reservation"},{"squareMiles": 0.032,"population": 88,"Name": "Lovelock Indian Colony"},{"squareMiles": 389.56,"population": 1505,"Name": "Lower Brule Reservation"},{"squareMiles": 2.15,"population": 609,"Name": "Lower Elwha Reservation"},{"squareMiles": 2.68,"population": 419,"Name": "Lower Sioux Indian Community"},{"squareMiles": 36.68,"population": 4706,"Name": "Lummi Reservation"},{"squareMiles": 46.97,"population": 1414,"Name": "Makah Indian Reservation"},{"squareMiles": 0.59,"population": 212,"Name": "Manchester-Point Arena Rancheria"},{"squareMiles": 7.17,"population": 78,"Name": "Manzanita Reservation"},{"squareMiles": 32.78,"population": 1001,"Name": "Maricopa Ak Chin Indian Reservation"},{"squareMiles": 2.55,"population": 299,"Name": "Mashantucket Pequot Reservation"},{"squareMiles": 0.12,"population": 65,"Name": "Mattaponi Reservation"},{"squareMiles": 362.82,"population": 3141,"Name": "Menominee Reservation"},{"squareMiles": 2.73,"population": 98,"Name": "Mesa Grande Reservation"},{"squareMiles": 719.06,"population": 3613,"Name": "Mescalero Reservation"},{"squareMiles": 136.12,"population": 406,"Name": "Miccosukee Reservation"},{"squareMiles": 0.19,"population": 56,"Name": "Middletown Rancheria"},{"squareMiles": 103.23,"population": 4907,"Name": "Mille Lacs Reservation"},{"squareMiles": 47.05,"population": 7436,"Name": "Mississippi Choctaw Reservation"},{"squareMiles": 110.97,"population": 260,"Name": "Moapa River Indian Reservation"},{"squareMiles": 0.8,"population": 48,"Name": "Mohegan Reservation"},{"squareMiles": 0.12,"population": 12,"Name": "Montgomery Creek Rancheria"},{"squareMiles": 0.46,"population": 181,"Name": "Mooretown Rancheria"},{"squareMiles": 53.6,"population": 913,"Name": "Morongo Reservation"},{"squareMiles": 6.14,"population": 3870,"Name": "Muckleshoot Reservation"},{"squareMiles": 32.4,"population": 1611,"Name": "Nambe Pueblo"},{"squareMiles": 27451.5,"population": 173667,"Name": "Navajo Nation"},{"squareMiles": 1204.25,"population": 18437,"Name": "Nez Perce Reservation"},{"squareMiles": 8.22,"population": 575,"Name": "Nisqually Reservation"},{"squareMiles": 4.49,"population": 884,"Name": "Nooksack Reservation"},{"squareMiles": 0.36,"population": 60,"Name": "North Fork Rancheria"},{"squareMiles": 707.12,"population": 4789,"Name": "Northern Cheyenne Indian Reservation"},{"squareMiles": 26.71,"population": 6309,"Name": "Ohkay Owingeh"},{"squareMiles": 0.97,"population": 1,"Name": "Oil Springs Reservation"},{"squareMiles": 309.99,"population": 4773,"Name": "Omaha Reservation"},{"squareMiles": 102.31,"population": 22776,"Name": "Oneida Reservation"},{"squareMiles": 0.081,"population": 25,"Name": "Oneida Nation Reservation"},{"squareMiles": 9.29,"population": 468,"Name": "Onondaga Nation Reservation"},{"squareMiles": 2303.98,"population": 47472,"Name": "Osage Reservation"},{"squareMiles": 50.82,"population": 273,"Name": "Paiute Reservation"},{"squareMiles": 20.35,"population": 1315,"Name": "Pala Reservation"},{"squareMiles": 2.46,"population": 73,"Name": "Pamunkey Reservation"},{"squareMiles": 2.2,"population": 3484,"Name": "Pascua Pueblo Yaqui Reservation"},{"squareMiles": 9.36,"population": 206,"Name": "Pauma and Yuima Reservation"},{"squareMiles": 7.02,"population": 346,"Name": "Pechanga Reservation"},{"squareMiles": 175.67,"population": 631,"Name": "Penobscot Reservation"},{"squareMiles": 0.31,"population": 69,"Name": "Picayune Rancheria"},{"squareMiles": 27.36,"population": 1886,"Name": "Picuris Pueblo"},{"squareMiles": 4353.8,"population": 18834,"Name": "Pine Ridge Reservation"},{"squareMiles": 0.16,"population": 129,"Name": "Pinoleville Rancheria"},{"squareMiles": 0.97,"population": 749,"Name": "Pleasant Point Reservation"},{"squareMiles": 0.63,"population": 287,"Name": "Poarch Creek Reservation"},{"squareMiles": 4.64,"population": 29,"Name": "Pokagon Reservation"},{"squareMiles": 1.88,"population": 682,"Name": "Port Gamble Reservation"},{"squareMiles": 11.65,"population": 7640,"Name": "Port Madison Reservation"},{"squareMiles": 121.58,"population": 1469,"Name": "Prairie Band of Potawatomi Nation Reservation"},{"squareMiles": 2.83,"population": 217,"Name": "Prairie Island Indian Community"},{"squareMiles": 82.12,"population": 1727,"Name": "Pueblo de Cochiti"},{"squareMiles": 21.41,"population": 3316,"Name": "Pueblo of Pojoaque"},{"squareMiles": 29.42,"population": 46816,"Name": "Puyallup Reservation"},{"squareMiles": 729.52,"population": 1660,"Name": "Pyramid Lake Paiute Reservation"},{"squareMiles": 81.71,"population": 9018,"Name": "Qualla Boundary"},{"squareMiles": 1.12,"population": 187,"Name": "Quartz Valley Reservation"},{"squareMiles": 1.61,"population": 460,"Name": "Quileute Reservation"},{"squareMiles": 324.08,"population": 1408,"Name": "Quinault Reservation"},{"squareMiles": 0.85,"population": 13,"Name": "Ramona Village"},{"squareMiles": 22.92,"population": 1123,"Name": "Red Cliff Reservation"},{"squareMiles": 1258.33,"population": 5896,"Name": "Red Lake Reservation"},{"squareMiles": 0.042,"population": 34,"Name": "Redding Rancheria"},{"squareMiles": 0.42,"population": 238,"Name": "Redwood Valley Rancheria"},{"squareMiles": 3.36,"population": 919,"Name": "Reno-Sparks Indian Colony"},{"squareMiles": 0.34,"population": 31,"Name": "Resighini Rancheria"},{"squareMiles": 6.16,"population": 1215,"Name": "Rincon Reservation"},{"squareMiles": 0.13,"population": 14,"Name": "Roaring Creek Rancheria"},{"squareMiles": 0.32,"population": 207,"Name": "Robinson Rancheria"},{"squareMiles": 171.34,"population": 3323,"Name": "Rocky Boy's Reservation"},{"squareMiles": 0.069,"population": 38,"Name": "Rohnerville Rancheria"},{"squareMiles": 1975.42,"population": 10869,"Name": "Rosebud Indian Reservation"},{"squareMiles": 36.23,"population": 401,"Name": "Round Valley Reservation"},{"squareMiles": 0.76,"population": 77,"Name": "Rumsey Indian Rancheria"},{"squareMiles": 23.66,"population": 173,"Name": "Sac and Fox Nation Reservation"},{"squareMiles": 9.86,"population": 1062,"Name": "Sac and Fox/Meskwaki Settlement"},{"squareMiles": 85.39,"population": 6289,"Name": "Salt River Reservation"},{"squareMiles": 2926.92,"population": 10068,"Name": "San Carlos Reservation"},{"squareMiles": 80.01,"population": 3563,"Name": "San Felipe Pueblo"},{"squareMiles": 47.31,"population": 1752,"Name": "San Ildefonso Pueblo"},{"squareMiles": 1.05,"population": 112,"Name": "San Manuel Reservation"},{"squareMiles": 2.24,"population": 1097,"Name": "San Pasqual Reservation"},{"squareMiles": 38.89,"population": 4965,"Name": "Sandia Pueblo"},{"squareMiles": 101.05,"population": 621,"Name": "Santa Ana Pueblo"},{"squareMiles": 77.1,"population": 11021,"Name": "Santa Clara Pueblo"},{"squareMiles": 0.63,"population": 652,"Name": "Santa Rosa Rancheria"},{"squareMiles": 17.06,"population": 71,"Name": "Santa Rosa Reservation"},{"squareMiles": 0.24,"population": 271,"Name": "Santa Ynez Reservation"},{"squareMiles": 23.42,"population": 330,"Name": "Santa Ysabel Reservation"},{"squareMiles": 184.51,"population": 901,"Name": "Santee Reservation"},{"squareMiles": 106.34,"population": 3255,"Name": "Santo Domingo Pueblo"},{"squareMiles": 0.073,"population": 71,"Name": "Sauk-Suiattle Reservation"},{"squareMiles": 1.98,"population": 1747,"Name": "Sault Ste. Marie Reservation"},{"squareMiles": 2.49,"population": 658,"Name": "Shakopee Mdewakanton Sioux Community"},{"squareMiles": 0.77,"population": 168,"Name": "Sherwood Valley Rancheria"},{"squareMiles": 0.27,"population": 102,"Name": "Shingle Springs Rancheria"},{"squareMiles": 1.35,"population": 662,"Name": "Shinnecock Reservation"},{"squareMiles": 1.31,"population": 82,"Name": "Shoalwater Bay Indian Reservation"},{"squareMiles": 6.87,"population": 506,"Name": "Siletz Reservation"},{"squareMiles": 8.43,"population": 730,"Name": "Skokomish Reservation"},{"squareMiles": 28.16,"population": 23,"Name": "Skull Valley Reservation"},{"squareMiles": 0.31,"population": 113,"Name": "Smith River Rancheria"},{"squareMiles": 10.8,"population": 482,"Name": "Soboba Reservation"},{"squareMiles": 5.22,"population": 414,"Name": "Sokaogon Chippewa Community"},{"squareMiles": 26.56,"population": 122,"Name": "South Fork Reservation"},{"squareMiles": 1063.41,"population": 12153,"Name": "Southern Ute Reservation"},{"squareMiles": 399.41,"population": 4238,"Name": "Spirit Lake Reservation"},{"squareMiles": 250.44,"population": 2096,"Name": "Spokane Reservation"},{"squareMiles": 3.38,"population": 431,"Name": "Squaxin Island Reservation"},{"squareMiles": 3.81,"population": 768,"Name": "St. Croix Reservation"},{"squareMiles": 20.99,"population": 3228,"Name": "St. Regis Mohawk Reservation"},{"squareMiles": 3662.63,"population": 8217,"Name": "Standing Rock Reservation"},{"squareMiles": 4.42,"population": 147,"Name": "Stewart Community"},{"squareMiles": 0.066,"population": 78,"Name": "Stewarts Point Rancheria"},{"squareMiles": 0.35,"population": 4,"Name": "Stillaguamish Reservation"},{"squareMiles": 23.87,"population": 644,"Name": "Stockbridge Munsee Community"},{"squareMiles": 0.085,"population": 61,"Name": "Sulphur Bank Rancheria"},{"squareMiles": 19.73,"population": 1,"Name": "Summit Lake Reservation"},{"squareMiles": 1.67,"population": 549,"Name": "Susanville Indian Rancheria"},{"squareMiles": 21.02,"population": 3010,"Name": "Swinomish Reservation"},{"squareMiles": 1.28,"population": 211,"Name": "Sycuan Reservation"},{"squareMiles": 0.12,"population": 103,"Name": "Table Bluff Reservation"},{"squareMiles": 0.21,"population": 64,"Name": "Table Mountain Rancheria"},{"squareMiles": 156.2,"population": 4384,"Name": "Taos Pueblo"},{"squareMiles": 26.93,"population": 841,"Name": "Tesuque Pueblo"},{"squareMiles": 12.79,"population": 24,"Name": "Timbi-Sha Shoshone Reservation"},{"squareMiles": 4453.83,"population": 10201,"Name": "Tohono O'odham Nation Reservation"},{"squareMiles": 11.87,"population": 517,"Name": "Tonawanda Reservation"},{"squareMiles": 0.13,"population": 120,"Name": "Tonto Apache Reservation"},{"squareMiles": 49.26,"population": 5594,"Name": "Torres-Martinez Reservation"},{"squareMiles": 0.13,"population": 132,"Name": "Trinidad Rancheria"},{"squareMiles": 52.22,"population": 10631,"Name": "Tulalip Reservation"},{"squareMiles": 84.29,"population": 1049,"Name": "Tule River Reservation"},{"squareMiles": 1.22,"population": 121,"Name": "Tunica-Biloxi Reservation"},{"squareMiles": 0.59,"population": 185,"Name": "Tuolumne Rancheria"},{"squareMiles": 237.43,"population": 8669,"Name": "Turtle Mountain Reservation"},{"squareMiles": 9.08,"population": 1152,"Name": "Tuscarora Nation Reservation"},{"squareMiles": 0.6,"population": 12,"Name": "Twenty-Nine Palms Reservation"},{"squareMiles": 6825.13,"population": 24369,"Name": "Uintah and Ouray Reservation"},{"squareMiles": 270.7,"population": 3031,"Name": "Umatilla Reservation"},{"squareMiles": 0.75,"population": 87,"Name": "Upper Lake Rancheria"},{"squareMiles": 2.05,"population": 148,"Name": "Upper Sioux Community"},{"squareMiles": 0.18,"population": 220,"Name": "Upper Skagit Reservation"},{"squareMiles": 900.97,"population": 1742,"Name": "Ute Mountain Reservation"},{"squareMiles": 2.51,"population": 520,"Name": "Viejas Reservation"},{"squareMiles": 531.35,"population": 746,"Name": "Walker River Reservation"},{"squareMiles": 1023.04,"population": 4012,"Name": "Warm Springs Reservation"},{"squareMiles": 0.13,"population": 70,"Name": "Wells Colony"},{"squareMiles": 1167.01,"population": 9562,"Name": "White Earth Reservation"},{"squareMiles": 3532.61,"population": 26490,"Name": "Wind River Reservation"},{"squareMiles": 178.11,"population": 2694,"Name": "Winnebago Reservation"},{"squareMiles": 0.56,"population": 53,"Name": "Winnemucca Indian Colony"},{"squareMiles": 0.61,"population": 214,"Name": "Woodfords Community"},{"squareMiles": 15.25,"population": 60,"Name": "XL Ranch Rancheria"},{"squareMiles": 2187.98,"population": 31272,"Name": "Yakama Nation Reservation"},{"squareMiles": 684.53,"population": 6465,"Name": "Yankton Reservation"},{"squareMiles": 1.01,"population": 718,"Name": "Yavapai-Apache Nation Reservation"},{"squareMiles": 2.2,"population": 192,"Name": "Yavapai-Prescott Reservation"},{"squareMiles": 0.031,"population": 151,"Name": "Yerington Colony"},{"squareMiles": 7.31,"population": 95,"Name": "Yomba Reservation"},{"squareMiles": 5.03,"population": 804,"Name": "Ysleta del Sur Pueblo"},{"squareMiles": 88.08,"population": 1238,"Name": "Yurok Reservation"},{"squareMiles": 191.12,"population": 737,"Name": "Zia Pueblo"},{"squareMiles": 725.82,"population": 7891,"Name": "Zuni Reservation"}];

const ProviderVisualizations = () => {
  const [data1, setData1] = useState(undefined);
  const [plot1Initialized, setPlot1Initialized] = useState(false);
  const [data2, setData2] = useState(undefined);
  const [plot2Initialized, setPlot2Initialized] = useState(false);
  const [data3, setData3] = useState(undefined);
  const [plot3Initialized, setPlot3Initialized] = useState(false);
  const container1Ref = useRef();
  const container2Ref = useRef();
  const container3Ref = useRef();

  // PLOT 1
  // Fetch data
  useEffect(() => {
    if (data1 === undefined) {
      (async () => {
        const responseData = await getData1();
        setData1(responseData ? responseData.results : null);
      })();
    }
  }, [data1]);

  // Create and append plot
  useEffect(() => {
    if (data1 && !plot1Initialized) {
      // First, sort the data by 'numTribes'
      const sortedData = data1.slice().sort((a, b) => b.numTribes - a.numTribes);

      // Extract the names in sorted order
      const sortedNames = sortedData.map(d => d.name);

      // Create the plot with the sorted data and using the sorted names for the x-axis
      const plot = Plot.plot({
        width: 800,
        x: { 
          domain: sortedNames, // This ensures the x-axis respects the sorted order
          label: "Agency",
          tickFormat: () => ""
        },
        y: {
          label: "Number of tribes served"
        },
        color: { scheme: "burd" },
        marks: [
          Plot.ruleY([0]),
          Plot.barY(sortedData, {
            x: "name",
            y: "numTribes",
            fill: "State",
            tip: true
          })
        ]
      });

      container1Ref.current.appendChild(plot);
      setPlot1Initialized(true);

    }
  }, [data1, plot1Initialized]);



  // PLOT 2
  useEffect(() => {
    if (data2 === undefined) {
      (async () => {
        setData2(await getData2());
      })();
    }
  }, [data2]);

  // Create and append PLOT 2
  useEffect(() => {
    if (data2 && !plot2Initialized) {
      console.log(data2);
      const plot = Plot.plot({
        x: {
          label: "State" // Label for the x-axis
        },
        y: {
          label: "Number of Tribes", // Label for the y-axis
        },
        color: {
          scheme: "burd" // Use the 'burd' color scheme
        },
        marks: [
          Plot.ruleY([0]),
          Plot.barY(data2, {
            x: "State",
            y: "numTribes",
            fill: "State", // Color the bars based on the State
            tip: true
          })
        ]
      });
      container2Ref.current.appendChild(plot);
      setPlot2Initialized(true);

    }
  }, [data2, plot2Initialized]);



  // PLOT 3
  useEffect(() => {
    if (data3 === undefined) {
      (async () => {
        setData3(await getData3());
      })();
    }
  }, [data3]);

  // Create and append PLOT 3
  useEffect(() => {
    if (data3 && !plot3Initialized) {
      const plot = Plot.plot({
        x: {
          label: "Area in Square Miles",
        },
        y: {
          label: "Population",
        },
        color: {
          scheme: "burd" // Use the 'burd' color scheme
        },
        marks: [
          Plot.dot(data3, {
            x: "squareMiles",
            y: "population",
            channels: {
              Name: d => d.Name // Replace 'customData' with the field you want to use
            },
            tip: {
              pointer: 'xy'
            }
          }),
        ]
      });
      container3Ref.current.appendChild(plot);
      setPlot3Initialized(true);

    }
  }, [data3, plot3Initialized]);


  const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 32,
    marginBottom: 32
  }
  return (
    <div>
      <div style={{ marginTop: 16, fontSize: 30, fontWeight: 'bolder' }}>
        <span>{'Visualizations of data from '}</span>
        <a href='https://www.nativeaidnetwork.me/home'>{'NativeAidNetwork'}</a>
        <span>{'.'}</span>
      </div>
      <div style={containerStyle}>
        <div>{'Number of Tribes per State'}</div>
          <div ref={container2Ref}>
            {!plot2Initialized && <Loader/>}
          </div>
      </div>
      <div style={containerStyle}>
        <div>{'Agencies by Number of Tribes Served'}</div>
        <div ref={container1Ref}>
          {!plot1Initialized && <Loader/>}
        </div>
      </div>
      <div style={containerStyle}>
        <div>{'Reservations by Area and Population'}</div>
        <div ref={container3Ref}>
          {!plot3Initialized && <Loader/>}
        </div>
      </div>
    </div>
  );
};

export default ProviderVisualizations;
