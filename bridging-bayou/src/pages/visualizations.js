import { useEffect, useState } from "react";
import PulseLoader from "react-spinners/PulseLoader";
import { getCityBriefs, getPantryBriefs } from "../requests/requests";
import PovertyBarChart from "../components/visualizations/PovertyBarChart";
import ChartWithLegend from "../components/ChartWithLegend";
import StackedRaceDemo from "../components/visualizations/StackedRaceDemo";

const Loader = () => {
  return (
    <PulseLoader 
      color={"gray"}
      loading={true}
      // cssOverride={override}
      size={20}
      speedMultiplier={0.5}
      aria-label="Loading Spinner"
      data-testid="loader"
    />
  );
};

const Visualizations = () => {
  const [data1, setData1] = useState(undefined);
  const [data2, setData2] = useState(undefined);
  const [data3, setData3] = useState(undefined);

  // PLOT 1
  // Fetch data
  useEffect(() => {
    if (data1 === undefined) {
      (async () => {
        const responseData = await getCityBriefs("");
        if(responseData) {
          setData1(processCityData(responseData));
        }
      })();
    }
  }, [data1]);

  const processCityData = (data) => {
    return data.map((city) => ({
      City: city.city +", " + city.state,
      poverty_rate: Math.round(city.poverty_rate * 10000)/100,
    })).sort(function(a,b){return b.poverty_rate - a.poverty_rate});
  };

  // PLOT 2
  // Fetch data
  useEffect(() => {
    if (data1 === undefined) {
      (async () => {
        const responseData = await getPantryBriefs("");
        if(responseData) {
          setData2(processPantryData(responseData));
        }
      })();
    }
  }, [data2]);

  //colors for pie chart
  const colors = [
    //light pink
    'rgba(255 ,156 ,183, 0.6)',
    //teal
    'rgba(75, 210, 210, 0.6)',
    //blue
    'rgba(54, 100, 250, 0.6)',
    //purple
    'rgba(153, 102, 255, 0.6)',
    //dark green
    'rgba(33 ,103 ,94, 0.6)',
    //yellow
    'rgba(255, 205, 86, 0.6)',
    //maroon
    'rgba(160, 16, 68, 0.6)',
    //pink
    'rgba(230, 99, 132, 0.6)',
    //brown
    'rgba(100, 91, 91, 0.6)'
  ]

  const processPantryData = (data) => {
    let num_pantries = new Map();
    let data_p = [];
    let states = [];
    let col = [];
    let ret = {
      pie: {
        datasets: [
        ]
      }
    };
    data.map((pantry) => 
      {
        let prev = 0;
        if (num_pantries.has(pantry.state)) {
          prev = num_pantries.get(pantry.state);
        } else {
          states.push(pantry.state);
        }
        num_pantries.set(pantry.state, prev + 1);
        return ({
          state: pantry.state
        })
      }
    );
    let legend = states.map((state, i) => {
      col.push(colors[i]);
      data_p.push(Math.round(num_pantries.get(state) / data.length * 10000) / 100)
      return({
        label : state + " - " + num_pantries.get(state) + " pantries",
        color : colors[i],
      })
    });
    legend.push({
      label: "Total pantries: " + data.length,
    })
    ret.pie.datasets.push({
      data: data_p,
      backgroundColor: col
    })
    ret.key = legend;
    return ret;
  };

  // PLOT 3
  // Fetch data
  useEffect(() => {
    if (data1 === undefined) {
      (async () => {
        const responseData = await getCityBriefs("");
        if(responseData) {
          setData3(processCityRaceData(responseData));
        }
      })();
    }
  }, [data3]);

  const processCityRaceData = (data) => {
    data = data.map((city) => {
      return ({
        city: city.city + ", " + city.state + " pop: " + city.population,
        poverty_rate: city.poverty_rate,
        race_breakdown: city.race_breakdown.split("//")
      });
    }).sort(function(a,b){return b.poverty_rate - a.poverty_rate});
    console.log(data);
    let race_data = [];
    data.map((elem) => {
      let races = [];
      let population = 0;
      elem.race_breakdown.map((cur_race) => {
        let temp = cur_race.split(":");
        races.push(parseFloat(temp[1]));
        population += parseFloat(temp[1]);
      });
      race_data.push({
        name: elem.city,
        white: Math.round(races[0] / population * 10000) / 100,
        black: Math.round(races[1] / population * 10000) / 100,
        native: Math.round(races[2] / population * 10000) / 100,
        asian: Math.round(races[3] / population * 10000) / 100,
        pacific: Math.round(races[4] / population * 10000) / 100,
        other: Math.round(races[5] / population * 10000) / 100,
        multiple: Math.round(races[6] / population * 10000) / 100
      });
      console.log("race array:");
      console.log(race_data);
    });
    return race_data;
  };

  const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 32,
    marginBottom: 32
  }
  return (
    <div>
      <div style={{ marginTop: 16, fontSize: 30, fontWeight: 'bolder' }}>
        <span>{'Visualizations of data from Bridging the Bayou'}</span>
      </div>
      <div style={containerStyle}>
        <div>{'Cities in order of Poverty Rate'}</div>
       { data1 && <PovertyBarChart data={data1}/>|| <Loader/>}
      </div>
      <div style={containerStyle}>
      <div>{'City Race Demographics (Percentages)'}</div>
        <StackedRaceDemo data={data3}/>
      </div>
      <div style={containerStyle}>
      <div>{'Number of Food Pantries per State'}</div>
        {data2 && <ChartWithLegend data={data2.pie} legendItems={data2.key}/>|| <Loader/>}
      </div>
    </div>
  );
};



export default Visualizations;
