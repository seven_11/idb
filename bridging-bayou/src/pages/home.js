import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import "../css/homepage.css"
import {Link} from 'react-router-dom'
import ModelPreviewCard from '../components/card_templates/ModelPreviewCard';
import shelterImage from '../img/shelter.jpg'
const Home = () => {
  return (
   <div>
    {/* Image source: https://mississippitoday.org/2018/10/29/no-room-for-them-for-mississippians-in-deep-poverty-voting-is-easier-said-than-done/*/}
      <div className="w-100" id="home-image">
        <div className="d-flex justify-content-md-center align-items-center vh-100">
          <div>
            <h1 className="display-2 text-white px-1">Bridging the Bayou</h1>
            <h3 className="text-white ">Helping those experiencing homelessness in the Mississippi Delta region.</h3>
          
          </div>
        </div>
      </div>
      
      {/* About section for Bridging the Bayou*/}
      <div className="row">
        <div className="col-sm home-about">
          <h4 className="display-5 text-white">About us</h4>
              {/* Cite for quote: https://www.bbc.com/news/magazine-16385337*/}
              <p className="text-white w-75 m-auto">Known as "the poorest corner in the poorest state in America," the Mississippi
                Delta Region is lacking the resources and funding in order to provide for their underserved communities. With a lack of public transportation
                and an increased need for low-income housing, communities within the Delta region are in need of support and additional resources to provide for their families.
                Learn about the growing rates of poverty within the cities of the Delta region, and check out the available resources to offer your support for those experiencing homelessness (BBC). </p>
          <Link to={'/cities'}>
            <button type="button" className="btn btn-custom px-2 my-3">
            Explore each city's resources
            </button>
          </Link>
        </div>
      </div>

        {/* Cards for cities, shelters, and food pantries model landing pages*/}
        <div  className="row my-4">
          <div className="col p-2">
            <ModelPreviewCard 
              image="cities_bg.png"
              link="/cities"
              about="Learn more about the resources available at the cities within the 
              Delta region and find ways to support those experiencing homelessness."
              name="Cities"
            />
          </div>
          <div className="col p-2">
            <ModelPreviewCard 
              image={shelterImage}
              link="/shelters"
              about="Learn more about the resources available at the nearby shelters within the cities of the Delta region."
              name="Shelters"
            />
          </div>
          <div className="col p-2">
            <ModelPreviewCard 
              image="food_bank.png"
              link="/food-pantries"
              about="Learn more about the available resources and meal assistance available at the nearby food pantries within the cities of the Delta region."
              name="Food Pantries"
            />
          </div>
      </div>

   </div>
  );
};
  
export default Home;