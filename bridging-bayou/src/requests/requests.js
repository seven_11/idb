// const BASE_URL = 'http://54.224.191.12:5000/api/';
const BASE_URL = 'https://api.bridging-the-bayou.me';

function catURL(url1, url2) {
  if(!url1.endsWith('/'))
    url1 += '/';
  if(url2.startsWith('/'))
    url2 = url2.substring(1);
  return url1 + url2;
}

async function get(url) {
  const fullURL = catURL(BASE_URL, url);
  const responsePromise = fetch(fullURL);
  return responsePromise;
}

function formatText(text) {

  const specialChars = {"+":"%2B", " ":"%20", "/":"%2F", "?":"%3F","%":"%25", "%":"%25", "#":"%23","&":"%26"}
  Object.entries(specialChars).map(([key, value]) => {
    text.replaceAll(key, value)
  })
  return text
}

async function getJSON(url) {
  const response = await get(url);
  const jsonPromise = response.json();
  return jsonPromise;
}

function getQueryString(sort, desc, filters) {
  let query = "?";
  if(sort != "") {
    query += `&sort=${sort}`;
    if(desc) {
      query += "&sort_order=desc";
    }
  }
  for(let filter in filters) {
    if(filters[filter] != "") {
      query += `&${filter}=${filters[filter]}`
    }
  }
  return query == "?" ? "" : query;
}

/**
 * @returns string[]
 */
async function getShelterNames() {
  return await getJSON('allShelterNames');
}

/**
 * @returns object[]
 */
async function getShelterBriefs(query) {
  return await getJSON('allShelterBriefs' + query);
}


/**
 * @returns object[]
 */
async function getShelterInstance(name) {
  return await getJSON("/shelter/" + name.replace(" ", "%20"))
}

/**
 * @returns string[]
 */
async function getPantryNames() {
  return await getJSON('allPantryNames');
}

/**
 * @returns object[]
 */
async function getPantryBriefs(query) {
  return await getJSON('allPantryBriefs' + query);
}


async function getPantryInstance(name) {
  return await getJSON("/pantry/" + formatText(name))
}
/**
 * @returns string[]
 */
async function getCityNames() {
  return await getJSON('allCityNames');
}

/**
 * @returns object[]
 */
async function getCityBriefs(query) {
  return await getJSON('allCityBriefs' + query);
}

/**
 * @returns object[]
 */
async function getCityInstance(name, state) {
  return await getJSON("/city/" + formatText(name) + "/" + formatText(state));
}

async function getCitiesSearch(search) {
  return await getJSON("/allCityBriefs?search="+search.replace(" ", "%20"))
}

async function getSheltersSearch(search) {
  return await getJSON("/allShelterBriefs?search="+search.replace(" ", "%20"))
}

async function getPantriesSearch(search) {
  return await getJSON("/allPantryBriefs?search="+search.replace(" ", "%20"))
}
export {
  getShelterNames,
  getShelterBriefs,
  getShelterInstance,
  getPantryNames,
  getPantryBriefs,
  getPantryInstance,
  getCityNames,
  getCityBriefs,
  getCityInstance,
  formatText,
  getCitiesSearch,
  getSheltersSearch,
  getPantriesSearch,
  getQueryString
};