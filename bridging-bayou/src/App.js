
import React from 'react';
import './App.css';
import NavbarCustom from './components/NavBarCustom';
import { Routes, Route } from 'react-router-dom';
import { Helmet } from "react-helmet";
import Home from './pages/home';
import About from './pages/about';
import FoodPantries from './pages/model_pages/food-pantries';
import Shelters from './pages/model_pages/shelters';
import Cities from './pages/model_pages/cities';
import CityPage from './pages/instance_pages/city';
import PantryPage from './pages/instance_pages/pantry'
import ShelterPage from './pages/instance_pages/shelter'
import ProviderVisualizations from './pages/provider-visualizations';
import Visualizations from './pages/visualizations';
import SearchResults from './components/SearchResults';



function App() {
  return (
    <div className="App">
      <Helmet>
        <title>Bridging the Bayou</title>
      </Helmet>
      <NavbarCustom />
      <Routes>
        <Route path='/' element={<Home/>} />
        <Route path='/home' element={<Home/>} />
        <Route path='/cities' element={<Cities/>} />
        <Route path="/city/:name/:state" element={<CityPage />} />
        <Route path='/shelters' element={<Shelters/>} />
        <Route path="/shelter/:name" element={<ShelterPage />} />
        <Route path="/pantry/:name" element={<PantryPage/>} />
        <Route path='/food-pantries' element={<FoodPantries/>} />
        <Route path='/about' element={<About/>} />
        <Route path="/search/:filter/:query" element={<SearchResults/>} />
        <Route path='/provider-visualizations' element={<ProviderVisualizations/>} />
        <Route path='/visualizations' element={<Visualizations/>} />
      </Routes>
    </div>
  );
}

export default App;
