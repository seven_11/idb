import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';

import "../css/paginate.css"
const DisplayCards = ({cards, CardComponent, queryReg, cardsPerPage, cardImages, setCardImages}) => {
  console.log(`rendering DisplayCards w ith ${cards?.length} cards`);
  // determines how many items we show per page

  // const currentItems = cards;
  const [currentItems, setCurrentItems] = useState(cards);
  const [itemOffset, setItemOffset] = useState(0);
  const [pageCount, setPageCount] = useState(null);

  // TODO: Cite source react-paginate
  useEffect(()=> {
    setCurrentItems(cards);
    setPageCount(Math.ceil(cards.length / cardsPerPage));
  }, [cards])

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    console.log(`handling page click, ${event.selected} / ${cardsPerPage} / ${cards.length}`);
    const newOffset = (event.selected * cardsPerPage) % cards.length;
    console.log(`User requested page number ${event.selected}, which is offset ${newOffset}`);
    setItemOffset(newOffset);
  }

  const instanceCountMessage = currentItems.length > cardsPerPage ?
    (itemOffset + cardsPerPage > currentItems.length ?
      currentItems.length - itemOffset :
      cardsPerPage)
    + ` out of ${currentItems.length} shown`
    : `${currentItems.length} instances available`

return (

  <div>
    <div className="container p-3">

      <div className="num-instances-text">
      {instanceCountMessage}
    </div>
        <div className="row justify-content-evenly" >

        {/* Loop to display current items for page */}
          {currentItems.slice(itemOffset, itemOffset + cardsPerPage).map((item)=> (
            
              
              <div className="col p-3 d-flex justify-content-center">   
              {/* Uses component name that was passed as props and passes current shelter instance's info as props */}
              { <CardComponent imageLink={item.photo_url} setImage={cardImages && setCardImages} data={item} regex={queryReg} />}
              </div>
              ))
            }
          </div>
        <div className="w-sm-75 w-m-100 pagination-box">
          <ReactPaginate
          breakLabel="..."
          nextLabel={"→"}
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          pageClassName="page_item_custom"
          pageLinkClassName="page_link_custom"
          breakClassName="page_item_custom"
          breakLinkClassName="page_link_custom"
          containerClassName="pagination"
          activeClassName={"page_item_custom--active"}
          previousLabel={"←"}
          previousLinkClassName={"prev_button"}
          nextLinkClassName={"next_button"}
          renderOnZeroPageCount={null}
          />
        </div>    

    </div>

  </div>
)
}
export default DisplayCards;
