// Used for search bar implementation 
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton'
import {useState, useEffect} from 'react'
import SearchResults from './SearchResults';
import { Link, useNavigate } from "react-router-dom";
import { nanoid } from "nanoid";



const Searchbar = () => {
  const [search, setSearch] = useState("")
  const [filter, setFilter] = useState("All")
  const [showSearch, setShowSearch] = useState(false)

  const navigate = useNavigate();

  const handleChange = (e) => {
    setSearch(e.target.value)
  }

  const handleFilter = (e) => {
    console.log(showSearch)
    setFilter(e)
  }
  return (
    <>
      <Row>
        <Col xs="auto">
          <DropdownButton id="dropdown-item-button" title={filter} onSelect={(e) => handleFilter(e)}>
            <Dropdown.Item as="button" eventKey="All">All</Dropdown.Item>
            <Dropdown.Item as="button" eventKey="Cities">Cities</Dropdown.Item>
            <Dropdown.Item as="button" eventKey="Shelters">Shelters</Dropdown.Item>
            <Dropdown.Item as="button" eventKey="Pantries">Pantries</Dropdown.Item>
         </DropdownButton>
        </Col>
        <Col xs="auto">
          <Form.Control
            type="text"
            placeholder="Search"
            value={search}
            className=" mr-sm-2"
            onChange={handleChange}
          />
        </Col>
        <Col xs="auto">
          <Link className="btn submit-button" to={`/search/${filter}/${search}`} onClick={() => navigate(`/search/${filter}/${search}`, { state: nanoid() })}>Submit</Link>
        </Col>
      </Row>
    </>
  )
};

export default Searchbar;