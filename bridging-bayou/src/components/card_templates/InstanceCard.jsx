import {Link} from 'react-router-dom'
import "../../css/card-instance.css"
import {useEffect} from "react"
import { Highlight } from 'react-highlight-regex'
import "../../css/search.css"

const InstanceCard = ({imageLink, defaultImage, title, listItems, pageLink, regex}) => {
  function highlightText(input) {
    if (regex != null) {
      return <Highlight highlightClassname={"mark"} match={regex} text={input} />;
    }
    return input;
  }

  useEffect(()=> {
    console.log("image link")
    console.log(imageLink)
  }, [])

  return (

  <div className="card h-100" style={{width: 18 + "rem"}}>
    
    <img src={imageLink? imageLink: defaultImage} alt="default photo" className="thumbnail-image" />
    <div className="card-body">
      <h5 className="card-title">{title}</h5>
      <p className="card-text">{highlightText("Learn more about the available resources in " + title + ".")}</p>
    </div>
    <ul className="list-group list-group-flush">
      {listItems.map(item => 
        <li className="list-group-item">{highlightText(item)}</li>
      )}
    </ul>
    <div className="card-body">
        <Link to={pageLink}><div className="btn btn-card">Read more</div></Link>
    </div>
  </div>
  );
}

export default InstanceCard;