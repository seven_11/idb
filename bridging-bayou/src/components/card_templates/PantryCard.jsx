import InstanceCard from './InstanceCard'
import PantryImage from '../../img/pantry_default.png'

const PantryCard = ({imageLink, setImage, data, regex}) => {
  const listItems = [
    "Address: " + data.address,
    "City: " + data.city,
    "State: " + data.state,
    "Zip code: " + data.zip_code,
    "Available Days: " +data.schedule_description
    ];

  return(
    <InstanceCard 
      imageLink={imageLink}
      defaultImage={PantryImage}
      title={data.name}
      pageLink={`/pantry/${data.name}`}
      regex={regex}
      listItems={listItems}
    />
  );
}

export default PantryCard;