import InstanceCard from "./InstanceCard";
import city_pic from '../../img/genericCity.png'

const CityCard = ({imageLink, setImage, data, regex}) => {
  const listItems = [
    "Location (Zip codes): " + data.zip_codes.join(", "),
    "County: " + data.counties,
    "State: " + data.state,
    "Population size: " + data.population,
    "Poverty rate: " + Math.round(data.poverty_rate * 10000)/100 + "%",
    "Number of food pantries: " + data.pantries.length,
    "Number of shelters: " + data.shelters.length
    ];

  return(
    <InstanceCard 
      imageLink={imageLink}
      defaultImage={city_pic}
      title={data.city}
      pageLink={`/city/${data.city}/${data.state}`}
      regex={regex}
      listItems={listItems}
    />
  );
}

export default CityCard;