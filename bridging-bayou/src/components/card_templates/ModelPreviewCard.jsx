/* City Card: Component used to display information about each city. */
import React from 'react';
import {Link} from 'react-router-dom'
import "../../css/card-instance.css"


// Template for displaying city information in a card view
const ModelPreviewCard = ({name, about, image, link}) => {
  return (
    <div className="card mx-auto h-100" style={{width: 18 + "rem"}}>
      <img src={image} className="thumbnail-image" alt="city photo"/>
      <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">{about}</p>
        <Link to={link}><div className="btn btn-card">Read more</div></Link>
      </div>
    </div>
  );
};

export default ModelPreviewCard;