import InstanceCard from './InstanceCard'
import ShelterImage from '../../img/shelter_default.png'

const ShelterCard = ({imageLink, setImage, data, regex}) => {
  const listItems = [
    "Zip code: " + data.zip_code,
    "City: " + data.city,
    "State: " + data.state,
    "Genders served: " + data.genders_served,
    "Laundry services: " + data.has_laundry_services ? "Yes" : "No",
    ];

  return(
    <InstanceCard 
      imageLink={imageLink}
      defaultImage={ShelterImage}
      title={data.name}
      pageLink={`/shelter/${data.name}`}
      regex={regex}
      listItems={listItems}
    />
  );
}

export default ShelterCard;