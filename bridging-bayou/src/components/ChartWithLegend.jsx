import React from 'react';
import PropTypes from 'prop-types';
import { Pie } from 'react-chartjs-2';
import {Chart, ArcElement} from 'chart.js'
Chart.register(ArcElement);



const ChartWithLegend = ({ data, legendItems, name }) => {
  return (
    <div className="home-about">
      <h1 className="display-5 text-white" style={{WebkitColumnFill:"#49814b"}} >{name}</h1>
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <div style={{ maxWidth: '400px', margin: '50px' }}>
        <Pie data={data} />
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', marginLeft: '20px' }}>
        {legendItems.map((item, index) => (
          <div key={index} style={{ display: 'flex', alignItems: 'center', margin: '5px' }}>
            <div
              style={{
                backgroundColor: item.color,
                width: '20px',
                height: '20px',
                marginRight: '5px',
              }}
            ></div>
            <div className='text-white'>{item.label}</div>
          </div>
        ))}
      </div>
    </div>
    </div>
  );
};

ChartWithLegend.propTypes = {
  data: PropTypes.object.isRequired, // Chart data
  legendItems: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired, // Legend label
      color: PropTypes.string, // Color associated with the legend item
    })
  ).isRequired,
};

export default ChartWithLegend;
