import React from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import '../css/navbar.css'
import {LinkContainer} from 'react-router-bootstrap'
import {Link} from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import {Navbar} from 'react-bootstrap'
import {Nav} from 'react-bootstrap';
import Searchbar from './Searchbar'
import '../css/homepage.css'

const NavbarCustom = () => {
  return (
    <>
    <Navbar expand="lg" className="gap-2 px-3 nav-text" 
      // activeKey={window.location.pathname}       
      style={{backgroundSize: "0", backgroundColor: "#49814b"}}>

      {/* TODO: adding logo https://medium.com/nerd-for-tech/navbar-styling-react-with-bootstrap-6c74b0631d04 */}
      <Container fluid>
      <LinkContainer to="/">
      <Navbar.Brand className="ml-1 nav-brand">Bridging the Bayou</Navbar.Brand>
      </LinkContainer>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />

      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="me-auto">
          <LinkContainer to="/home" className="mr-auto nav-text">
            <Nav.Link
              as={Link}
              to="/"  
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              Home
            </Nav.Link>     
          </LinkContainer>
          <LinkContainer to="/cities" className="mr-auto nav-text">
            <Nav.Link
              as={Link}
              to="/cities" 
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              Cities
            </Nav.Link>
          </LinkContainer>
          <LinkContainer to="/shelters" className="nav-text">
            <Nav.Link
              as={Link}
              to="/shelters"
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              Shelters
            </Nav.Link>
          </LinkContainer>
          <LinkContainer to="/food-pantries" className="nav-text">
            <Nav.Link
              as={Link}
              to={'/food-pantries'}
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              Food Pantries
            </Nav.Link>
          </LinkContainer>
          <LinkContainer to="/about" className="nav-text">
            <Nav.Link
              as={Link}
              to={'/about'}
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              About
            </Nav.Link>
          </LinkContainer>
          <LinkContainer to="/visualizations" className="nav-text">
            <Nav.Link
              as={Link}
              to={'/visualizations'}
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              Visualizations
            </Nav.Link>
          </LinkContainer>
          <LinkContainer to="/provider-visualizations" className="nav-text">
            <Nav.Link
              as={Link}
              to={'/about'}
              className={({ isActive, isPending }) =>
              isPending ? "pending" : isActive ? "active" : ""}
            >
              Provider Visualizations
            </Nav.Link>
          </LinkContainer>
        </Nav>
        <Searchbar/>
      </Navbar.Collapse>

      </Container>
    </Navbar>
    </>
  );
};
  
export default NavbarCustom;