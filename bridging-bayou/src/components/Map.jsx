import {
  setKey,
  setDefaults,
  setLanguage,
  setRegion,
  fromAddress,
  geocode,
  RequestType,
} from "react-geocode";
import GoogleMapReact from 'google-map-react'
import "../css/map.css"
import {useState, useEffect} from 'react'


const Map = (data, locationName, address, city, state) => {

  const [location, setLocation] = useState(null)
  const [url, setUrl] = useState(null)
  const LocationPin = ({ text }) => (
    <div className="pin">
      <i class="bi bi-pin-map-fill"></i>
      <p className="pin-text">{text}</p>
    </div>
  )

  setKey(process.env.REACT_APP_GEOCODE_KEY)
// Set default response language and region (optional).
// This sets default values for language and region for geocoding requests.
// setDefaults({
//   key: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", // Your API key here.
//   language: "en", // Default language for responses.
//   region: "es", // Default region for responses.
// });

// Alternatively

// Set Google Maps Geocoding API key for quota management (optional but recommended).
// Use this if you want to set the API key independently.
// setKey("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"); // Your API key here.

// // Set default response language (optional).
// // This sets the default language for geocoding responses.
// setLanguage("en"); // Default language for responses.

// // Set default response region (optional).
// // This sets the default region for geocoding responses.
// setRegion("es"); // Default region for responses.

// process.env.api_key 
// https://blog.logrocket.com/integrating-google-maps-react/
// Get latitude & longitude from address.

useEffect(()=> {
  const newLocation =  data.address.replaceAll(" ", "+")  + "," + data.city.replace(" ", "+") + "," + data.state.replace(" ", "+")
  const editedLocation = data.locationName.replaceAll(" ", "+")
  const url = "https://www.google.com/maps/embed/v1/place?key=" + process.env.REACT_APP_MAP_KEY + "&q=" + editedLocation
  setUrl(url)
  setLocation(newLocation)
  }, [])
return (
  <div>
    {location && url &&

    <div className="col align-items-center p-3">
      <iframe
        className="map-frame rounded-4"
        width="600"
        height="450"
        loading="lazy"
        allowfullscreen
        referrerpolicy="no-referrer-when-downgrade"
        src={url}>
      </iframe>
          
    </div>
    }
  </div>
  )

};
export default Map;
