
import {useState, useEffect, useRef} from 'react'

import { getCitiesSearch, getPantriesSearch, getSheltersSearch } from '../requests/requests';
import CityCard from './card_templates/CityCard'
import ShelterCard from './card_templates/ShelterCard';
import PantryCard from './card_templates/PantryCard';
import DisplayCards from './DisplayCards';
import { Link, useLocation, useParams } from "react-router-dom";

const SearchResults = () => {
  const {query: queryFromRoute} = useParams();
  const queryReg = new RegExp(`(?:${queryFromRoute.replaceAll("%20", "|")})`, "i");


  const {filter: filterFromRoute} = useParams();
  const [filterParam, setFilter] = useState(filterFromRoute);
  const [search, setSearch] = useState(queryFromRoute);
  
  const searchResultsListRef = useRef();
  console.log(searchResultsListRef.current)

  console.log(`search: ${search}`);
  if (search !== queryFromRoute) {
    console.log(`setting search to ${queryFromRoute}`);
    setSearch(queryFromRoute)
  }
  if (filterParam !== filterFromRoute) {
    console.log(`setting filter to ${filterFromRoute}`);
    setFilter(filterFromRoute)
  }
  const [cities, setCities] = useState(null)
  const [shelters, setShelters] = useState(null)
  const [pantries, setPantries] = useState(null)
  const { state } = useLocation();

  async function fetchCities() {
    let info = await getCitiesSearch(search);
    setCities(info)
    console.log(`setting cities inside useEffect: ${info}`);
    return info;
  }

  async function fetchShelters() {
    let info = await getSheltersSearch(search);
    setShelters(info)
    return info;
    // Fetch each shelter's images
  }

  async function fetchPantries() {
    let info = await getPantriesSearch(search);
    setPantries(info)
    return info;
    // Fetch each shelter's images
  }

  useEffect(() => { 
    if (search) {
  
      if (filterFromRoute == "All") {

        fetchCities()
        fetchShelters()
        fetchPantries()
      }
      if (filterFromRoute== "Cities") {
        fetchCities()
      }

      if (filterFromRoute == "Shelters") {
        fetchShelters()
      }
      if (filterFromRoute == "Pantries") {
        fetchPantries()
      }
    }
  }, [state, search])

  console.log(`re-rendering SearchResults with ${cities?.length} cities...`);
  return (
    <div>
    
    <h4 className="display-4 mt-5">Search Results</h4>

    
    {(filterParam == "Cities" || filterParam == "All") && 
    <div>
        <h4 className="display-5 mt-5">Cities</h4>
    
      {shelters ? 
     <DisplayCards cards={cities} queryReg={queryReg} CardComponent={CityCard} cardsPerPage={12} cardImages={cities} setCardImages={setCities}/>
      : "No cities found"
      }
      </div>
    }
    
    
    {(filterParam == "Shelters" || filterParam == "All") && 
      <div>
        <h4 className="display-5 mt-5">Shelters</h4>
    
      {shelters ? 
     <DisplayCards cards={shelters} queryReg={queryReg} CardComponent={ShelterCard} cardsPerPage={12} cardImages={shelters} setCardImages={setShelters}/>
      : "No shelters found"
      }
      </div>
    }
    
    {(filterParam == "Pantries" || filterParam == "All") &&
      <div>
         <h4 className="display-5 mt-5">Food Pantries</h4>
      {pantries ?
    
      <DisplayCards cards={pantries} queryReg={queryReg} CardComponent={PantryCard} cardsPerPage={12} cardImages={pantries} setCardImages={setPantries}/>
        : "No pantries found"
      } 
      </div> 
    } 


    
  
    </div>
  )
};
export default SearchResults;