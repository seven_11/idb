import MapDropdown from "./MapDropdown";

const SortOrderDropdown = ({scroll, setSortOrder, setLoading}) => {
    return(
        <MapDropdown title={"Sort Order"} 
        items={{"Ascending":false, "Descending":true}} 
        scroll={scroll} 
        setFilter={setSortOrder} 
        setLoading={setLoading} />
    )
}

export default SortOrderDropdown;