import BaseDropdown from "./BaseDropdown";

const MapDropdown = ({title, items, scroll, setLoading, setFilter, setSortOrder}) => {
    const mapItems = (value) => {
        for(let title in items) {
          if(value == title) {
            return items[title];
          }
        }
      }
    
    const handleFilter = (value) => {
        let res = mapItems(value);
        setFilter(res);

        setLoading(false);
    }

    return (<BaseDropdown title={title} items={Object.keys(items)} scroll={scroll} onChange={handleFilter} />)
}

export default MapDropdown;