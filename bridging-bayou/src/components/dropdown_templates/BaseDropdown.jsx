import React, { useState } from "react";

import Dropdown from "react-bootstrap/Dropdown";
import Container from "react-bootstrap/Container";
import "../../css/dropdown.css"
const BaseDropdown = ({ title, items, scroll, onChange}) => {
  const [choice, setChoice] = useState(title);

  const handleClick = (value) => {
    setChoice(value);
    onChange(value);
  };

  return (
    <Dropdown>
      <Dropdown.Toggle id="dropdown-btn">
      {title}
      </Dropdown.Toggle>

      <Dropdown.Menu variant="success" data-testid="dropdown">
        <Container style={scroll ? { height: "20rem", overflowY: "scroll"} : {}}>
          {items?.map((item) => {
            return (
              <Dropdown.Item onClick={() => handleClick(item)}>
                {item}
              </Dropdown.Item>
            );
          })}
        </Container>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default BaseDropdown;