import Dropdown from 'react-bootstrap/Dropdown';
import Form from 'react-bootstrap/Form';
import { Link } from 'react-router-dom';
import { Row } from 'react-bootstrap';
import "../../css/dropdown.css"

const Filter = ({selected, setSelected, setLoading, items}) => {
  const handleFilter = (e, text, val) => {
    const checked = e.target.checked;
    let items = [...selected.items]
    if (checked && (val == "true")) {
        items[0][text] = "true"
      
    } else if (checked && (val == "false")) {
      items[0][text] = "false"
    } else {
      items[0][text] = ""
    }
    setSelected({items:items})
  }

  const handleSubmit = () => {
    setLoading(false)
  }


  const clearOption = (name) => {
    let items = [...selected.items]

    items[0][name] = ""
    console.log(items)
    setSelected({items:items})
  }
  const handleClear = () => {
    console.log(selected.items)
    let keys = Object.keys(selected.items[0])
    console.log(keys)

    let newItems = [...selected.items]
    console.log(newItems)
    for (let i = 0; i < keys.length; i++) {
      console.log(newItems)
      newItems[0][keys[i]] = ""
    }
  
    console.log(newItems)
    setSelected({items:newItems})
    setLoading(false)
  }

  return (
    <Dropdown className="float-right">
      <Dropdown.Toggle id="dropdown-btn">
        Filter by
      </Dropdown.Toggle>

      <Dropdown.Menu  className="px-3" title="Filter by">
          <div  className="mb-3">
          {items.map((item)=> {
            return (
            <>
            <Form.Text>
              {item.text}
              <Row><Link onClick={()=>clearOption(item.key)} className="mx-auto px-3">Clear option</Link></Row>

            </Form.Text>
            {item.options.map((option)=> 
              (
                <Form.Check
                  label={option.key}
                  type="radio"
                  id={option.key}
                  name={item.key}
                  checked = {(selected.items[0][item.key] === option.value) ? true : false}
                  onChange={(event)=>handleFilter(event, item.key, option.value)}
                />
              )
              )}
            </>
            )
          })} 
        </div>
      
        <Row>
          <div className="btn btn-card" onClick={()=>handleSubmit()}>Apply</div>
        </Row>
        <Row>
          <div className="btn btn-card my-1" onClick={()=>handleClear()}>Clear Filter</div>

          {/* <Link onClick={handleClear} className="mx-auto">Clear filter</Link> */}
        </Row>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default Filter;