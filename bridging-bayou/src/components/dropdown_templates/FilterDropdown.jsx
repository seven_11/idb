import MapDropdown from "./MapDropdown";
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col";

const FilterDropdown = ({title, scroll, setFilter, setLoading}) => {
    return(
      <Row>
          <Col>
            <h5>{title}</h5>
          </Col>
          <Col>
            <MapDropdown
            items={{"Yes":"true", "No":"false", "Doesn't Matter":""}} 
            scroll={scroll} 
            setFilter={setFilter} 
            setLoading={setLoading} />
          </Col>

      </Row>
    )
}

export default FilterDropdown;