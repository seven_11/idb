import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const PovertyBarChart = ({ data }) => {
    return (
        <BarChart
        width={1000}
        height={500}
        data={data}
        margin={{
        top: 10,
        right: 30,
        left: 20,
        bottom: 5,
        }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="City" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="poverty_rate" name="Poverty Rate" fill="#49814b" unit="%" />
        </BarChart> 
    );
}

/* code sourced from 
https://gitlab.com/bran.heng/park-dex/-/blob/main/frontend/src/components/visualizations/BiodiversityBarChart.js?ref_type=heads
*/

export default PovertyBarChart;