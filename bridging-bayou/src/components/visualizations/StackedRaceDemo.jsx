// import React from 'react';
// import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

// const stackedRaceDemo = ({data}) => {
//     return (
//       <ResponsiveContainer width="100%" height="100%">
//         <BarChart
//           width={500}
//           height={300}
//           data={data}
//           margin={{
//             top: 20,
//             right: 30,
//             left: 20,
//             bottom: 5,
//           }}
//         >
//           <CartesianGrid strokeDasharray="3 3" />
//           <XAxis dataKey="name" />
//           <YAxis />
//           <Tooltip />
//           <Legend />
//           <Bar dataKey="white" name="White" stackId="a" fill="#8884d8" />
//           <Bar dataKey="black" name="Black" stackId="a" fill="#82ca9d" />
//         </BarChart>
//       </ResponsiveContainer>
//     );
// }

// export default stackedRaceDemo;

import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

const StackedRaceDemo = ({data}) => {
  return (
    <BarChart
      width={1000}
      height={500}
      data={data}
      margin={{
        top: 20,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name"/>
      <YAxis domain={[0, 100]}/>
      <Tooltip />
      <Legend />
      <Bar dataKey="white" name="White (%)" stackId="a" fill="#1c4e4f" />
      <Bar dataKey="black" name="Black (%)" stackId="a" fill="#436e6f" />
      <Bar dataKey="native" name="American Indian & Alaska Native (%)" stackId="a" fill="#6a8e8f" />
      <Bar dataKey="asian" name="Asian (%)" stackId="a"fill="#879693" />
      <Bar dataKey="pacific" name="Native Hawaiian & Other Pacific Islander (%)" stackId="a" fill="#ffcbb5" />
      <Bar dataKey="other" name="Some Other Race (%)" stackId="a" fill="#d45f3b" />
      <Bar dataKey="multiple" name="Two or More Races (%)" stackId="a" fill="#b82424"/>
    </BarChart>
  );
};

export default StackedRaceDemo;