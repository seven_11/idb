const fs = require('fs');
const path = require('path');

const cityImagesDir = 'city_images';

// Function to copy and rename the file
function copyAndRenameFile(subDir) {
  // Construct the full path for the subdirectory
  const subDirPath = path.join(cityImagesDir, subDir);
  // Read the contents of the subdirectory
  fs.readdir(subDirPath, (err, files) => {
    if (err) {
      return console.error(`Error reading directory ${subDirPath}: ${err.message}`);
    }

    // Assuming there is only one file in the directory and it is not a directory itself
    const file = files.find(f => !fs.statSync(path.join(subDirPath, f)).isDirectory());
    if (!file) {
      return console.error(`No file found in directory ${subDirPath}`);
    }

    // Full path to the source file
    const sourceFilePath = path.join(subDirPath, file);
    // Target file path with the subdirectory name
    const targetFilePath = path.join(__dirname, `${subDir}${path.extname(file)}`);

    // Copy the file to the current directory and rename it
    fs.copyFile(sourceFilePath, targetFilePath, err => {
      if (err) {
        return console.error(`Error copying file ${sourceFilePath} to ${targetFilePath}: ${err.message}`);
      }
      console.log(`Copied ${sourceFilePath} to ${targetFilePath}`);
    });
  });
}

// Read the city_images directory and process each subdirectory
fs.readdir(cityImagesDir, (err, subDirs) => {
  if (err) {
    return console.error(`Error reading directory ${cityImagesDir}: ${err.message}`);
  }

  // Filter out non-directory files
  subDirs = subDirs.filter(subDir => fs.statSync(path.join(cityImagesDir, subDir)).isDirectory());

  // Process each subdirectory
  subDirs.forEach(copyAndRenameFile);
});