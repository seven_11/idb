# # # # # # # # # # # # # # # # # # # # # # # #
# You can run this file locally to test with: #
#              flask run                      #
# # # # # # # # # # # # # # # # # # # # # # # #

from flask import Flask, jsonify, make_response, request
from sqlalchemy import desc, func, create_engine, MetaData, Table
import re

DATABASE_URL = "postgresql+psycopg2://postgres:seveneleven@postgres-1.cjvg3duu6ibf.us-east-2.rds.amazonaws.com:5432/postgres"
CITY_SORTABLE_FIELDS = ['city', 'state', 'population', 'poverty_rate', 'median_household_income']
CITY_FILTERS = [
    {
        'name': 'has_pantry',
        'field': 'pantries',
        'type': 'array_not_empty'
    },
    {
        'name': 'has_shelter',
        'field': 'shelters',
        'type': 'array_not_empty'
    }
]
SHELTER_SORTABLE_FIELDS = ['name', 'city', 'state']
SHELTER_FILTERS = [
    {
        'name': 'has_laundry_services',
        'field': 'has_laundry_services',
        'type': 'is_true'
    },
    {
        'name': 'serves_men',
        'field': 'genders_served',
        'type': 'is_not_string',
        'extra': 'women'
    },
    {
        'name': 'serves_women',
        'field': 'genders_served',
        'type': 'is_not_string',
        'extra': 'men'
    }
]
PANTRY_SORTABLE_FIELDS = ['name', 'city', 'state']
PANTRY_FILTERS = [
    {
        'name': 'open_mondays',
        'field': 'monday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
    {
        'name': 'open_tuesdays',
        'field': 'tuesday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
    {
        'name': 'open_wednesdays',
        'field': 'wednesday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
    {
        'name': 'open_thursdays',
        'field': 'thursday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
    {
        'name': 'open_fridays',
        'field': 'friday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
    {
        'name': 'open_saturdays',
        'field': 'saturday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
    {
        'name': 'open_sundays',
        'field': 'sunday_hours',
        'type': 'is_not_string',
        'extra': 'closed'
    },
]

CITY_FIELDS = [
    'city',
    'state',
    'zip_codes',
    'population',
    'counties',
    'poverty_rate',
    'median_household_income',
    'race_breakdown',
    'shelters',
    'pantries',
    'photo_url_1'
]

CITY_SEARCHABLE_FIELDS = [
    'city',
    'state',
    'zip_codes',
    'population',
    'counties',
    'poverty_rate',
    'median_household_income',
    'race_breakdown',
    'shelters',
    'pantries',
]

CITY_BRIEF_FIELDS = {
    'city': 'city',
    'state': 'state',
    'zip_codes': 'zip_codes',
    'population': 'population',
    'counties': 'counties',
    'poverty_rate': 'poverty_rate',
    'race_breakdown': 'race_breakdown',
    'shelters': 'shelters',
    'pantries': 'pantries',
    'photo_url': 'photo_url_1'
}

SHELTER_FIELDS = [
    'name',
    'address',
    'city',
    'state',
    'zip_code',
    'phone_number',
    'email_address',
    'fax_number',
    'description',
    'genders_served',
    'serves_children',
    'has_laundry_services',
    'photo_url_1'
]

SHELTER_SEARCHABLE_FIELDS = [
    'name',
    'address',
    'city',
    'state',
    'zip_code',
    'phone_number',
    'email_address',
    'fax_number',
    'description',
    'genders_served',
]

SHELTER_BRIEF_FIELDS = {
    'name': 'name',
    'address': 'address',
    'city': 'city',
    'state': 'state',
    'zip_code': 'zip_code',
    'genders_served': 'genders_served',
    'serves_children': 'serves_children',
    'has_laundry_services': 'has_laundry_services',
    'photo_url': 'photo_url_1'
}

PANTRY_FIELDS = [
    'name',
    'description',
    'address',
    'city',
    'state',
    'zip_code',
    'local_photo_url',
    'phone_number',
    'email_address',
    'contact_person',
    'schedule_description',
    'monday_hours',
    'tuesday_hours',
    'wednesday_hours',
    'thursday_hours',
    'friday_hours',
    'saturday_hours',
    'sunday_hours',
]

PANTRY_SEARCHABLE_FIELDS = [
    'name',
    'description',
    'address',
    'city',
    'state',
    'zip_code',
    'phone_number',
    'email_address',
    'contact_person',
    'schedule_description',
    'monday_hours',
    'tuesday_hours',
    'wednesday_hours',
    'thursday_hours',
    'friday_hours',
    'saturday_hours',
    'sunday_hours',
]

PANTRY_BRIEF_FIELDS = {
    'name': 'name',
    'address': 'address',
    'city': 'city',
    'state': 'state',
    'zip_code': 'zip_code',
    'schedule_description': 'schedule_description',
    'monday_hours': 'monday_hours',
    'tuesday_hours': 'tuesday_hours',
    'wednesday_hours': 'wednesday_hours',
    'thursday_hours': 'thursday_hours',
    'friday_hours': 'friday_hours',
    'saturday_hours': 'saturday_hours',
    'sunday_hours': 'sunday_hours',
    'photo_url': 'local_photo_url',
}

# Create a database engine
engine = create_engine(DATABASE_URL)

# Reflect the tables from the database
metadata = MetaData()
metadata.reflect(engine, views=True) # to let us use views in addition to tables from PostgresSQL.

# Access the 'shelters_view' view
shelters_table = Table('shelters_view', metadata)

# Access the 'pantries_view' view
pantries_table = Table('pantries_view', metadata)

# Access the 'cities' table
cities_table = Table('cities', metadata)

app = Flask(__name__)

def make_json_response(data):
    resp = make_response(jsonify(data))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

def get_query(table, field_names: tuple[str]):
    query = table.select().with_only_columns(
        *(table.c[field_name] for field_name in field_names)
    )
    return query

def rows_to_objects(rows, field_names):
    for row in rows:
        yield { field_names[i]: row[i] for i in range(len(field_names)) }

def make_brief(row_object, brief_fields: dict):
    res: dict = dict()
    for brief_field, table_field in brief_fields.items():
        if table_field in row_object:
            res[brief_field] = row_object[table_field]
    if '_search_matches' in row_object:
        res['_search_matches'] = row_object['_search_matches']
    return res
    
@app.route('/', methods=['GET'])
def get_base():
    html_content = '''
    <!DOCTYPE html>
    <html>
    <head>
    <title>Bridging the Bayou API</title>
    </head>
    <body>
    <h1>Welcome to the API for Bridging the Bayou.</h1>
    <p><a href="https://documenter.getpostman.com/view/29331498/2s9YJXb65D">Here is the documentation</a> for the API.</p>
    </body>
    </html>
    '''
    # Return the HTML content
    return html_content


def allShelterNames():
    with engine.connect() as connection:
        result = connection.execute(shelters_table.select().with_only_columns(shelters_table.c.name))
        names = [row[0] for row in result]
        return make_json_response(names)

@app.route('/allShelterNames', methods=['GET'])
def get_all_shelter_names():
    return allShelterNames()

def get_searchable_shelter_text(shelter_brief):
    return get_searchable_text_from_components(
        shelter_brief['name'],
        shelter_brief['city'],
        shelter_brief['state'],
        str(shelter_brief['zip_code']),
        shelter_brief['genders_served'],
        'Yes' if shelter_brief['has_laundry_services'] else 'No',
    )

def allShelterBriefs(request_args):
    with engine.connect() as connection:
        query = get_query(shelters_table, SHELTER_FIELDS)
        query = add_simple_sorts_to_query(query, request_args, shelters_table, SHELTER_SORTABLE_FIELDS)
        query = add_simple_filters_to_query(query, request_args, shelters_table, SHELTER_FILTERS)
        result_objects = tuple(rows_to_objects(connection.execute(query), SHELTER_FIELDS))
        result_objects = add_search(result_objects, request_args, get_searchable_text_fn(SHELTER_SEARCHABLE_FIELDS))
        briefs = [make_brief(obj, SHELTER_BRIEF_FIELDS) for obj in result_objects]
        return briefs
    
@app.route('/allShelterBriefs', methods=['GET'])
def get_all_shelter_briefs():
    return make_json_response(allShelterBriefs(request.args))

def shelter(name):
    # Query for the shelter with the given name
    with engine.connect() as connection:
        result_row = connection.execute(shelters_table.select().where(shelters_table.c.name == name)).fetchone()
        if result_row:
            result_dict = {str(shelters_table.c[i].key): result_row[i] for i in range(len(result_row))}
            return make_json_response(result_dict)
        else:
            return make_json_response({'error': 'Shelter not found'}), 404
    
@app.route('/shelter/<name>', methods=['GET'])
def get_shelter(name):
    return shelter(name)

def allPantryNames():
    with engine.connect() as connection:
        result = connection.execute(pantries_table.select().with_only_columns(pantries_table.c.name))
        names = [row[0] for row in result]
        return make_json_response(names)
    
@app.route('/allPantryNames', methods=['GET'])
def get_all_pantry_names():
    return allPantryNames()

def get_searchable_pantry_text(pantry_brief):
    return get_searchable_text_from_components(
        pantry_brief['name'],
        pantry_brief['address'],
        pantry_brief['city'],
        pantry_brief['state'],
        pantry_brief['zip_code'],
        pantry_brief['schedule_description']
    )

def allPantryBriefs(request_args):
    with engine.connect() as connection:
        query = get_query(pantries_table, PANTRY_FIELDS)
        query = add_simple_sorts_to_query(query, request_args, pantries_table, PANTRY_SORTABLE_FIELDS)
        query = add_simple_filters_to_query(query, request_args, pantries_table, PANTRY_FILTERS)
        result_objects = tuple(rows_to_objects(connection.execute(query), PANTRY_FIELDS))
        result_objects = add_search(result_objects, request_args, get_searchable_text_fn(PANTRY_SEARCHABLE_FIELDS))
        briefs = [make_brief(obj, PANTRY_BRIEF_FIELDS) for obj in result_objects]
        return briefs
    
@app.route('/allPantryBriefs', methods=['GET'])
def get_all_pantry_briefs():
    return make_json_response(allPantryBriefs(request.args))

def pantry(name):
    # Query for the pantry with the given name
    with engine.connect() as connection:
        result_row = connection.execute(pantries_table.select().where(pantries_table.c.name == name)).fetchone()
        if result_row:
            result_dict = {str(pantries_table.c[i].key): result_row[i] for i in range(len(result_row))}
            return make_json_response(result_dict)
        else:
            return make_json_response({'error': 'Pantry not found'}), 404
        
@app.route('/pantry/<name>', methods=['GET'])
def get_pantry(name):
    return pantry(name)
        
def allCityNames():
    with engine.connect() as connection:
        result = connection.execute(cities_table.select().with_only_columns(cities_table.c.city))
        names = [row[0] for row in result]
        return make_json_response(names)
    
@app.route('/allCityNames', methods=['GET'])
def get_all_city_names():
    return allCityNames()

def allCityBriefs(request_args):
    with engine.connect() as connection:
        query = get_query(cities_table, CITY_FIELDS)
        query = add_simple_sorts_to_query(query, request_args, cities_table, CITY_SORTABLE_FIELDS)
        query = add_simple_filters_to_query(query, request_args, cities_table, CITY_FILTERS)
        result_objects = tuple(rows_to_objects(connection.execute(query), CITY_FIELDS))
        result_objects = add_search(result_objects, request_args, get_searchable_text_fn(CITY_SEARCHABLE_FIELDS))
        briefs = [make_brief(obj, CITY_BRIEF_FIELDS) for obj in result_objects]
        return briefs

@app.route('/allCityBriefs', methods=['GET'])
def get_all_city_briefs():
    return make_json_response(allCityBriefs(request.args))

def city(city, state):
    # Query for the city with the given name
    with engine.connect() as connection:
        result_row = connection.execute(cities_table.select().where(cities_table.c.city == city and cities_table.c.state == state)).fetchone()
        if result_row:
            result_dict = {str(cities_table.c[i].key): result_row[i] for i in range(len(result_row))}
            return make_json_response(result_dict)
        else:
            return make_json_response({'error': 'City not found'}), 404

@app.route('/allBriefs', methods=['GET'])
def get_all_briefs():
    """Behavior only defined if exactly one query parameter, search, is given."""
    return make_json_response(allBriefs(request.args))

def allBriefs(request_args):
    return {
        'cities': allCityBriefs(request_args),
        'shelters': allShelterBriefs(request_args),
        'pantries': allPantryBriefs(request_args)
    }

def city(city, state):
    # Query for the city with the given name
    with engine.connect() as connection:
        result_row = connection.execute(cities_table.select().where(cities_table.c.city == city and cities_table.c.state == state)).fetchone()
        if result_row:
            result_dict = {str(cities_table.c[i].key): result_row[i] for i in range(len(result_row))}
            return make_json_response(result_dict)
        else:
            return make_json_response({'error': 'City not found'}), 404
        
def add_simple_sorts_to_query(query, request_args, table, sortable_fields): 
    if 'sort' in request_args and request_args['sort'] in sortable_fields:
        if 'sort_order' in request_args and request_args['sort_order'].lower() == 'desc':
            query = query.order_by(desc(table.c[request_args['sort']]))
        else:
            query = query.order_by(table.c[request_args['sort']])
    return query

def add_simple_filters_to_query(query, request_args, table, filters):
    for filter in filters:
        filter_name = filter['name']
        filter_field = filter['field']
        if filter_name in request_args:
            filter_value = request_args[filter_name]
            filter_type = filter['type']
            if filter_value.lower() != 'false': # treat it as true
                if filter_type == 'array_not_empty':
                    query = query.where(func.array_length(table.c[filter_field], 1) > 0)
                elif filter_type == 'is_not_null':
                    query = query.where(table.c[filter_field] != None)
                elif filter_type == 'is_true':
                    query = query.where(table.c[filter_field] == True)
                elif filter_type == 'is_not_string':
                    query = query.where(table.c[filter_field] != filter['extra'])
                else:
                    raise ValueError(f'filter type: {filter_type}')
            else:
                if filter_type == 'array_not_empty':
                    query = query.where(table.c[filter_field] == '{}')
                elif filter_type == 'is_not_null':
                    query = query.where(table.c[filter_field] == None)
                elif filter_type == 'is_true':
                    query = query.where(table.c[filter_field] == False)
                elif filter_type == 'is_not_string':
                    query = query.where(table.c[filter_field] == filter['extra'])
                else:
                    raise ValueError(f'filter type: {filter_type}')
    return query

def get_searchable_text_fn(fields):
    def fn(brief):
        pieces = [
            ', '.join(brief[x]) if isinstance(brief[x], list) else str(brief[x]) for x in fields
        ]
        return get_searchable_text_from_components(*pieces)
    return fn

def add_search(result_objects, request_args, get_searchable_text):
    if 'search' in request_args:
        result_objects = search_result_objects(result_objects, request_args['search'], get_searchable_text)
    return result_objects

def get_searchable_text_from_components(*text_components):
    """Does not modify the case of the text. Filters out Nones."""
    text = ' @@ '.join(s for s in text_components if s is not None)
    return text

def search_result_objects(briefs, search_string, get_searchable_text) -> list[dict]:
    """Modifies the objects, adding a _search_matches array. Returns a list of the objects that match the search in priority order. Trims and normalizes the search_string."""
    search_string = re.sub(r'\s+', ' ',search_string.strip())
    search_alternatives = [re.escape(search_string)] + list(map(lambda token: re.escape(token), search_string.split(' ')))
    pattern = re.compile('|'.join(map(lambda alt: f'({alt})', search_alternatives)), re.IGNORECASE)
    matching_briefs = []
    for brief in briefs:
        searchable_text = get_searchable_text(brief)
        matches: list[str] = {match.group() for match in pattern.finditer(searchable_text)}
        if len(matches) > 0:
            brief['_search_matches'] = matches
            brief['_longest_search_match'] = max(matches, key=len)
            matching_briefs.append(brief)
    # sort the briefs by priority
    matching_briefs.sort(key=lambda d: len(d['_longest_search_match']), reverse=True)
    for matching_brief in matching_briefs:
        matching_brief['_search_matches'] = list(matching_brief['_search_matches'])
        del matching_brief['_longest_search_match']
    return matching_briefs

@app.route('/city/<city_arg>/<state_arg>', methods=['GET'])
def get_city(city_arg, state_arg):
    return city(city_arg, state_arg)

if __name__ == "__main__":
    app.run(debug=True)