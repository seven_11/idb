PROJECT_FOLDER := bridging-bayou

all: install

install:
	cd $(PROJECT_FOLDER) && npm install

run: start

start:
	cd $(PROJECT_FOLDER) && npm start