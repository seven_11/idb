import { Locator, WebElement } from "selenium-webdriver";
import * as _ from "lodash";
import * as fs from 'fs';
import { parseJSON } from '../utils';

type Pantry = {
  name: string | undefined,
  description: string | undefined,
  address: string | undefined,
  city: string | undefined,
  state: string | undefined,
  zipCode: string | undefined,
  photoURLs: Set<string>,
  phone: string | undefined,
  email: string | undefined,
  contactPerson: string | undefined,
  mondayHours: string | undefined,
  tuesdayHours: string | undefined,
  wednesdayHours: string | undefined,
  thursdayHours: string | undefined,
  fridayHours: string | undefined,
  saturdayHours: string | undefined,
  sundayHours: string | undefined,
  scheduleDescription: string | undefined
};

type SerializablePantry = {
  name?: string,
  description?: string,
  address?: string,
  city?: string,
  state?: string,
  zip_code?: string,
  photo_url_1?: string,
  photo_url_2?: string,
  photo_url_3?: string,
  phone_number?: string,
  email_address?: string,
  contact_person?: string,
  monday_hours?: string,
  tuesday_hours?: string,
  wednesday_hours?: string,
  thursday_hours?: string,
  friday_hours?: string,
  saturday_hours?: string,
  sunday_hours?: string,
  schedule_description?: string
};

type AddressParts = {
  line1?: string,
  city?: string,
  zip?: string,
  state?: string
};


/**
 * @returns undefined if given undefined or the format is not recognized.
 */
function normalizeHours(hours: string | undefined): string | undefined {
  if(hours === undefined)
  return undefined;
  hours = hours.toLowerCase().replace(/\s+/g, '');
  if(hours.includes('close'))
    return 'closed';
  let parts: string[] = hours.split(/-|to/i);
  /** Assumes the given string has no whitespace and is lower case. */
  function normalizeEndpoint(endpoint: string) {
    return endpoint;
  }
  if(parts.length < 2)
    return undefined;
  return `${normalizeEndpoint(parts[0])} - ${normalizeEndpoint(parts[1])}`
}

function normalizePhone(phone: string | undefined): string | undefined {
  if(phone === undefined)
    return undefined;
  phone = phone.replace(/\s+/g, '');
  //delete everything after the first letter, if there is a letter (there shouldn't be).
  for(let i = 0; i < phone.length; i++) {
    if(phone[i].match(/[a-zA-Z]/)) {
      phone = phone.substring(0, i);
      break;
    }
  }
  //strip parens if of the form (123)-456-7890
  if(phone[0] === '(' && phone[4] === ')')
    phone = `${phone.substring(1, 4)}${phone.substring(5)}`;
  //insert dashes if of the form 1234567890
  if(/^\d+$/.test(phone) && phone.length === 10)
    return `${phone.substring(0, 3)}-${phone.substring(3, 6)}-${phone.substring(6)}`;
  return phone;
}

async function findOne(element: WebElement, locator: Locator): Promise<WebElement | undefined> {
  const elements: WebElement[] = await element.findElements(locator);
  if(elements.length !== 1)
    return undefined;
  return elements[0];
}

async function extractTextFromElement(element: WebElement | undefined): Promise<string | undefined> {
  if(element === undefined)
    return undefined;
  return normalizeText((await element.getText()));
}

async function extractLinkFromElement(element: WebElement | undefined): Promise<string | undefined> {
  return extractAttributeFromElement(element, 'href');
}

async function extractAttributeFromElement(element: WebElement | undefined, attributeName: string): Promise<string | undefined> {
  if(element === undefined)
    return undefined;
  let link: string | undefined = (await element.getAttribute(attributeName)) ?? undefined;
  return normalizeText(link);
}

function normalizeText(text: string | undefined): string | undefined {
  if(text === undefined)
    return undefined;
  if(/^\s*$/.test(text))
    return undefined;
  return text.trim();
}

function newPantry(): Pantry {
  return {
    name: undefined,
    description: undefined,
    address: undefined,
    city: undefined,
    state: undefined,
    zipCode: undefined,
    photoURLs: new Set(),
    phone: undefined,
    email: undefined,
    contactPerson: undefined,
    mondayHours: undefined,
    tuesdayHours: undefined,
    wednesdayHours: undefined,
    thursdayHours: undefined,
    fridayHours: undefined,
    saturdayHours: undefined,
    sundayHours: undefined,
    scheduleDescription: undefined
  };
}

/**
 * NOTE: p1 has priority!
 * Assumes areMergable(p1, p2)
 */
function mergePantries(p1: Pantry, p2: Pantry): Pantry {
  const result: Pantry = {
    ...p1,
    photoURLs: new Set([...Array.from(p1.photoURLs), ...Array.from(p2.photoURLs)])
  };
  for(const key in result)
    result[key] ??= p2;
  return result;
}

function areMergable(p1: Pantry, p2: Pantry) {
  return p1.name === p2.name;
}

/** O(n + m) in space and time. */
function mergePantryLists(pList1: Pantry[], pList2: Pantry[]): Pantry[] {
  const pMap1 = _.mapKeys(pList1, v => v.name);
  const pMap2 = _.mapKeys(pList2, v => v.name);
  const resultMap: Record<string, Pantry> = {};
  for(const key in pMap1)
    resultMap[key] = pMap1[key];
  for(const key in pMap2) {
    if(key in resultMap)
      resultMap[key] = mergePantries(resultMap[key], pMap2[key]);
    else
      resultMap[key] = pMap2[key];
  }
  return Object.values(resultMap);
}

function serialize(pantry: Pantry): SerializablePantry {
  const photoURLs: string[] = Array.from(pantry.photoURLs);
  return {
    name: pantry.name,
    description: pantry.description,
    address: pantry.address,
    city: pantry.city,
    state: pantry.state,
    zip_code: pantry.zipCode,
    photo_url_1: photoURLs[0],
    photo_url_2: photoURLs[1],
    photo_url_3: photoURLs[2],
    phone_number: pantry.phone,
    email_address: pantry.email,
    contact_person: pantry.contactPerson,
    monday_hours: pantry.mondayHours,
    tuesday_hours: pantry.tuesdayHours,
    wednesday_hours: pantry.wednesdayHours,
    thursday_hours: pantry.thursdayHours,
    friday_hours: pantry.fridayHours,
    saturday_hours: pantry.saturdayHours,
    sunday_hours: pantry.sundayHours,
    schedule_description: pantry.scheduleDescription
  };
};

function serializeList(pantries: Pantry[]): SerializablePantry[] {
  return pantries.map(p => serialize(p));
}

function deserialize(spantry: SerializablePantry): Pantry {
  return {
    name: spantry.name,
    description: spantry.description,
    address: spantry.address,
    city: spantry.city,
    state: spantry.state,
    zipCode: spantry.zip_code,
    photoURLs: new Set([spantry.photo_url_1, spantry.photo_url_2, spantry.photo_url_3].filter(u => u !== undefined)),
    phone: spantry.phone_number,
    email: spantry.email_address,
    contactPerson: spantry.contact_person,
    mondayHours: spantry.monday_hours,
    tuesdayHours: spantry.tuesday_hours,
    wednesdayHours: spantry.wednesday_hours,
    thursdayHours: spantry.thursday_hours,
    fridayHours: spantry.friday_hours,
    saturdayHours: spantry.saturday_hours,
    sundayHours: spantry.sunday_hours,
    scheduleDescription: spantry.schedule_description,
  };
}

function deserializeList(spantries: SerializablePantry[]): Pantry[] {
  return spantries.map(sp => deserialize(sp));
}

function readPantries(filePath: string): SerializablePantry[] {
  return parseJSON(fs.readFileSync(filePath, 'utf-8')) as SerializablePantry[];
}

/** A VERY primitive address parser. */
function parseAddress(address: string): AddressParts {
  const parts: string[] = address.split(/,\s*/).map(s => s.trim());
  if(parts[parts.length - 1].toLowerCase() === 'usa')
    parts.pop();
  const result: AddressParts = {};
  if(parts.length === 1) {
    result.line1 = parts[0];
  }
  else if(parts.length >= 3) {
    result.line1 = parts.slice(0, parts.length - 2).join(', ');
    const stateZip = parts[parts.length - 1].split(/\s+/);
    result.state = stateZip[0];
    result.zip = stateZip[1];
    result.city = parts[parts.length - 2];
  }
  return result;
}

export {
  normalizeHours,
  normalizePhone,
  normalizeText,
  extractTextFromElement,
  extractLinkFromElement,
  extractAttributeFromElement,
  findOne,
  newPantry,
  mergePantries,
  areMergable,
  mergePantryLists,
  serialize,
  serializeList,
  deserialize,
  deserializeList,
  readPantries,
  parseAddress
};

export type { Pantry, SerializablePantry, AddressParts};