import { Builder, WebDriver, By, WebElement, Locator, Key, until } from 'selenium-webdriver';
import { findOne, normalizeHours, normalizeText, extractTextFromElement, extractLinkFromElement, extractAttributeFromElement, normalizePhone, newPantry, mergePantryLists, serializeList, deserializeList, SerializablePantry, readPantries, parseAddress, AddressParts } from './pantry_utils';
import { Pantry } from './pantry_utils';
import * as fs from 'fs';

type PantryCardInfo = {
  url: string,
  name: string,
  address: string
};

const SEARCHES: string[] = [
  'Mississippi, USA',
  'Arkansas, USA',
  'Louisiana, USA',
  'Ruston, LA, USA',
  'Dumas, AR, USA',
  'Water Valley, MS, USA'
];

const MAX_PANTRIES_PER_STATE: number = 100;
const DATA_FILE_PATH: string = './output/ample_harvest.json';
const LOAD_EXISTING_DATA: boolean = false;

function extractEmailFromURL(url: string | undefined): string | undefined {
  if (url === undefined)
    return undefined;
  if (url.startsWith('mailto:'))
    return url.substring('mailto:'.length);
  return url;
}

class PantryFinder {

  readonly driver: WebDriver;

  constructor(driver: WebDriver) {
    this.driver = driver;
  }

  async findOne(locator: Locator): Promise<WebElement | undefined> {
    const elements: WebElement[] = await this.driver.findElements(locator);
    if (elements.length !== 1)
      return undefined;
    return elements[0];
  }

  async find(locator: Locator): Promise<WebElement[]> {
    const elements: WebElement[] = await this.driver.findElements(locator);
    return elements;
  }

  async waitFor(locator: Locator) {
    await this.driver.wait(until.elementLocated(locator))
    return await this.find(locator);
  }

  async extractText(locator: Locator): Promise<string | undefined> {
    return await extractTextFromElement(await this.findOne(locator));
  }

  async extractLink(locator: Locator): Promise<string | undefined> {
    const element: WebElement | undefined = await this.findOne(locator);
    return extractLinkFromElement(element);
  }

  async findPantriesNear(search: string, namesToSkip?: Iterable<string>): Promise<Pantry[]> {
    const skipNames: Set<string> = new Set(namesToSkip ?? []);
    console.log(`starting search for pantries near '${search}'...`);
    await this.driver.get('https://ampleharvest.org/find-food/');
    const milesInput: WebElement = await this.findOne(By.id('miles'));
    await milesInput.clear();
    await milesInput.sendKeys(`100`);
    const locationInput: WebElement = await this.findOne(By.id('autocomplete-search'));
    await locationInput.clear();
    await locationInput.sendKeys(search);
    await locationInput.sendKeys(Key.ENTER);
    const pantryCards: WebElement[] = await this.waitFor(By.css(' .pantry-list > ul > li'));
    const pantryCardInfos: PantryCardInfo[] = await Promise.all(
      pantryCards.map(async (pc: WebElement): Promise<PantryCardInfo> => {
        const url: string = await (await pc.findElement(By.css('a'))).getAttribute('href');
        const name: string = await (await pc.findElement(By.css('a > h2'))).getText();
        const address: string = await (await pc.findElement(By.css('address'))).getText();
        return { url, name, address };
      })
    );
    console.log(`${pantryCardInfos.length} pantries found in '${search}'! Searching them...`);
    const pantries: Pantry[] = [];
    let pantriesProcessed: number = 0;
    for (const pantryCardInfo of pantryCardInfos) {
      pantriesProcessed++;
      if(skipNames.has(pantryCardInfo.name)) {
        console.log(`\tpantry ${pantriesProcessed}/${pantryCardInfos.length} SKIPPED ('${pantryCardInfo.name}')`);  
        continue;
      }
      const pantry: Pantry = await this.extractPantry(pantryCardInfo);
      console.log(`\tpantry \t${pantriesProcessed}/${pantryCardInfos.length} processed`);
      pantries.push(pantry);
      if(pantries.length >= MAX_PANTRIES_PER_STATE) {
        console.log(`\t(!) stopping search; pantry per state limit (${MAX_PANTRIES_PER_STATE}) reached`);
        break;
      }
    }
    return pantries;
  }

  async extractPantry({ url, name, address }: PantryCardInfo): Promise<Pantry> {
    this.driver.get(url);
    const p = newPantry();
    p.name = name;
    const addressParts: AddressParts = parseAddress(address);
    p.address = addressParts.line1;
    p.city = addressParts.city;
    p.state = addressParts.state;
    p.zipCode = addressParts.zip;
    const metadataElements: WebElement[] = await this.find(By.css('.location > .flex-container > ul > li'))
    for (const metadataElement of metadataElements) {
      let description: string = await extractTextFromElement(await findOne(metadataElement, By.css('p:nth-child(1)')));
      description = description.toLowerCase();
      if (description === 'email') {
        let linkElement: WebElement = await findOne(metadataElement, By.css('p:nth-child(2) > a'));
        p.email = extractEmailFromURL(await extractLinkFromElement(linkElement));
      }
      else {
        let dataElement: WebElement = await findOne(metadataElement, By.css('p:nth-child(2)'));
        const data: string = normalizeText(await extractTextFromElement(dataElement));
        if (description == 'phone') {
          p.phone = normalizePhone(data);
        }
        else if (description == 'point of contact') {
          p.contactPerson = data;
        }
      }
    }
    p.description = await this.extractText(By.css('#main > section.pantry-overview > div.container.flex-container > article > h2.heading-5'));
    const imgSrc1: string | undefined = await extractAttributeFromElement(await this.findOne(By.css('span.image-wrapper > img')), 'src');
    if(imgSrc1)
      p.photoURLs.add(imgSrc1);
    const imgSrc2: string | undefined = await extractAttributeFromElement(await this.findOne(By.css('span.image-wrapper > picture > img')), 'src');
    if(imgSrc2)
      p.photoURLs.add(imgSrc2);
    const weeklySchedule: WebElement = await this.findOne(By.css('.weekly-schedule'));
    const freeformSchedule: WebElement = await this.findOne(By.css('.freeform-schedule'));
    if (weeklySchedule) {
      const hours = async (n: number) => normalizeHours(await extractTextFromElement(await findOne(weeklySchedule, By.css(`dd:nth-of-type(${n})`))));
      p.mondayHours = await hours(1);
      p.tuesdayHours = await hours(2);
      p.wednesdayHours = await hours(3);
      p.thursdayHours = await hours(4);
      p.fridayHours = await hours(5);
      p.saturdayHours = await hours(6);
      p.sundayHours = await hours(7);
      p.scheduleDescription = await this.extractText(By.css('article.donation-times > p:nth-child(1 of .small)'));
    }
    if (freeformSchedule) {
      p.scheduleDescription = normalizeText(await freeformSchedule.getText());
    }
    return p;
  }

  async quit() {
    await this.driver.quit();
  }

}

async function main() {
  console.log('starting...');
  const pantryFinder: PantryFinder = new PantryFinder(await new Builder().forBrowser('chrome').build());
  let allPantries: Pantry[] = [];
  let existingPantries = 0;
  if(LOAD_EXISTING_DATA) {
    allPantries = deserializeList(readPantries(DATA_FILE_PATH));
    existingPantries = allPantries.length;
  }
  for(const search of SEARCHES) {
    const oldLength: number = allPantries.length;
    const pantries: Pantry[] = await pantryFinder.findPantriesNear(search, allPantries.map(p => p.name));
    console.log(`found ${pantries.length} pantries near '${search}'`);
    allPantries = mergePantryLists(allPantries, pantries);
    if(oldLength > 0)
      console.log(`\t${allPantries.length - oldLength}/${pantries.length} unique`);
  }
  await pantryFinder.quit();
  console.log(allPantries);
  console.log(`${allPantries.length} unique pantries found (${allPantries.length - existingPantries} new)!. Writing file...`);
  const allPantriesSerializable = serializeList(allPantries);
  fs.writeFileSync(DATA_FILE_PATH, JSON.stringify(allPantriesSerializable), 'utf-8');
  console.log('done!');
}

main();

export { DATA_FILE_PATH };