import { PoolClient } from "pg";
import { runOnDB } from "../db_utils";
import ENV from "../env";
import { existsSync, mkdirSync, writeFileSync } from "fs";

const FOLDER_NAME: string = 'images';

async function downloadPhoto(row: any) {
  let url = row.photo_url_1;
  const keyIndex: number = url.indexOf('key=');
  if(keyIndex >= 0)
    url = `${url.substring(0, url.indexOf('key=') + 4)}${ENV.GOOGLE_API_KEY}`;
  const buffer: ArrayBuffer = await (await fetch(url)).arrayBuffer();
  writeFileSync(`${FOLDER_NAME}/${row.id}.jpg`, new Uint8Array(buffer));
}

async function main() {
  if(!existsSync(FOLDER_NAME))
    mkdirSync(FOLDER_NAME);
  await runOnDB(async (client: PoolClient) => {
    const rows: any[] = (await client.query(`SELECT * FROM pantries`)).rows;
    let numWithoutPhoto = 0;
    const promises: Promise<any>[] = [];
    for(const row of rows) {
      if(row.photo_url_1) {
        promises.push(downloadPhoto(row));
      }
    }
    console.log('downloading photos...');
    await Promise.all(promises);
    console.log('done downloading photos!...');
  });
  console.log(`done!`);
}

main();