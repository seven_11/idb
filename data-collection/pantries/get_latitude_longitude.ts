import { PoolClient } from "pg";
import { runOnDB } from "../db_utils";
import { readFileSync, writeFileSync } from "fs";
import { parseJSON } from "../utils";

const query = `SELECT DISTINCT p.id, p.name, p.city, p.state, c.latitude, c.longitude FROM pantries p LEFT JOIN cities c ON p.city=c.city AND p.state=c.state ORDER BY p.id ASC;`;
const FILE: string = 'city_latlons.json';

async function main() {
  // runOnDB(async (client: PoolClient) => {
  //   const rows = (await client.query(query)).rows;
  //   writeFileSync(FILE, JSON.stringify(rows));
  // });
  const latlons = parseJSON(readFileSync(FILE, 'utf-8'));
  runOnDB(async (client: PoolClient) => {
    let i = 0;
    for(const ll of latlons) {
      await client.query(`UPDATE pantries SET latitude=${ll.latitude}, longitude=${ll.longitude} WHERE id=${ll.id}`);
      i++;
      console.log(`${i}/${latlons.length}`);
    }
    console.log('done');
  });
}

main();