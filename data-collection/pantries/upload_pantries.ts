import { PoolClient } from "pg";
import { SerializablePantry, readPantries } from "./pantry_utils";
import ENV from "../env";
import { runOnDB } from "../db_utils";

const TABLE_COLS = [
  ['name', `VARCHAR(200) NOT NULL`],
  ['description', `VARCHAR(6000)`],
  ['address', `VARCHAR(200)`],
  ['city', `VARCHAR(100)`],
  ['state', `CHAR(2)`],
  ['zip_code', `CHAR(5)`],
  ['photo_url_1', `VARCHAR(200)`],
  ['photo_url_2', `VARCHAR(200)`],
  ['photo_url_3', `VARCHAR(200)`],
  ['phone_number', `VARCHAR(20)`],
  ['email_address', `VARCHAR(200)`],
  ['contact_person', `VARCHAR(200)`],
  ['monday_hours', `VARCHAR(100)`],
  ['tuesday_hours', `VARCHAR(100)`],
  ['wednesday_hours', `VARCHAR(100)`],
  ['thursday_hours', `VARCHAR(100)`],
  ['friday_hours', `VARCHAR(100)`],
  ['saturday_hours', `VARCHAR(100)`],
  ['sunday_hours', `VARCHAR(100)`],
  ['schedule_description', `VARCHAR(1000)`],
];

const MAKE_TABLE = true;

async function makeTable(client: PoolClient) {
  const makeTableQuery = `
    CREATE TABLE ${ENV.PANTRIES_TABLE_NAME} (
      ${TABLE_COLS.map(col => `${col[0]} ${col[1]}`).join(',')}
    );
  `;
  await client.query(makeTableQuery);
}

function toRow(sp: SerializablePantry): any[] {
  const colNames: string[] = TABLE_COLS.map(c => c[0]);
  return colNames.map((colName: string) => sp[colName] ?? null);
}

async function uploadRows(client: PoolClient, rows: any[][]) {
  const query = `INSERT INTO ${ENV.PANTRIES_TABLE_NAME} (${TABLE_COLS.map(c => c[0]).join(', ')}) VALUES (${Array.from({ length: TABLE_COLS.length }, (_, i) => `$${i + 1}`).join(', ')})`
  for(let i = 0; i < rows.length; i++) {
    const row = rows[i];
    await client.query(query, row);
    console.log(`${i + 1}/${rows.length} uploaded`);
  }
}

async function upload() {
  const pantires: SerializablePantry[] = readPantries(`./output/ample_harvest.json`);
  console.log(`${pantires.length} shelters read and parsed from file`);
  const rows = pantires.map(s => toRow(s));
  console.log(`${pantires.length} shelters reformatted to database rows`);
  console.log(ENV);
  console.log(process.env);
  await runOnDB(async (client: PoolClient) => {
    console.log(`connected to postgres database successfully!`);
    if(MAKE_TABLE) {
      console.log(`making table...`);
      await makeTable(client);
    }
    console.log(`uploading rows...`);
    await uploadRows(client, rows);
    console.log(`all rows uploaded!`);
  });
  console.log(`done!`);
}

upload();