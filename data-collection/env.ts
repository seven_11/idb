import * as dotenv from 'dotenv';
dotenv.config({
  path: './.env'
});

type Env = {
  DB_USERNAME: string,
  DB_HOST: string,
  DB_NAME: string,
  DB_PASSWORD: string,
  DB_PORT: number,
  SHELTERS_TABLE_NAME: string,
  PANTRIES_TABLE_NAME: string,
  CITIES_TABLE_NAME: string,
  GOOGLE_API_KEY: string
};

const ENV: Env = {
  DB_USERNAME: process.env.DB_USERNAME,
  DB_HOST: process.env.DB_HOST,
  DB_NAME: process.env.DB_NAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_PORT: parseInt(process.env.DB_PORT),
  SHELTERS_TABLE_NAME: process.env.SHELTERS_TABLE_NAME,
  CITIES_TABLE_NAME: process.env.CITIES_TABLE_NAME,
  PANTRIES_TABLE_NAME: process.env.PANTRIES_TABLE_NAME,
  GOOGLE_API_KEY: process.env.GOOGLE_API_KEY
};

export default ENV;