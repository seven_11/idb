//The response from an API call that I'm making is a JSON array. Some of the values of this array a strings, but those strings use different start and end delimiters. Some use single quotes, some use double quotes, and some even use backticks (

import { parseJSON } from "./utils";
import * as fs from 'fs';

async function main() {
  // console.log(obj);
  // console.log("abc".substring(1, 100));
  // const obj = new JSONParser("{a:'h', b: `hi`}").parse();
  const str = fs.readFileSync('sampleobj.txt', 'utf-8');
  const result = parseJSON(str);
  console.log(result);
}

main();