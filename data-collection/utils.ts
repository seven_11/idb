function urlEncodeQueryParameter(param: string) {
  return param.replace(/ /, '%20');
}

/** Assumes char is a string of length 1. */
function isWhitespace(char: string) {
  return char === ' ' || char === '\r' || char === '\n' || char === '\t'; //not an exhaustive list
}

function isHexDigits(str: string) {
  return /^[0-9a-fA-F]+$/.test(str);
}

function isValidEscapeSequence(text: string, startIndex: number = 0): boolean {
  return escapeSequenceLength(text, startIndex) !== undefined;
}

/** The startIndex is the index of the backslash (\).
 * Returns the length of the escape sequence
 * starting at startIndex in the given string, including the backslash.
 * For example, for "\t" returns 2, for "\u1234" returns 6.
*/
function escapeSequenceLength(text: string, startIndex: number): number | undefined {
  if(text[startIndex] !== '\\')
    return undefined;
  const char = text[startIndex + 1];
  if( char === "'" || char === '"' || char === '\\' || char == '/' || //forward slash can be escaped in JSON
      char === 'b' || char === 'f' || char === 'n' || char === 'r' ||
      char === 't') {
      return 2;
  }
  if(char === 'u' && isHexDigits(text.substring(startIndex + 2, startIndex + 6))) {
    return 6;
  }
  return undefined;
}

/** Assumes given a valid escape sequence. */
function translateEscapeSequence(text: string, startIndex: number = 0): string {
  if(text[startIndex + 1] === 'u')
    return String.fromCharCode(parseInt(text.substring(startIndex + 2, startIndex + 6), 16));
  switch(text[startIndex + 1]) {
    case '"':
    case "'":
    case '\\':
    case '/':
      return text[startIndex + 1];
    case 'b':
      return '\b';
    case 'f':
      return '\f';
    case 'n':
      return '\n';
    case 'r':
      return '\r';
    case 't':
      return '\t'
  };
}

class JSONParser {

  static isJSONNumber(text: string) {
    return /^-?(([0-9]+(\.[0-9]+)?)|(\.[0-9]+))$/.test(text);
  }

  readonly text: string;
  index: number;

  constructor(text: string) {
    this.text = text;
    this.index = 0;
  }
    
  /** A better JSON parser, since the default one (JSON.parse(...)) doesn't handle
   * some stuff that appears in the responses of some of our API calls.
   */
  parse() {
    return this.parseJSONArray() ?? this.parseJSONObject();
  }
  
  parseJSONArray(): any[] {
    this.skipSpace();
    const start = this.index;
    if(!this.take('['))
      return undefined;
    const arr: any[] = [];
    while(true) {
      const value = this.parseFieldValue();
      if(value === undefined) {
        if(this.take(']')) {
          break;
        }
        this.index = start;
        return undefined;
      }
      arr.push(value);
      if(!this.take(',')) {
        if(!this.take(']')) {
          this.index = start;
          return undefined;
        }
        break;
      }
    }
    return arr;
  }

  /** Returns undefined if not JSON object at the location in the text. */
  parseJSONObject(): Record<string, any> | undefined {
    const start = this.index;
    this.skipSpace();
    if(!this.take('{')) {
      this.index = start;
      return undefined;
    }
    const obj = {};
    while(true) {
      const name: string | undefined = this.parseFieldName();
      if(!name) {
        if(this.take('}'))
          break;
        this.index = start;
        return undefined;
      }
      if(!this.take(':')) {
        this.index = start;
        return undefined;
      }
      const value: any = this.parseFieldValue();
      if(value === undefined) {
        this.index = start;
        return undefined;
      }
      obj[name] = value;
      if(!this.take(',')) {
        if(!this.take('}')) {
          this.index = start;
          return undefined;
        }
        break;
      }
    }
    return obj;
  }

  /** The field name may or may not be in double quotes (no other kind of quotes).
   * If the field was in quotes, the quotes will NOT be inlcuded in the result. */
  parseFieldName(): string | undefined {
    const start = this.index;
    this.skipSpace();
    const startAfterSkippingSpace = this.index;
    let quotes = false;
    if(this.cur() === '"') {
      quotes = true;
      this.index++;
    }
    if(quotes) {
      let name = this.parseDoubleQuotedStringContents();
      this.index += 1; // for closing double quote
      return name;
    }
    else { //not quoted
      while(/[a-zA-Z0-9_]/.test(this.cur())) {
        this.index++;
      }
      return this.text.substring(startAfterSkippingSpace, this.index);
    }
    
  }
  /** returns the string contents */
  parseQuotedString(quote: `"` | `'` | '`'): string | undefined {
    this.skipSpace();
    const start = this.index;
    if(!this.take(quote))
      return undefined;
    const contents = this.parseQuotedStringContents(quote);
    if(contents === undefined) {
      this.index = start;
      return undefined;
    }
    if(!this.take(quote)) {
      this.index = start;
      return undefined;
    }
    return contents;
  }

  parseDoubleQuotedString() {
    return this.parseQuotedString(`"`);
  }

  parseSingleQuotedString() {
    return this.parseQuotedString(`'`);
  }

  parseBacktickQuotedString() {
    return this.parseQuotedString('`');
  }

  parseQuotedStringContents(quote: `"` | `'` | '`'): string | undefined {
    const start = this.index;
    let str = '';
    while(this.index < this.text.length) { //i.e. while true, basically
      if(this.cur() === quote) {
        break;
      }
      else if(this.cur() === '\\') {
        const length = escapeSequenceLength(this.text, this.index);
        if(length === undefined) { //invalid escape sequence - can't parse
          this.index = start;
          return;
        }
        str += translateEscapeSequence(this.text, this.index);
        this.index += length;
      }
      else {
        str += this.cur();
        this.index++;
      }
    }
    return str;
  }

  parseDoubleQuotedStringContents(): string | undefined {
    return this.parseQuotedStringContents('"');
  }

  parseFieldValue(): any {
    if(this.take('null'))
      return null;
    if(this.take('false'))
      return false;
    if(this.take('true'))
      return true;
    const obj = this.parseJSONObject();
    if(obj !== undefined)
      return obj;
    const arr = this.parseJSONArray();
    if(arr !== undefined)
      return arr;
    let str = this.parseDoubleQuotedString();
    if(str !== undefined)
      return str;
    str = this.parseSingleQuotedString();
    if(str !== undefined)
      return str;
    str = this.parseBacktickQuotedString();
    if(str !== undefined)
        return str;
    return this.parseNumber();
  }

  parseNumber(): number | undefined {
    let i = this.index;
    let match = undefined;
    while(i < this.text.length) {
      if(this.text[i] === '.' || this.text[i] === '-') { // can't end on either of these characters.
        i++;
        continue;
      }
      const sub = this.text.substring(this.index, i + 1);
      if(JSONParser.isJSONNumber(sub)) {
        i++;
        match = sub;
      }
      else {
        break;
      }
    }
    if(match === undefined)
      return undefined;
    const number = parseFloat(match);
    if(isNaN(number))
      return undefined;
    this.index = i;
    return number;
  }

  skipSpace(): void {
    while(isWhitespace(this.text[this.index]))
      this.index++;
  }

  skipLookahead(token: string): boolean {
    this.skipSpace();
    return this.text.substring(this.index, this.index + token.length) === token;
  }

  cur(): string | undefined {
    return this.text[this.index];
  }

  take(str: string): boolean {
    this.skipSpace();
    if(this.text.substring(this.index, this.index + str.length) === str) {
      this.index += str.length;
      return true;
    }
    return false;
  } 

};

function parseJSON(str: string): any {
  return new JSONParser(str).parse();
}
  
export { urlEncodeQueryParameter, parseJSON};