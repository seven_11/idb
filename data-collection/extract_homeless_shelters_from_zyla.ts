import * as fs from 'fs';
import { parseJSON, urlEncodeQueryParameter } from './utils';
import { City, CityFileDescription, getAllReadShelters, getFilePath, hasBeenRead, isSuccessfulResponseObject, readCities } from './zyla_shelter_utils';

/**
 * A hard limit on the number of cities we get data for to
 * limit the number of api calls.
 */
const MAX_CITIES = 75;
// const API_KEY = '2339|63LSsT30HwCTLsYsrTeu2ZZhoa9fgsk1v6DMqcJ2';
const API_KEY = '2353|vM2iKKkNHmWUUzTguEqzWAwKmKJ24FyTzhmqRnzk';
const OUTPUT_FILE_PATH: string = `output/shelters/zyla/all.txt`;
const OUTPUT_WITH_DERIVED_FIELDS_FILE_PATH: string = `output/shelters/zyla/all_with_derived_fields.txt`;

async function main() {
  const cities: City[] = readCities();
  let citiesQueried: number = 0;
  for(const city of cities) {
    if(hasBeenRead(city)) {
      console.log(`skipping`, city);
      continue;
    }
    else {
      console.log(`city ${citiesQueried}: `, city);
    }
    const fileName = getFilePath(city);
    const url = `https://zylalabs.com/api/2676/homeless+shelters+api/2780/shelters+by+city+and+state?state=${urlEncodeQueryParameter(city.state)}&city=${urlEncodeQueryParameter(city.name)}`;
    const response: Response = await fetch(url, {
      headers: {
        'Authorization': `Bearer ${API_KEY}`
      }
    });
    const responseText = await response.text();
    fs.writeFileSync(fileName, responseText); //overwrites existing file contents
    const sheltersFromRequest: any = parseJSON(responseText);
    if(sheltersFromRequest === undefined) {
      console.log(`\t(!) parse failed`);
    }
    else if(!isSuccessfulResponseObject(sheltersFromRequest)) {
      console.log(`\t(!) unsuccessfuly response with message '${sheltersFromRequest.message.substring(0,20)}...'`);
    }
    citiesQueried++;
    if(citiesQueried >= MAX_CITIES)
      break;
  }
  // console.log(`writing to file ${shelters.length} shelters...`);
  
  console.log(`done getting data...`);
  const shelters: Record<string, any>[] = getAllReadShelters(cities);
  console.log(`${shelters.length} shelters found; writing them to ${OUTPUT_FILE_PATH}...`);
  fs.writeFileSync(OUTPUT_FILE_PATH, JSON.stringify(shelters));
  console.log(`done!`);
}

// main();

function testing() {
  const cities = readCities();
  let successes = 0;
  let total = cities.length;
  for(const city of cities)
    if(hasBeenRead(city))
      successes++;
  console.log(`${successes}/${total} city successes`);
  const shelters = getAllReadShelters(cities);
  const names: Set<string> = new Set();
  for(const shelter of shelters) {
    names.add(shelter.name);
  }
  console.log(`${names.size} unique shelter names`);
  // console.log(Array.isArray(undefined));
}
// testing();

type Sexes =  'men' | 'women' | 'both';
function getSexes(words: string[]): Sexes {
  const men: boolean = words.includes('men') || words.includes('Men');
  const women: boolean = words.includes('women') || words.includes('Women');
  if(men && !women)
    return 'men';
  if(women && !men)
    return 'women'
  return 'both';
}

function servesChildren(words: string[]): boolean {
  return words.includes('children') || words.includes('Children');
}

function hasLaundryServices(words: string[]): boolean {
  return words.includes('laundry') || words.includes('Laundry');
}

function addFields() {
  const cities: City[] = readCities();
  const shelters = getAllReadShelters(cities);
  const sexBreakdown: { [K in Sexes]: number } = { 'men': 0, 'women': 0, 'both': 0}
  let numServingChildren = 0;
  let numWithLaundryServices = 0;
  for(const shelter of shelters) {
    const words = shelter.description.split(/\s+/);
    const sexes: Sexes = getSexes(words);
    const children: boolean = servesChildren(words);
    const laundry: boolean = hasLaundryServices(words);
    if(children)
      numServingChildren++;
    if(laundry)
      numWithLaundryServices++;
    // shelter.sexes = getSexes(words);
    console.log(`${words.length} words -> ${sexes}`);
    sexBreakdown[sexes]++;
    shelter['genders_served'] = sexes;
    shelter['serves_children'] = children;
    shelter['has_laundry_services'] = laundry;
  }
  console.log(sexBreakdown);
  console.log(`${numServingChildren} serving children`);
  console.log(`${numWithLaundryServices} with laundry services`);
  fs.writeFileSync(OUTPUT_WITH_DERIVED_FIELDS_FILE_PATH, JSON.stringify(shelters));
}

// addFields();

(function() {
  const cities: City[] = readCities();
  const shelters = getAllReadShelters(cities);
  let longestDesc = '';
  for(const shelter of shelters) {
    const desc = shelter['description'];
    longestDesc = (desc?.length ?? 0) > longestDesc.length ? desc : longestDesc;
  }
  // console.log(`longest desc: ${longestDesc}`);
  // console.log(`length: ${longestDesc.length}`);
  let mostPics = [];
  const picCounts = [0, 0, 0, 0, 0, 0];
  let longestPhotoURL = '';
  for(const shelter of shelters) {
    const photo_urls = shelter['photo_urls'];
    console.log(photo_urls);
    for(const url of photo_urls) {
      if(url.length > longestPhotoURL.length)
        longestPhotoURL = url;
    }
    mostPics = (photo_urls?.length ?? 0) > mostPics.length ? photo_urls : mostPics;

    picCounts[photo_urls.length]++;
  }

  // console.log(`most pics: ${mostPics.length}`);
  // console.log(picCounts);
  // console.log(longestPhotoURL);
  // console.log(`longest URL: ${longestPhotoURL.length}`);
  // for(const shelter of shelters)
  //   console.log(shelter.location);

  for(const key of Object.keys(shelters[0]))
    console.log(key);
})();


// fs.writeFileSync('over.txt', 'bye');