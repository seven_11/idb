import { PoolClient } from "pg";
import { runOnDB } from "./db_utils";
import { getOutputSheltersWithDerivedFields } from "./zyla_shelter_utils";
import ENV from "./env";

const MAX_PHOTO_URLS = 5;

const TABLE_COLS = [
  ['name', `VARCHAR(200) NOT NULL`],
  ['address', `VARCHAR(200)`],
  ['city', `VARCHAR(100)`],
  ['state', `CHAR(2)`],
  ['zip_code', `CHAR(5)`],
  ['latitude', `REAL`],
  ['longitude', `REAL`],
  ['phone_number', `VARCHAR(20)`],
  ['email_address', `VARCHAR(200)`],
  ['fax_number', `VARCHAR(20)`],
  ['official_website', `VARCHAR(500)`],
  ['twitter', `VARCHAR(200)`],
  ['facebook', `VARCHAR(200)`],
  ['instagram', `VARCHAR(200)`],
  ['description', `VARCHAR(6000)`],
  ['photo_url_1', `VARCHAR(200)`],
  ['photo_url_2', `VARCHAR(200)`],
  ['photo_url_3', `VARCHAR(200)`],
  ['photo_url_4', `VARCHAR(200)`],
  ['photo_url_5', `VARCHAR(200)`],
  ['genders_served', `VARCHAR(5) CHECK (genders_served IN ('men', 'women', 'both'))`],
  ['serves_children', `BOOLEAN`],
  ['has_laundry_services', `BOOLEAN`]
];
async function makeTable(client: PoolClient) {
  const makeTableQuery = `
    CREATE TABLE ${ENV.SHELTERS_TABLE_NAME} (
      ${TABLE_COLS.map(col => `${col[0]} ${col[1]}`).join(',')}
    );
  `;
  await client.query(makeTableQuery);
}

function toRow(o: Record<string, any>): any[] {
  const photo_urls = o.photo_urls;
  while(photo_urls.length < MAX_PHOTO_URLS)
    photo_urls.push(null);
  const [latStr, lonStr] = o.location.split(',');
  const lat = parseFloat(latStr);
  const lon = parseFloat(lonStr);
  return [
    o.name,
    o.address,
    o.city,
    o.state,
    o.zip_code,
    lat,
    lon,
    o.phone_number,
    o.email_address,
    o.fax_number,
    o.official_website,
    o.twitter,
    o.facebook,
    o.instagram,
    o.description,
    ...photo_urls,
    o.genders_served,
    o.serves_children,
    o.has_laundry_services
  ].map(x => x === '' ? null : x)
}

async function uploadRows(client: PoolClient, rows: any[][]) {
  const query = `INSERT INTO ${ENV.SHELTERS_TABLE_NAME} (${TABLE_COLS.map(c => c[0]).join(', ')}) VALUES (${Array.from({ length: TABLE_COLS.length }, (_, i) => `$${i + 1}`).join(', ')})`
  for(let i = 0; i < rows.length; i++) {
    const row = rows[i];
    client.query(query, row);
    console.log(`${i + 1}/${rows.length} uploaded`);
  }
}

async function upload() {
  const shelters = getOutputSheltersWithDerivedFields();
  console.log(`${shelters.length} shelters read and parsed from file`);
  const rows = shelters.map(s => toRow(s));
  console.log(`${shelters.length} shelters reformatted to database rows`);
  await runOnDB(async (client: PoolClient) => {
    console.log(`connected to postgres database successfully!`);
    //make test table
    // await client.query(`CREATE TABLE test_table (name VARCHAR(100));`);
    // await makeTable(client);
    // console.log(`table made`);
    await uploadRows(client, rows);
    console.log(`all rows uploaded!`);
  });
  console.log(`done!`);
}

upload();