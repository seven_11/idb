import { PoolClient } from "pg";
import { runOnDB } from "../db_utils";

function haversineDistance(lat1, lon1, lat2, lon2) {
  // Convert latitude and longitude from degrees to radians
  lat1 = lat1 * (Math.PI / 180);
  lon1 = lon1 * (Math.PI / 180);
  lat2 = lat2 * (Math.PI / 180);
  lon2 = lon2 * (Math.PI / 180);
  
  // Haversine formula
  const dlat = lat2 - lat1;
  const dlon = lon2 - lon1;
  const a = Math.sin(dlat / 2) * Math.sin(dlat / 2) +
            Math.cos(lat1) * Math.cos(lat2) * 
            Math.sin(dlon / 2) * Math.sin(dlon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  
  // Radius of Earth in miles
  const R = 3958.8;
  
  // Calculate distance
  const distance = R * c;
  
  return distance;
}

function findNearest(source: any, targets: any[]) {
  let min: number =  1_000_000;
  let best: any = targets[0];
  for(const target of targets) {
    if(source !== target) {
      const dist = haversineDistance(source.latitude, source.longitude, target.latitude, target.longitude);
      if(dist < min) {
        min = dist;
        best = target;
      }
    }
  }
  return best;
}

async function main() {
  await runOnDB(async (client: PoolClient) => {
    const cities: any[] = (await client.query('SELECT * FROM cities')).rows;
    const shelters: any[] = (await client.query('SELECT * FROM shelters')).rows;
    const pantries: any[] = (await client.query('SELECT * FROM pantries')).rows;
    for(const city of cities) {
      if(city.nearest_shelter === null) {
        const shelter = findNearest(city, shelters);
        await client.query(`UPDATE cities SET nearest_shelter=$$${shelter.name}$$ WHERE id=${city.id}`);
        console.log(`shelter added to ${city.city}, ${city.state}`);
      }
      if(city.nearest_pantry === null) {
        const pantry = findNearest(city, pantries);
        await client.query(`UPDATE cities SET nearest_pantry=$$${pantry.name}$$ WHERE id=${city.id}`);
        console.log(`pantry added to ${city.city}, ${city.state}`);
      }
    }
  });
  console.log('done!');
}

main();