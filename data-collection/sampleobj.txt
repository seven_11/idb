[
  {
    name: "Timmons's Habitat - Domestic Violence Transitional Housing",
    address: 'P.O. Box 871371',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70187',
    location: '29.953700000000026,-90.07774999999998',
    phone_number: '(504) 615-7043',
    email_address: 'TIMMONS11@BELLSOUTH.NET',
    fax_number: '',
    official_website: 'http://timmonshabitatinc.vpweb.com',
    twitter: '',
    facebook: '',
    instagram: '',
    description: "Timmons' Habitat is a new non-profit 501 (c) (3) organization that aims to reduce the number of homeless and domestic violence cases by providing safer housing options for women and children in dangerous and hostile environments.",
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/no_photo_15.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: 'Salvation Army Center of Hope New Orleans',
    address: '3020 Legion St',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70056',
    location: '29.892567546026253,-90.02747190709283',
    phone_number: '(504) 899-4569',
    email_address: 'sawajxn@gmail.com',
    fax_number: '(504) 896-1076',
    official_website: 'http://salvationarmyalm.org/nola/center-of-hope/',
    twitter: 'https://twitter.com/salarmyalm',
    facebook: 'https://www.facebook.com/SalvationArmyGNO',
    instagram: 'https://www.instagram.com/salarmyalm',
    description: 'Center of HopeThe Salvation Army Center of Hope provides a hot meal and overnight lodging for single adults, families, seniors and the disabled who have no other option for safe shelter all in keeping with our mission, "to meet human needs without discrimination." A variety of social service programs are also available to those in need.Overnight Emergency ShelterClients can utilize overnight emergency shelter for 30 to 90 days (first 7 days free, followed by $10 per night thereafter)Provides a full breakfast and dinner providedClients are allowed to bring 2 bags into facilityAfter initial entry, a bed in the Center of Hope will be reserved for the client until departure (must check in by 6:00 PM for reserved bed)Clients depart Center of Hope by 7:00 AM and can return to facility at 4:00 PM (curfew is 6:00 PM)',
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/3354_salvationarmyalm_org.jpg',
      'https://www.homelessshelterdirectory.org/gallery/35_salarmyalm.jpg',
      'https://www.homelessshelterdirectory.org/gallery/3354__wwm.jpg',
      'https://www.homelessshelterdirectory.org/gallery/3354_la_salvation-army-center-of-hope-new-orleans_lgv.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: 'SALVATION ARMY TRANSITIONAL FAMILY HOUSING',
    address: '4526 S Claiborne Ave',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70125',
    location: '29.939638003678823,-90.1054169750332',
    phone_number: '(504) 899-4569',
    email_address: '',
    fax_number: '',
    official_website: '',
    twitter: '',
    facebook: '',
    instagram: '',
    description: "The Salvation Army allows people to stay five nights for free after which they have to pay $10 a night. The shelter also has beds set aside for people who are working but don't make enough to afford their own home.The Salvation Army provides caseworkers to help people seek jobs, housing or medical care.Office Hours: 4:30 p.m.-12 a.m.PHONE: 504-899-4569 EXT. 300 OR 318Intake Procedure: Walk-in intake is at 4:30 daily. Morning departure is by 6:30 a.m. The first seven nights are free, and the fee is $8 thereafter or a voucher.Freeze Plan beds are available to the public.THE SALVATION ARMY OF GREATER NEW ORLEANS WOMEN AND CHILDRENS EMERGENCY SHELTEREmergency shelter for homeless women with or without children, children needing to be at least 6 weeks, female children no older than 17 years and males no older than 12 years (housing and case management for up to 3 months)4500 S. Claiborne Ave.; New Orleans, LA 70125Office Hours: 4:30 a.m.-12 a.m.PHONE: 504-899-4569 EXT. 300Intake Procedure: Walk-in intake is at 4:30 p.m. daily. Morning departure is at 6:30 am. IDs and current TB cards are required (after first three day waiver). The first 7 nights are free, and the fee is $8 thereafter or a voucher. Housing and case management for women with children is up to three months.Freeze Plan beds are available to the public.",
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/no_photo_11.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: 'Odyssey House Family Center',
    address: '1125 North Tonti Street',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70119',
    location: '29.972141012536426,-90.07633998527953',
    phone_number: '(504) 821-9211',
    email_address: 'hlikaj@ohlinc.org',
    fax_number: '',
    official_website: 'http://www.ohlinc.org/',
    twitter: 'https://twitter.com/odysseyhousela',
    facebook: 'https://www.facebook.com/OdysseyHouseLouisiana',
    instagram: 'https://www.instagram.com/odysseyhousela/',
    description: `Detox Program. This is not free. Call for costs. Odyssey House Louisiana (OHL) is a nonprofit behavioral healthcare facility with an emphasis on addiction treatment. Established in 1973, OHL offers comprehensive services and effective support systems- including detox, treatment, physical and mental healthcare, life-skills, counseling and case management-that enable individuals to chart new lives and return to their communities as contributing members. OHL is currently accepting clients for all levels of treatment throughout its programs. Our admissions process varies depending on the program. Please select the link related to your program of interest to find out more about the Admissions Process. Potential clients can be referred to OHL by various medical and case management agencies, agencies in the penal system, or by themselves. Potential clients should call the appropriate facility to begin the process of admittance. Clients must call on their own behalf; OHL cannot admit any client against their will. Potential clients must undergo a screening process either face-to-face or over the phone. Our admissions process varies depending on the program. To find out more about the admissions process for a particular program, please visit that program's information page, found by clicking on the links under "Substance Abuse Programs" and "Community Programs."`,
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/1110_ohlinc_org.jpg',
      'https://www.homelessshelterdirectory.org/gallery/1110_1648725091.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: 'New Orleans Mission',
    address: '1130 Oretha Castle Haley Blvd',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70113',
    location: '29.943391000825628,-90.07702403239767',
    phone_number: '(504) 638-5434',
    email_address: 'info@neworleansmission.org',
    fax_number: '(504) 529-3094',
    official_website: 'https://neworleansmission.org/',
    twitter: 'https://twitter.com/NOLAMission',
    facebook: 'https://www.facebook.com/neworleansmission',
    instagram: 'https://www.instagram.com/nola_mission_/',
    description: "We are open 24 hours a day, 7 days a week. Men's Discipleship The Mens Discipleship Program is designed to holistically meet the mental, physical, social, and spiritual needs of the participants. The holistic approach of our structured year-long Mens Discipleship Program promotes a strong mind, body, soul connection that helps to balance all aspects of a persons life. At risk youth 18-24 and all ages of men suffering from abuse or addiction enroll in our programs and learn how to champion everyday life choices and take charge of their health and well-being. Disciples have mentors who walk alongside them, empowering them to face their fears, surrender, and trust Gods spirit within us to guide their actions. Our weekly bible studies, chapel services, small groups, one-on-one time, and phase classes are helping men experience the unconditional love and forgiveness of our Lord, Jesus Christ, which ultimately leads to a healed heart over the twelve months of the program. Personal experience has taught us that until a person experiences a healed heart, permanent recovery is not possible; therefore, based on results, becoming a disciple is the first step to a life-altering transformation and the beginning to the end of their cycle of deadly behavior. Womens Discipleship: The percentage of addicted, abused, human trafficked, and homeless women in New Orleans has increased dramatically over the years. The New Orleans Mission is vigorously addressing the crisis to make a positive impact in our region. The Womens Discipleship program offers hurting women that are suffering from abuse or addiction, a safe drug and alcohol-free environment where they can focus on their recovery. While enrolled in our program, we love, encourage, and minister to the ladies as they begin to regain their health and well-being. A good mind-set ultimately leads to forgiveness and a healed heart. Hope is instilled daily as we provide a source of dignity and purpose for every lady in our program. These women are successfully rebuilding the tattered remnants of their self-worth and are becoming able to fulfill Gods purpose for their life. Our main womens recovery facility is the Lynhaven Retreat. The Lynhaven is beautiful 18-acre property in Hammond, LA where these hurting women can get out of the distractions of the city and focus on their healing and recovery. We require no insurance or fees for enrollment. The program is completely free to anyone that seeks our support. If you or someone you love needs help, please contact us today. Desperate Reality-Facing the Truth-Americas only live street rescue program: Our live radio rescue show, Desperate Reality-Facing the Truth, was launched in June of 2016 and is still ministering to the homeless in the streets of our city. The show is hosted by Johnny Lonardo, our COO and Director of Discipleship, who ministers to hurting people over the phone and sends Ministry Outreach teams into the streets of the city to talk to the homeless in their painful circumstances, pray for their safety and for Gods healing. The show airs Saturday nights from 10pm to midnight and provides awareness of the work being done at the Mission to listeners and to the homeless men and women met by our Outreach teams. Several people came to the Mission because of the show and joined the Discipleship program. Ex-Offender Reentry The New Orleans court systems as well as the courts on the North Shore offer many non-violent and first offenders the opportunity of joining our Discipleship Program in lieu of going to jail; over 50 ex-offenders are enrolled in our program monthly and receiving multi-pronged legal, medical, literary tutoring, spiritual enlightenment, and vocational training. We also provide prison ministry regularly at local prisons. In 2018, we experience a 20% rise in Drug Court offenders that were mandated to our Discipleship program for group and one-on-one time, Bible classes, and weekly drug screens instead of typical court appointed programs. Many studies have indicated that re-entry initiatives, like ours at the New Orleans Mission, that combine work training with counseling and housing assistance, can greatly reduce recidivism. The Department of Justices Bureau of Justice Statistics has estimated that nearly 75% of all released prisoners will be rearrested within five years of their release and about 6 in 10 will be reconvicted. The recidivism rate of re-entry Graduates of our Discipleship program is only 15%, significantly lower than the national average. Fines and registration fees for sex offenders have been converted to community service for Disciples of re-entry; to date $130,000 in fees has been converted. Our Re-entry Coordinator maintains a calendar of court dates and ensures transportation to and from court; he accompanies Disciple and speaks to judges on their behalf. We assist in execution of Child Protective Services and D.C.F.S case plans The cost for tax payers to house one offender is $36,639.00 per person per year; our program is saving the parish thousands of dollars housing ex-offenders through our Re-entry program.",
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/3404_la_new-orleans-mission_ndx.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: 'Project Lazarus',
    address: '2824 Dauphine St',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70117',
    location: '29.965003989620598,-90.04866201898487',
    phone_number: '(504) 949-3609',
    email_address: 'rthoulion@projectlazarus.net',
    fax_number: '',
    official_website: 'http://www.projectlazarus.net/',
    twitter: 'https://twitter.com/projectlazarus1',
    facebook: 'https://www.facebook.com/pages/Project-Lazarus/95160603448?fref=ts',
    instagram: 'https://www.instagram.com/projectlazarusnola',
    description: 'At Project Lazarus, we provide housing that fits the needs of people living with HIV/AIDS: Respite Care and Transitional Housing.Our transitional housing is limited in duration usually from several months up to two years. On average our residents stay in this program for one year. Its intention is to help people transition from a housing crisis into a permanent, stable housing situation. Their crisis may have been caused by a medical condition, resulting in hospitalization, loss of employment, homelessness or stays in a homeless shelter, and/or other effects of the disease progression of HIV/AIDS.Our focus and goal in this program is to provide temporary housing and services to individuals to ensure access to healthcare, increase medication education and adherence, promote a healthy diet and lifestyle, connect with community resources and access available benefits allowing for further medical stabilization and the opportunity to secure permanent housing. This program is ideal for the individual who temporarily needs an extra hand and support to regain independence. Often it is necessary to assist individuals in developing the skills and provide ongoing resources needed to succeed in permanent housing.',
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/1114__tsg.jpg',
      'https://www.homelessshelterdirectory.org/gallery/1114_projectlazarus_net.jpg',
      'https://www.homelessshelterdirectory.org/gallery/1114__rlu.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: 'Ozanam Inn',
    address: '843 Camp Street',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70130',
    location: '29.94476500430366,-90.07109199188235',
    phone_number: '(504) 523-1184',
    email_address: 'admin@ozanaminn.org',
    fax_number: '(504) 523-1187',
    official_website: 'http://www.ozanaminn.org',
    twitter: '',
    facebook: 'https://www.facebook.com/ozanaminn',
    instagram: 'https://www.instagram.com/ozanaminn/',
    description: '',
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/1112_la_ozanam-inn_apk.jpg',
      'https://www.homelessshelterdirectory.org/gallery/1112_la_ozanam-inn_lgb.jpg',
      'https://www.homelessshelterdirectory.org/gallery/1112_1623149618.jpg',
      'https://www.homelessshelterdirectory.org/gallery/1112_ozanaminn_org.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  },
  {
    name: "New Orleans Women's And Children's Shelter",
    address: '2020 S Liberty St',
    city: 'New Orleans',
    state: 'LA',
    zip_code: '70113',
    location: '29.94064399969794,-90.0841309642811',
    phone_number: '(504) 522-9340',
    email_address: 'info@nowcs.org',
    fax_number: '',
    official_website: 'https://nowcs.org/',
    twitter: 'https://twitter.com/nowcshelter',
    facebook: 'https://www.facebook.com/NOWCS?fref=ts',
    instagram: 'https://www.instagram.com/nowcshelter',
    description: "Women 21 years of age or older can call for service options; no boys over 11 years of age are accepted.The New Orleans Women's Shelter's (NOWS) mission is to enable women and their children to transition out of homelessness and poverty into a stable and productive lifestyle. To realize this mission, NOWS seeks to: 1) Operate a resource rich, safe, empowering environment for women to make a new future for themselves and their children, and 2) Assist women in transitioning from homeless to independent living. NOWS reaches these goals by providing case management, counseling, employment search assistance, and educational services while stressing and supporting personal responsibility. NOWS can house 20 women and their children at any one time.",
    photo_urls: [
      'https://www.homelessshelterdirectory.org/gallery/3405_nowcs_org.jpg',
      'https://www.homelessshelterdirectory.org/gallery/3405__tns.jpg'
    ],
    update_datetime: '2023-06-21T17:02:04Z'
  }
]