import * as fs from 'fs';
import { parseJSON } from './utils';

type CityFileDescription = {
  /**
   * The full state name in titlecase of every city in the file.
   * Ex: "Mississippi" 
   */
  state: string,
  fileName: string
};

type City = {
  /**
   * Full state name in titlecase.
   */
  state: string,
  name: string,
  population: number
};

/** in the input folder */
const CITIES_FILE_DESCRIPTIONS: CityFileDescription[] = [
  {
    state: 'Arkansas',
    fileName: 'AK_cities_by_pop.txt'
  },
  {
    state: 'Louisiana',
    fileName: 'LA_cities_by_pop.txt',
  },
  {
    state: 'Mississippi',
    fileName: 'MS_cities_by_pop.txt'
  }
];

function getFilePath(city: City) {
  return `output/shelters/zyla/${city.state}_${city.name}.txt`;
}

function readCitiesFromFile(fileDescription: CityFileDescription): City[] {
  const contents: string = fs.readFileSync(`input/${fileDescription.fileName}`, 'utf-8');
  const citiesFromFile: City[] = contents.trim().split(/\r?\n/).map(line => {
    const [name, population] = line.split(/ (?=\d)/);
    return { state: fileDescription.state, name, population: Number(population) };
  })
  return citiesFromFile;
}

function sortByPopulationDescending(cities: City[]): void {
  cities.sort((a: City, b: City) => b.population - a.population);
}

/** Sorted by population. */
function readCities(sorted: boolean = true): City[] {
  const cities: City[] = [];
  for(const fileDescription of CITIES_FILE_DESCRIPTIONS)
    cities.push(...readCitiesFromFile(fileDescription));
  if(sorted)
    sortByPopulationDescending(cities);
  return cities;
}

/** Returns undefined if the file doesn't exist OR if the JSON is invalid. */
function readCityFile(city: City): any {
  if(!fs.existsSync(getFilePath(city)))
    return undefined;
  return parseJSON(fs.readFileSync(getFilePath(city), 'utf-8'));
}

function hasBeenRead(city: City) {
  return isSuccessfulResponseObject(readCityFile(city));
}

function isSuccessfulResponseObject(obj: any) {
  return Array.isArray(obj); //returns false if obj === undefined, which we want.
}

function getAllReadShelters(cities: City[]): Record<string, any>[] {
  const shelters: Record<string, any>[] = [];
  for(const city of cities) {
    const obj = readCityFile(city);
    if(isSuccessfulResponseObject(obj))
      shelters.push(...obj);
  }
  return shelters;
}

function getOutputSheltersWithDerivedFields(): any[] | undefined {
  const contents: string = fs.readFileSync('output/shelters/zyla/all_with_derived_fields.txt', 'utf-8');
  return parseJSON(contents);
}

export {
  getFilePath,
  readCities,
  readCityFile,
  readCitiesFromFile,
  getAllReadShelters,
  hasBeenRead,
  isSuccessfulResponseObject,
  getOutputSheltersWithDerivedFields
};
export type { City, CityFileDescription };