CREATE TABLE cities (
    city VARCHAR(200),
    state CHAR(2),
    zip_codes TEXT[],
    population INTEGER,
    county VARCHAR(200),
    poverty_rate REAL,
    average_income INTEGER,
    num_shelters INTEGER,
    num_pantries INTEGER,
    closest_shelter VARCHAR(200),
    closest_pantry VARCHAR(200),
    race_breakdown TEXT
);