import { PoolClient } from "pg";
import { runOnDB } from "../db_utils";
import ENV from "../env";
import { readFileSync } from "fs";

const TABLE_COLS = [
  ['city', ''],
  ['state', ''],
  ['zip_codes', ''],
  ['population', ''],
  ['county', ''],
  ['poverty_rate', ''],
  ['average_income', ''],
  ['num_shelters', ''],
  ['num_pantries', ''],
  ['closest_shelter', ''],
  ['closest_pantry', ''],
  ['race_breakdown', '']
];

async function uploadRows(client: PoolClient, rows: any[][]) {
  const query = `INSERT INTO ${ENV.CITIES_TABLE_NAME} (${TABLE_COLS.map(c => c[0]).join(', ')}) VALUES (${Array.from({ length: TABLE_COLS.length }, (_, i) => `$${i + 1}`).join(', ')})`
  for(let i = 0; i < rows.length; i++) {
    const row = rows[i];
    client.query(query, row);
    console.log(`${i + 1}/${rows.length} uploaded`);
  }
}

async function upload() {
  const lines: string[] = readFileSync('cities.txt', 'utf8').trim().split(/\r?\n/);
  const rows: any[][] = [];
  for(const line of lines) {
    const [city, state] = line.split(', ');
    rows.push([city, state, ['12345', '67890'], '92834', 'Hinds', 0.283, 17843, 2, 1, 'Hinds County Human Resource Agency', 'The Good Samaritan Center', 'black:0.52/white:0.40/hispanic:0.08'])
  }

  console.log(`${rows.length} city rows shelters read and made from file`);
  await runOnDB(async (client: PoolClient) => {
    console.log(`connected to postgres database successfully!`);
    //make test table
    // await client.query(`CREATE TABLE test_table (name VARCHAR(100));`);
    // await makeTable(client);
    // console.log(`table made`);
    await uploadRows(client, rows);
    console.log(`all rows uploaded!`);
  });
  console.log(`done!`);
}

upload();