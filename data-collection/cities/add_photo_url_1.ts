import { PoolClient } from "pg";
import { runOnDB } from "../db_utils";

(async () => {
  await runOnDB(async (client: PoolClient) => {
    const rows: any[] = (await client.query(`SELECT city, state FROM cities;`)).rows;
    let count = 0;
    for(const row of rows) {
      const q =`UPDATE cities SET photo_url_1='/${encodeURIComponent(`${row.city},${row.state}`)}.jpg' WHERE city='${row.city}' AND state='${row.state}';`
      await client.query(q);
      console.log(count++);
    }
  });
  console.log('doe');
})();