import { Pool, PoolClient } from "pg";
import { runOnDB } from "../db_utils";
import ENV from "../env";
import { createWriteStream, existsSync, mkdirSync, readFileSync, writeFileSync } from "fs";
import { parseJSON } from "../utils";
import axios from "axios";

type City = {
  name: string,
  state: string
};

type PlaceIDResponseBody = {
  candidates: { place_id: string}[],
  status: string
};

type PhotoReferenceResponseBody = {
  html_attributions: any[],
  result: {
    photos: {
      width: number,
      height: number,
      html_attributions: string[],
      photo_reference: string
    }[]
  },
  status: string
};

async function requestPlaceID(city: City): Promise<PlaceIDResponseBody> {
  const response = await fetch(`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${encodeURIComponent(city.name + ',' + city.state)}&inputtype=textquery&fields=place_id&key=${ENV.GOOGLE_API_KEY}`);
  const body: PlaceIDResponseBody = await response.json();
  return body;
};

async function requestPhotoReference(placeID: string): Promise<PhotoReferenceResponseBody> {
  const response = await fetch(`https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeID}&fields=photo&key=${ENV.GOOGLE_API_KEY}`);
  const body: PhotoReferenceResponseBody = await response.json();
  return body;
}

async function downloadImages(photoReferences: string[], city: string, state: string) {
  const folderName: string = `${city},${state}`;
  ensureFolder(`images/${city},${state}`);
  for(let i = 0; i < photoReferences.length; i++) {
    const ref: string = photoReferences[i];
    const url = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=${ref}&key=${ENV.GOOGLE_API_KEY}`;
    const response = await axios.get(url, { responseType: 'stream' });
    response.data.pipe(createWriteStream(`images/${folderName}/${i}.jpg`));
  }
}

function ensureFolder(name) {
  if(!existsSync(name))
    mkdirSync(name);
}

async function main(): Promise<any> {
  // await runOnDB(async (client: PoolClient) => {
  //   const rows: any[] = (await client.query('SELECT * FROM cities WHERE google_place_id IS NULL;')).rows;
  //   const citiesWithoutPlaceID: City[] = rows.map(r => ({ name: r.city, state: r.state }));
  //   const placeIDResults: [City, PlaceIDResponseBody][] = [];
  //   let citiesProessed = 0;
  //   for(const city of citiesWithoutPlaceID) {
  //     const responseBody: PlaceIDResponseBody = await requestPlaceID(city);
  //     if(responseBody.status !== 'OK') {
  //       console.log(`STATUS NOT OK: '${responseBody.status}'`);
  //       break;
  //     }
  //     citiesProessed++;
  //     console.log(`${citiesProessed} cities processed`);
  //     placeIDResults.push([city, responseBody]);
  //   }
  //   writeFileSync('place_ids.json', JSON.stringify(placeIDResults));
  //   console.log(placeIDResults);
  // });
  const idRows: [City, PlaceIDResponseBody][] = parseJSON(readFileSync('place_ids.json', 'utf-8'));
  // let citiesWithMultipleCandidates = 0;
  // for(const row of idRows) {
  //   const responseBody = row[1];
  //   const city: City = row[0];
  //   if(responseBody.candidates.length !== 1) {
  //     console.log(`${responseBody.candidates.length} candidates for ${city}`);
  //     citiesWithMultipleCandidates++;
  //   }
  // }
  // console.log(`${citiesWithMultipleCandidates} cities with multiple candidates`);

  // await runOnDB(async (client: PoolClient) => {
  //   let count = 0;
  //   for(const idRow of idRows) {
  //     const [city, {candidates}] = idRow;
  //     await client.query(`UPDATE cities SET google_place_id='${candidates[0].place_id}' WHERE city='${city.name}' AND state='${city.state}'`);
  //     console.log(count++);
  //   }
  // })

  // await runOnDB(async (client: PoolClient) => {
  //   const rows: any[] = (await client.query('SELECT city, state, google_place_id FROM cities;')).rows;
  //   const photoResults: [string, PhotoReferenceResponseBody][] = [];
  //   let count = 0;
  //   for(const row of rows) {
  //     const placeID: string = row.google_place_id;
  //     const responseBody: PhotoReferenceResponseBody = await requestPhotoReference(placeID);
  //     photoResults.push([placeID, responseBody]);
  //     console.log(count++);
  //   }
  //   console.log('writing...');
  //   writeFileSync('photo_references.json', JSON.stringify(photoResults));
  // })

  // const refRows: [string, PhotoReferenceResponseBody][] = parseJSON(readFileSync('photo_references.json', 'utf-8'));
  // for(const row of refRows) {
  //   const placeID = row[0];
  //   const responseBody: PhotoReferenceResponseBody = row[1];
  //   if(responseBody.result.photos) {
  //     console.log(placeID);
  //     for(const photo of responseBody.result.photos) {
  //       const htmlAttributions: string[] = photo?.html_attributions;
  //       if(htmlAttributions)
  //         console.log(photo.html_attributions.length);
  //       else
  //         console.log('undefined!');
  //     }
  //   }
  // } 

  // for(const row of refRows) {
  //   const body: PhotoReferenceResponseBody = row[1];
  //   if(!body.result || !body.result.photos) {
  //     console.log(body);
  //     console.log();
  //   }
  // }

  // await runOnDB(async (client: PoolClient) => {
  //   function toArray(arr: string[]) {
  //     return `ARRAY[${arr.map(s => "'" + s + "'").join(', ')}]::TEXT[]`;
  //   }

  //   let count = 0;
  //   for(const row of refRows) {
  //     const placeID = row[0];
  //     const responseBody: PhotoReferenceResponseBody = row[1];
  //     const references: string[] = responseBody.result?.photos ? responseBody.result.photos.map(p => p.photo_reference) : [];
  //     const attributions: string[] = responseBody.result?.photos ? responseBody.result.photos.map(p => p.html_attributions[0]) : [];
  //     const query = `UPDATE cities SET google_photo_references=${toArray(references)}, google_photo_attributions=${toArray(attributions)} WHERE google_place_id='${placeID}'`;
  //     console.log(query);
  //     await client.query(`UPDATE cities SET google_photo_references=${toArray(references)}, google_photo_attributions=${toArray(attributions)} WHERE google_place_id='${placeID}'`);
  //     console.log(count++);
  //   }
  // });

  ensureFolder('images');
  await runOnDB(async (client: PoolClient) => {
    const rows: any[] = (await client.query(`SELECT city, state, google_photo_references FROM cities`)).rows;
    let count = 0;
    for(const row of rows) {
      await downloadImages(row.google_photo_references, row.city, row.state);
      console.log(count++);
    }
  });

  console.log(`done!`);
}

main();