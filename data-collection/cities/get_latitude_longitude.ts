import { Pool, PoolClient } from "pg";
import { runOnDB } from "../db_utils";
import { readFileSync, writeFileSync } from "fs";
import { parseJSON } from "../utils";
import _ = require("lodash");

const STATE_MAP = {
  'LA': 'Louisiana',
  'MO': 'Missouri',
  'AR': 'Arkansas',
  'AL': 'Alabama',
  'TN': 'Tennessee',
  'MS': 'Mississippi'
}

const FILE = 'city_latlons.json';

async function main() {
  // runOnDB(async (client: PoolClient) => {
  //   const rows = (await client.query('SELECT * FROM cities ORDER BY id ASC')).rows;
  //   const records = [];
  //   for(const row of rows) {
  //     const city = row.city;
  //     const stateFull = STATE_MAP[row.state];
  //     const response = await fetch(`https://api.api-ninjas.com/v1/geocoding?city=${city}`, {
  //       headers: {
  //         'X-Api-Key': `zf+NyXEnP6LBwcxghvRW4Q==MVVgfYea6FPTE2qY`
  //       }
  //     });
  //     const cityOptions = await response.json();
  //     let option = null;
  //     for(const o of cityOptions) {
  //       if(o.name === city && o.state === stateFull) {
  //         option = o;
  //         break;
  //       }
  //     }
  //     if(option === null) {
  //       console.log(`COULD NOT FIND LAT/LON for: ${city}, ${stateFull} in the options below`);
  //       console.log(cityOptions);
  //       console.log();
  //       continue;
  //     }
  //     const { latitude, longitude } = option;
  //     records.push({ id: row.id, name: city, state: row.state, latitude, longitude });
  //   }
  //   writeFileSync(FILE, JSON.stringify(records));
  // })
  const latlons = parseJSON(readFileSync(FILE, 'utf-8'));
  runOnDB(async (client: PoolClient) => {
    let count: number = 0;
    for(const ll of latlons) {
      await client.query(`UPDATE cities SET latitude=${ll.latitude}, longitude=${ll.longitude} WHERE id=${ll.id}`);
      count++;
      console.log(`${count}/${latlons.length}`);
    }
    console.log('done!');
  })
}

main();