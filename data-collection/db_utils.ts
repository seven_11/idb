import { Pool, PoolClient, PoolConfig } from 'pg';
import ENV from './env';

const CONFIG: PoolConfig = {
  user: ENV.DB_USERNAME,
  host: ENV.DB_HOST,
  database: ENV.DB_NAME,
  password: ENV.DB_PASSWORD,
  port: ENV.DB_PORT,
  ssl: {
    rejectUnauthorized: false
  }
};

async function runOnDB(func: (client: PoolClient) => Promise<any>) {
  const pool = new Pool(CONFIG);
  const client: PoolClient = await pool.connect();
  await func(client);
  client.release();
  await pool.end();
}

export { runOnDB };