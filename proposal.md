**name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL)**
Bridging the Bayou

**The proposed our project:**
Our project serves people experiencing homelessness within the Mississippi Delta region. We will provide additional resources to help address their housing and food insecurity. This website will act as a directory for those in the Mississippi Delta region that are experiencing homelessness and looking for help.

**Names of the team members**
- Kevin Pham
- Caleb Wolf
- Laura Young
- Julia Elias
- Sam Hooper - Phase Leader

URLs of at least three data sources that you will programmatically scrape using a RESTful API (be very sure about this)
- https://myresources.mdhs.ms.gov/MRProviderServices/Index?service=Food
- https://www.feedingamerica.org/find-your-local-foodbank/all-food-banks
- https://topapis.com/homeless-shelter-api/
- https://foodpantries.org/
- https://arkansasfoodbank.org/locations/#agency-finder
- https://developer.nytimes.com/docs/articlesearch-product/1/overview


**At least three models**
- Cities in the region
- Shelters
- Food pantries

**an estimate of the number of instances of each model**
- Cities in the region (150)
- Shelters (50)
- Food pantries (400)

**each model must have many attributes**
- Cities in the region
    - Location (Zip code)
    - Population size
    - news feeds
    - images


- Shelters
    - if they provide food
    - how long can you stay
    - if they offer elevating services (help you find a job.    etc.)
    - whether they have a single room or one large room
    - gender segregated or not

**describe five of those attributes for each model that you can filter or sort**
- Cities in the region
    - Map (displays city's location)
    - County
    - Poverty rates
    - Average income
    - Number of shelters
    - Number of food pantries

- Shelters
    - Location (Zip code)
    - Population size
    - capacity/availability
    - Women’s/Children, if they service a specific demographic (LGBT+, Black, etc.)
    - If they serve food

- Food pantries
    - Location (Zip code)
    - size/supply
    - dates for pick up
    - times for pick up
    - weekly volunteer hours

**Describe two types of media for instances of each model**
- Cities in the region
    - Map that displays city’s location within Delta region
    - Related news articles to the city
- Shelters
    - Map that displays shelter location within the Delta region
    - News feed of shelter updates/notifications 
- Food pantries
    - Map that displays food pantry’s location within the Delta region
    - Schedule that displays a chart of available times to pick up food from the pantry


**Three questions that our site will answer:**
- Where can I donate food to help people with food insecurity in the states of Mississippi, Arkansas, and Louisiana?
- Where can homeless people find shelter in the Mississippi Delta?
- How can I help support individuals experiencing homelessness within the Delta region?
