**Bridging the Bayou**

**The proposed our project:**
Our project serves people experiencing homelessness within the Mississippi Delta region. We will provide additional resources to help address their housing and food insecurity. This website will act as a directory for those in the Mississippi Delta region that are experiencing homelessness and looking for help.

**Three questions that our site will aim to answer:**
- Where can I donate food to help people with food insecurity in the states of Mississippi, Arkansas, and Louisiana?
- Where can homeless people find shelter in the Mississippi Delta?
- How can I help support individuals experiencing homelessness within the Delta region?

**Names, EIDs of the team members, Estimed time worked & actual time worked for phase 4**
- Kevin Pham, kmp4393, 5 hours estimated, 6 hours actual
- Caleb Wolf, crw3692, 7 hours estimated, 7 hours actual
- Laura Young, lcy248, 10 hours estimated, 5 hours actual
- Julia Elias, je27699, 4 hours estimated, 5 hours actual
- Sam Hooper, slh4838, 5 hours estimated, 10 hours actual

**Phase Leader**
Julia Elias: Make sure team is on track, coordinating get-togethers, ensure rubric is fulfilled

Website link: [https://app.bridging-the-bayou.me](https://app.bridging-the-bayou.me)
Backend API link: [https://api.bridging-the-bayou.me/](https://api.bridging-the-bayou.me/)
API Docs Link: [https://documenter.getpostman.com/view/29331498/2s9YJXb65D](https://documenter.getpostman.com/view/29331498/2s9YJXb65D)

Git SHA: 51883206f8033a44676f2df1bcdce959ee4a8ce4

